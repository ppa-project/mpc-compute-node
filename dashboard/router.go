// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package dashboard implements a HTTP router which serves routes for the MPC Dashboard
package dashboard

import (
	"bufio"
	_ "embed"
	"math/rand"
	"net/http"
	"os"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/logger"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const (
	// MaxTrailingLogBytes Maximum bytes of trailing log to push through websocket
	MaxTrailingLogBytes = 4096
)

// RESTController contains a router, handlers to serve REST
type RESTController struct {
	ww *WebsocketWriter
}

// NewRESTController Constructor for RESTController
func NewRESTController(logConfig *logger.LogConfig) *RESTController {
	ww := NewWebsocketWriter()
	logConfig.AddOutput(ww)
	return &RESTController{ww: ww}
}

// GetRouter returns the main router / handler
func (rc *RESTController) GetRouter() http.Handler {
	if !viper.GetBool("debug") {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	r.GET("/", rc.handleRootRequest)
	r.GET("/logs", rc.getLogStream)

	return r
}

//go:embed static/index.html
var html []byte

// Someone just asks for GET HTTP/2.0 /
// Send a terse response indicating the server is up
func (rc *RESTController) handleRootRequest(c *gin.Context) {
	c.Data(http.StatusOK, "text/html", html)
}

func (rc *RESTController) getLogStream(c *gin.Context) {
	rc.logStreamHandler(c.Writer, c.Request)
}

func (rc *RESTController) logStreamHandler(w http.ResponseWriter, r *http.Request) {
	var wsupgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Warn().Err(err).Msg("Failed upgrade websocket")
		return
	}

	rc.ww.registerWSConnection(conn)
}

// WebsocketWriter manages active websocket connections
type WebsocketWriter struct {
	mu    sync.Mutex
	chans map[int]*chan []byte
}

// NewWebsocketWriter constructs a new WebsocketWriter
func NewWebsocketWriter() *WebsocketWriter {
	return &WebsocketWriter{chans: make(map[int]*chan []byte)}
}

func (ww *WebsocketWriter) registerWSConnection(conn *websocket.Conn) {
	ww.mu.Lock()
	defer ww.mu.Unlock()
	id := -1
	exists := true
	for exists {
		id = rand.Int() //nolint:gosec
		_, exists = ww.chans[id]
	}
	updateChan := make(chan []byte, 5)
	ww.chans[id] = &updateChan

	go func() {
		mainloop := true
		for mainloop {
			msg := <-updateChan
			err := conn.WriteMessage(websocket.TextMessage, msg)
			if err != nil {
				_ = conn.Close()
				ww.mu.Lock()
				defer ww.mu.Unlock()
				delete(ww.chans, id)
				mainloop = false
			}
		}
	}()

	ww.pushTrailingLogs(&updateChan)
}

func (ww *WebsocketWriter) pushTrailingLogs(updateChan *chan []byte) {
	// TODO: implement local caching (remove dependency from reading file)
	// TODO: decrease locking delay for other clients
	f, err := os.OpenFile(viper.GetString("logFile"), os.O_RDONLY, 0600)
	if err != nil {
		log.Error().Err(err).Msg("Failed to load logfile")
		return
	}
	defer func() {
		if err = f.Close(); err != nil {
			log.Err(err).Msg("Error closing logfile")
		}
	}()

	fi, err := f.Stat()
	if err != nil {
		log.Error().Err(err).Msg("Failed to get stat of logfile")
		return
	}

	size := fi.Size()
	var offset int64

	if MaxTrailingLogBytes > size {
		offset = 0
	} else {
		offset = size - MaxTrailingLogBytes
	}
	_, _ = f.Seek(offset, 0)

	scanner := bufio.NewScanner(f)
	scanner.Scan() // Strip until new line
	for scanner.Scan() {
		*updateChan <- scanner.Bytes()
	}
}

func (ww *WebsocketWriter) Write(p []byte) (n int, err error) {
	ww.mu.Lock()
	defer ww.mu.Unlock()
	// TODO: filter logs for output
	msg := p[:len(p)-1] // Strip trailing \n
	for _, ch := range ww.chans {
		*ch <- msg
	}
	return len(p), nil
}
