// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package health provides health monitoring of the MPC network
package health

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/rs/zerolog/log"
)

// Monitor monitors the health of all nodes in the pool
type Monitor struct {
	mux             sync.Mutex
	ms              transport.MessageSender
	pool            map[string]h.Config
	PoolStatus      map[string]string
	PoolStatusMutex sync.RWMutex
	listeners       []Listener
	forceCheck      chan bool
}

// Listener Interface method for health listeners
type Listener func(nodeName string, healthy bool, err error) error

// NewMonitor contructs a new Monitor
func NewMonitor(ms transport.MessageSender, pool map[string]h.Config) *Monitor {
	return &Monitor{ms: ms, pool: pool, PoolStatus: make(map[string]string), forceCheck: make(chan bool, 1)}
}

// Start runs the monitor in regular intervals
func (hm *Monitor) Start(interval time.Duration, unhealthyInterval time.Duration) chan bool {
	closeChan := make(chan bool, 1)
	checkPoolHealth := func() (pool []string, extPoolStatus map[string]map[string]string) {
		pool = make([]string, 0)
		extPoolStatus = make(map[string]map[string]string)
		for _, c := range hm.pool {
			// TODO: "GUI" is now hardcoded, but this is how it works now
			// Might change in the future to support multiple GUI's
			if c.Name == "GUI" {
				continue
			}
			pool = append(pool, c.Name)
			poolStatus, err := hm.ms.CheckHealth(c.Name)
			extPoolStatus[c.Name] = poolStatus
			hm.PoolStatusMutex.Lock()
			if err != nil {
				log.Warn().Err(err).Str("node", c.Name).Msg("health: peer compute node is not reachable")
				hm.PoolStatus[c.Name] = fmt.Sprintf("checkhealth error: %s", err.Error())
			} else {
				hm.PoolStatus[c.Name] = "ok"
			}
			hm.PoolStatusMutex.Unlock()
			hm.mux.Lock()
			for i := range hm.listeners {
				listenErr := hm.listeners[i](c.Name, (err == nil), err)
				if listenErr != nil {
					log.Warn().Err(listenErr).Msg("health: listener returned error")
				}
			}
			hm.mux.Unlock()
		}
		return
	}
	go func() {
		healthy := false
		close := false
		var extPoolStatus map[string]map[string]string
		for !close {
			tInterval := interval
			if !healthy {
				tInterval = unhealthyInterval
			}
			var pool []string
			var newExtPoolStatus map[string]map[string]string
			forceCheck := false
			select {
			case <-hm.forceCheck:
				forceCheck = true
				pool, newExtPoolStatus = checkPoolHealth()
			case <-time.After(tInterval):
				pool, newExtPoolStatus = checkPoolHealth()
			case <-closeChan:
				close = true
			}
			if forceCheck || extPoolStatus == nil || (extPoolStatus != nil && !reflect.DeepEqual(extPoolStatus, newExtPoolStatus)) {
				healthy = true
				l := log.Error()
				hm.PoolStatusMutex.RLock()
				for _, v := range hm.PoolStatus {
					if v != "ok" {
						healthy = false
					}
				}
				l.Interface("self", hm.PoolStatus)
				hm.PoolStatusMutex.RUnlock()
				for k, v := range newExtPoolStatus {
					for _, v2 := range v {
						if v2 != "ok" {
							healthy = false
						}
					}
					l.Interface(k, v)
				}
				if !healthy {
					l.Msg("health: MPC network is unhealthy")
				} else {
					log.Info().Interface("pool", pool).Msg("health: MPC network is fully connected and healthy")
				}
			}
			extPoolStatus = newExtPoolStatus
		}
	}()
	return closeChan
}

// RegisterListener registers a listener
func (hm *Monitor) RegisterListener(listener Listener) {
	hm.mux.Lock()
	defer hm.mux.Unlock()
	hm.listeners = append(hm.listeners, listener)
}

var l = &sync.Mutex{}
var timeoutTimer *time.Timer

// NodeConfigurationUpdate implements the coordinator.NodeConfigListener interface
// Forces a health check on the updated pool
func (hm *Monitor) NodeConfigurationUpdate(_, _, _, _, _ string) {
	l.Lock()
	if timeoutTimer == nil {
		timeoutTimer = time.NewTimer(server.ServerRestartTimeout + 1*time.Second)
	} else {
		timeoutTimer.Reset(server.ServerRestartTimeout + 1*time.Second)
		l.Unlock()
		return
	}
	l.Unlock()

	go func() {
		<-timeoutTimer.C
		l.Lock()
		timeoutTimer = nil
		hm.forceCheck <- true
		l.Unlock()
	}()
}
