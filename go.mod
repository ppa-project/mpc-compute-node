module ci.tno.nl/gitlab/ppa-project/mpc-compute-node

go 1.16

require (
	ci.tno.nl/gitlab/ppa-project/pkg/coordinator v1.0.16
	ci.tno.nl/gitlab/ppa-project/pkg/dpaillier v0.4.0
	ci.tno.nl/gitlab/ppa-project/pkg/http v1.0.3
	ci.tno.nl/gitlab/ppa-project/pkg/paillier v1.0.1
	ci.tno.nl/gitlab/ppa-project/pkg/scbindings v1.0.14
	ci.tno.nl/gitlab/ppa-project/pkg/types v1.0.5
	github.com/ethereum/go-ethereum v1.9.25
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/ianlancetaylor/cgosymbolizer v0.0.0-20210303021718-7cb7081f6b86
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/ugorji/go/codec v1.1.7
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)

replace (
	ci.tno.nl/gitlab/ppa-project/pkg/coordinator => ci.tno.nl/gitlab/ppa-project/pkg/coordinator.git v1.0.16
	ci.tno.nl/gitlab/ppa-project/pkg/dpaillier => ci.tno.nl/gitlab/ppa-project/pkg/dpaillier.git v0.4.0
	ci.tno.nl/gitlab/ppa-project/pkg/http => ci.tno.nl/gitlab/ppa-project/pkg/http.git v1.0.3
	ci.tno.nl/gitlab/ppa-project/pkg/paillier => ci.tno.nl/gitlab/ppa-project/pkg/paillier.git v1.0.1
	ci.tno.nl/gitlab/ppa-project/pkg/scbindings => ci.tno.nl/gitlab/ppa-project/pkg/scbindings.git v1.0.14
	ci.tno.nl/gitlab/ppa-project/pkg/secret => ci.tno.nl/gitlab/ppa-project/pkg/secret.git v0.5.0
	ci.tno.nl/gitlab/ppa-project/pkg/types => ci.tno.nl/gitlab/ppa-project/pkg/types.git v1.0.5
	github.com/ethereum/go-ethereum => ci.tno.nl/gitlab/ppa-project/pkg/go-ethereum.git v1.9.25-ppa-1
)
