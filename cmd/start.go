// Copyright © 2018 Maarten Everts (TNO) <maarten.everts@tno.nl>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"path"
	"runtime/debug"
	"sort"
	"strings"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/daemon"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/version"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start [nodename] [hostname]",
	Short: "Starts a MPC node",
	Args:  cobra.RangeArgs(0, 2),
	Long: `With the start command you can start a single MPC node using the provided configuration.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) >= 1 {
			viper.Set("nodename", args[0])
		}
		if len(args) >= 2 {
			viper.Set("host", args[1])
		}
		replaceEnv()
		printVersion()
		SetDefaults()
		daemon.Start()
	},
}

func printVersion() {
	if version.BuildDate != "" || version.GitCommit != "" || version.GitBranch != "" {
		fmt.Printf("MPC compute node - Git: %s/%s - Build date: %s", version.GitBranch, version.GitCommit, version.BuildDate)
	} else {
		fmt.Printf("MPC compute node - Dev")
	}

	bi, ok := debug.ReadBuildInfo()
	if !ok {
		fmt.Println()
		fmt.Println("Failed to read build info")
		return
	}

	if viper.GetBool("debug") {
		fmt.Println()
		for _, dep := range bi.Deps {
			if strings.HasPrefix(dep.Path, "ci.tno.nl") {
				fmt.Printf("%s version %s\n", dep.Path, dep.Version)
			}
		}
	} else {
		for _, dep := range bi.Deps {
			if dep.Path == "ci.tno.nl/gitlab/ppa-project/pkg/scbindings" {
				fmt.Printf(" - scbindings version %s\n", dep.Version)
			}
		}
	}
}

func replaceEnv() {
	// Read variables from environment variables
	// MPC_TEST_VAR becomes test.var in Viper
	viper.SetEnvPrefix("mpc")
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()
}

// SetDefaults sets the default configuration values
func SetDefaults() {
	// Check required variables
	requiredVars := []string{"nodename", "host"}
	for _, v := range requiredVars {
		if !viper.IsSet(v) || viper.GetString(v) == "" {
			log.Fatalf("Required variable %s is not set.\nSee --help to set the flag, or use an environment variable.\n", v)
		}
	}

	// Set default values
	nodename := viper.GetString("nodename")
	viper.SetDefault("logfile", fmt.Sprintf("mpc_node-%s.log", nodename))
	viper.SetDefault("version", "v1")

	viper.SetDefault("bindhost", "0.0.0.0")

	viper.SetDefault("dashboard.enabled", false)
	viper.SetDefault("dashboard.host", "127.0.0.1")
	viper.SetDefault("dashboard.bindhost", "0.0.0.0")
	viper.SetDefault("dashboard.port", 8000)

	viper.SetDefault("database.filename", fmt.Sprintf("/data/%s.csv", nodename))
	viper.SetDefault("database.metadatafilename", fmt.Sprintf("/data/%s.json", nodename))
	viper.SetDefault("database.intcolumns", []string{})

	viper.SetDefault("tls.certfile", path.Join("tls", nodename+".crt"))
	viper.SetDefault("tls.keyfile", path.Join("tls", nodename+".key"))

	// ethereum.privatekey
	viper.SetDefault("ethereum.url", "ws://0.0.0.0:0")
	// ethereum.smartcontract.address

	viper.SetDefault("quorum", true)

	viper.SetDefault("etcd.host", "127.0.0.1")
	viper.SetDefault("etcd.port", 2379)
	// etcd.cacert
	// etcd.clientcert
	// etcd.clientkey

	viper.SetDefault("ethereum.keyfile", "ethereumkey")
	viper.SetDefault("paillier.keyfile", "paillierkey.pub")
	viper.SetDefault("paillier.keysize", 2048)

	viper.SetDefault("dpaillier.keyfile", "dpaillierkey")
	viper.SetDefault("dpaillier.keysize", 2048)

	// Workaround to support unmarshaling of environment variables
	keys := viper.AllKeys()
	sort.Strings(keys)
	for _, key := range keys {
		val := viper.Get(key)
		viper.Set(key, val)
		if viper.GetBool("debug") {
			fmt.Printf("Using variable %v = %v\n", key, val)
		}
	}
}

func init() {
	startCmd.PersistentFlags().Int("port", 443, "Port number")
	viper.BindPFlag("port", startCmd.PersistentFlags().Lookup("port")) //nolint:errcheck,gosec

	startCmd.PersistentFlags().String("coordinator", "smartcontract", "Coordinator")
	viper.BindPFlag("coordinator", startCmd.PersistentFlags().Lookup("coordinator")) //nolint:errcheck,gosec

	rootCmd.AddCommand(startCmd)
}
