// Copyright © 2018 Maarten Everts (TNO) <maarten.everts@tno.nl>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package cmd defines the main applications of MPC compute node
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "mpc_node",
	Short: "A MPC (multi-party computation) computation node.",
	Long: `mpc_node is a (S)MPC (secure multi-party computation) node. It waits for queries
and tries to answer these queries (aggregations typically) together with the other
nodes.
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ./mpc_node.toml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Show detailed logging and print all configuration at startup")
	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug")) //nolint:errcheck,gosec

	rootCmd.PersistentFlags().BoolP("verbose", "v", false, "Show very detailed logging, including MPC HTTP traffic")
	viper.BindPFlag("verbose", rootCmd.PersistentFlags().Lookup("verbose")) //nolint:errcheck,gosec

	rootCmd.PersistentFlags().Bool("dev", false, "Allow to use fake data and fast but unsafe cryptography")
	viper.BindPFlag("dev", rootCmd.PersistentFlags().Lookup("dev")) //nolint:errcheck,gosec
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetConfigType("toml")
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in current directory with name mpc_node.toml
		viper.AddConfigPath(".")
		viper.SetConfigName("mpc_node")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil && viper.GetBool("debug") {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
