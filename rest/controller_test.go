// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

var router http.Handler

func TestMain(m *testing.M) {
	test.Setup()
	viper.SetConfigFile("../test/testdata/PartyA/mpc_node-localhost.toml")
	viper.ReadInConfig()

	var c h.Config
	var pool map[string]h.Config

	service := service.NewService()
	service.SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return nil, nil
	})

	hc := server.NewHTTPControllerWithClientAuth(c, pool)
	rc := NewRESTController(hc, service, nil)
	router = rc.GetRouter()

	code := m.Run()
	os.Exit(code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr
}

func TestRoot(t *testing.T) {
	req, _ := http.NewRequest("GET", "/", nil)
	response := executeRequest(req)

	assert.Equal(t, response.Code, http.StatusOK)
	assert.NotNil(t, response.Body)
}

func TestStatus(t *testing.T) {
	req, _ := http.NewRequest("GET", "/status", nil)
	response := executeRequest(req)

	assert.Equal(t, response.Code, http.StatusOK)
	assert.NotNil(t, response.Body)
}

func TestQuerySubRouter(t *testing.T) {
	req, _ := http.NewRequest("POST", "/testing/query/silly", nil)
	response := executeRequest(req)

	assert.NotEqual(t, response.Code, http.StatusNotFound)
	assert.NotNil(t, response.Body)
}
