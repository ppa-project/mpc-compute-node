// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"net/http"

	"github.com/rs/zerolog/log"

	"github.com/gin-gonic/gin"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
)

func (rc *RESTController) addQueryHandlers(r *gin.RouterGroup) {
	r.Use(func(c *gin.Context) {
		_, ok := c.Get("role")
		if !ok {
			log.Debug().Msg("No role set")
		}
		c.Next()
	})
	r.GET("/:queryID/result", rc.handleQueryResult)
	r.Use(server.HasRole(h.Node))
	{
		r.POST("/", rc.handleNewQuery)
		r.GET("/:queryID", rc.handleQuery)

		r.POST("/:queryID/abort", rc.handleAbort)
		r.POST("/:queryID/step/:step", rc.handleQueryStep)
	}
}

func (rc *RESTController) handleNewQuery(c *gin.Context) {
	body, _ := c.GetRawData()
	err := rc.mpcService.StartProtocolFromSerializedInputs(c.Param("protocol"), body)
	if err != nil {
		server.Error(c, http.StatusBadRequest, err.Error())
	} else {
		server.Success(c)
	}
}

func (rc *RESTController) handleQuery(c *gin.Context) {
	c.Set("step", "0")
	rc.handleQueryStep(c)
}

func (rc *RESTController) handleQueryStep(c *gin.Context) {
	body, _ := c.GetRawData()

	queryID := c.Param("queryID")
	step := c.Param("step")
	if step == "" {
		step = c.MustGet("step").(string)
	}

	msg := protocol.NewMessage(
		c.Param("protocol"),
		c.MustGet("sender").(string),
		c.MustGet("receiver").(string),
		queryID,
		step,
		body,
	)

	err := rc.mpcService.RouteMessage(c.Param("protocol"), msg)
	switch err {
	case nil:
		server.Success(c)
	case service.ErrUnknownQuery:
		server.Error(c, http.StatusConflict, err.Error())
	default:
		server.Error(c, http.StatusBadRequest, err.Error())
	}
}

func (rc *RESTController) handleQueryResult(c *gin.Context) {
	result, err := rc.mpcService.GetResult(c.Param("protocol"), c.Param("queryID"))
	if err != nil {
		server.Error(c, http.StatusNotFound, "Failed to get query result")
		return
	}
	c.JSON(http.StatusOK, result)
}

func (rc *RESTController) handleAbort(c *gin.Context) {
	server.Error(c, http.StatusNotImplemented, "Not implemented")
}
