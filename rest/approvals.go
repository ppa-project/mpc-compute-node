// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"fmt"
	"net/http"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/rest/messages"
	"github.com/gin-gonic/gin"
)

func (rc *RESTController) addApprovalHandlers(r *gin.RouterGroup) {
	r.GET("", rc.GetApprovalRequests)
	r.GET("/:queryID", rc.GetApprovalRequest)
	r.POST("/:queryID", rc.PostApprovalResponse)
}

// GetApprovalRequests godoc
// @Summary Get approval requests
// @Description Get approval requests
// @Tags Approval
// @Success 200 {object} messages.JSONApprovalRequests
// @Success 400 {object} messages.JSONResultError
// @Router /approvals [get]
func (rc *RESTController) GetApprovalRequests(c *gin.Context) {
	approvalRequests := rc.mpcService.ListApprovalRequests("basicstats")
	c.JSON(http.StatusOK, &messages.JSONApprovalRequests{ApprovalRequests: approvalRequests})
}

// GetApprovalRequest godoc
// @Summary Get approval request
// @Description Get approval request
// @Tags Approval
// @Success 200 {object} protocol.ApprovalRequest
// @Success 400 {object} messages.JSONResultError
// @Router /approvals/{queryID} [get]
func (rc *RESTController) GetApprovalRequest(c *gin.Context) {
	approvalRequest := rc.mpcService.GetApprovalRequest("basicstats", c.Param("queryID"))
	c.JSON(http.StatusOK, approvalRequest)
}

// PostApprovalResponse godoc
// @Summary Submit approval response
// @Description Submit approval response
// @Tags Approval
// @Accept json
// @Produce json
// @Param query body messages.JSONApprovalResponse true "Query"
// @Success 200 {object} messages.JSONResultOK
// @Success 400 {object} messages.JSONResultError
// @Router /approvals/{queryID} [post]
func (rc *RESTController) PostApprovalResponse(c *gin.Context) {
	var approvalResponse messages.JSONApprovalResponse

	if err := c.ShouldBindJSON(&approvalResponse); err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: err.Error()})
		return
	}

	err := rc.mpcService.SubmitReview("basicstats", c.Param("queryID"), *approvalResponse.Approve)
	if err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: fmt.Sprintf("Failed to submit review: %s", err.Error())})
		return
	}

	c.JSON(http.StatusOK, messages.JSONResultOK{Success: true})
}
