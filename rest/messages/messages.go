// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package messages contains common REST messages used as JSON model
package messages

import "ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"

// JSONResultOK defines a result OK
type JSONResultOK struct {
	Success bool `json:"success"`
}

// JSONResultError defines a result error
type JSONResultError struct {
	Error string `json:"error"`
}

// JSONApprovalRequests defines a list of ApprovalRequests
type JSONApprovalRequests struct {
	ApprovalRequests []service.ApprovalRequestSummary `json:"approvalRequests"`
}

// JSONApprovalResponse defines the request body of an approval response
type JSONApprovalResponse struct {
	Approve *bool `json:"approve" binding:"required"`
}

// JSONVersion defines the response body of a version request
type JSONVersion struct {
	GitBranch string `json:"git_branch"`
	GitCommit string `json:"git_commit"`
	BuildDate string `json:"build_date"`
}
