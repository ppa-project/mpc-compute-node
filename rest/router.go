// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package rest implements the various HTTP endpoints supported by the MPC node.
package rest

import (
	"fmt"
	"net/http"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/health"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/messages"
	msgs "ci.tno.nl/gitlab/ppa-project/mpc-compute-node/rest/messages"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/version"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// RESTController is responsible for serving REST to others and is the gateway for incomming communication
type RESTController struct { //nolint:golint
	hc            *server.HTTPController
	mpcService    *service.Service
	healthMonitor *health.Monitor
}

// NewRESTController Constructor for RESTController
func NewRESTController(hc *server.HTTPController, mpcService *service.Service, healthMonitor *health.Monitor) *RESTController {
	return &RESTController{hc, mpcService, healthMonitor}
}

// GetRouter returns the main router / handler
func (rc *RESTController) GetRouter() http.Handler {
	if !viper.GetBool("debug") {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	// Routes consist of a path and a handler function.
	r.StaticFS("/static/", http.Dir("static"))

	r.GET("/", rc.handleRootRequest)
	r.GET("/status", rc.handleStatus)
	r.GET("/version", rc.handleVersion)

	r.Use(server.RouteMetrics())
	r.Use(server.AuthCheck())
	r.Use(rc.hc.InjectClientIdentity())
	r.Use(server.RouteLogIncoming())
	r.Use(server.LogIncomingIPs()) // TODO: We might want to downgrade this to Debug logging later on

	r.GET("/health", rc.handleHealth)
	s := r.Group(fmt.Sprintf("/%s/query", viper.GetString("version")))
	rc.addProtocolHandlers(s)

	// approval
	approvalGroup := r.Group(fmt.Sprintf("/%s/approvals", viper.GetString("version")))
	rc.addApprovalHandlers(approvalGroup)

	return r
}

// Someone just asks for GET HTTP/2.0 /
// Send a terse response indicating the server is up
func (rc *RESTController) handleRootRequest(c *gin.Context) {
	c.String(http.StatusOK, fmt.Sprintf("This is MPC-node \"%v\".", viper.GetString("nodename")))
}

// Someone requests the node's current status
// The node's organization name and database attributes are sent back
func (rc *RESTController) handleStatus(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"Name": viper.GetString("nodename"),
	})
}

// Return the version of the MPC node
func (rc *RESTController) handleVersion(c *gin.Context) {
	c.JSON(http.StatusOK, msgs.JSONVersion{
		GitBranch: version.GitBranch,
		GitCommit: version.GitCommit,
		BuildDate: version.BuildDate,
	})
}

// A compute node is checking whether this node is up
// Send a terse response indicating the server is up
func (rc *RESTController) handleHealth(c *gin.Context) {
	var neighbors map[string]string
	if rc.healthMonitor != nil {
		rc.healthMonitor.PoolStatusMutex.RLock()
		defer rc.healthMonitor.PoolStatusMutex.RUnlock()
		neighbors = rc.healthMonitor.PoolStatus
	}
	c.JSON(http.StatusOK, &messages.HealthResponse{
		Status:    "OK",
		Neighbors: neighbors,
	})
}
