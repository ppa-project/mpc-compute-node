// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	"github.com/stretchr/testify/assert"
)

func TestQueryIDUnknown(t *testing.T) {
	helper.InstantiateSillyProtocol()
	c, pool := helper.Daemons["PartyA"].GetConfig("PartyA")

	ms, err := transport.NewMessageSenderHTTP(c, pool)
	assert.NoError(t, err)

	message := &protocol.Message{
		Protocol: "silly",
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1a2b3c",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err = ms.SendMessage(message)
	assertHTTPError(t, err, 409) // HTTP Status Conflict
}
