// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"testing"
	"time"
)

func TestHTTPMultipleServicesSilly(t *testing.T) {
	helper.InstantiateSillyProtocol()

	for _, d := range helper.Daemons {
		// Let all nodes know we've started
		d.Service.StartProtocol("silly", "1", "PartyA", nil)
	}

	// And they should be off by now!
	// Let's wait for the messages to clear
	time.Sleep(1 * time.Second)
}
