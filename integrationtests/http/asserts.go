// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package http contains all HTTP integrationtests
package http

import (
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	"github.com/stretchr/testify/assert"
)

func assertHTTPError(t *testing.T, err error, statuscode int) {
	e, ok := err.(*transport.HTTPError)
	assert.True(t, ok)
	assert.Equal(t, statuscode, e.StatusCode)
}

func assertNotHTTPError(t *testing.T, err error, statuscode int) {
	e, ok := err.(*transport.HTTPError)
	assert.True(t, ok)
	assert.NotEqual(t, statuscode, e.StatusCode)
}
