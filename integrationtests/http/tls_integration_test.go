// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"fmt"
	"net/url"
	"os"
	"strings"
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/integrationtests"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"github.com/stretchr/testify/assert"
)

var helper integrationtests.Helper

var pool = []string{"PartyA", "PartyC", "PartyB"}

func TestMain(m *testing.M) {
	test.Setup()

	helper = integrationtests.NewHelper(pool)

	helper.SpawnServers()

	code := m.Run()

	helper.Close()

	// Remove query result files
	os.RemoveAll(service.ResultDir)

	os.Exit(code)
}
func TestTLSOK(t *testing.T) {
	ms := helper.Daemons["PartyA"].MessageSender
	message := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1a2b3c",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err := ms.SendMessage(message)
	assertNotHTTPError(t, err, 403)
}

// Sender field is not used in ms.SendMessage()
func TestTLSSpoofedLegitimateSender(t *testing.T) {
	ms := helper.Daemons["PartyA"].MessageSender
	message := &protocol.Message{
		Sender:   "PartyC", // Unused field during send
		Receiver: "PartyB",
		QueryID:  "1a2b3c",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err := ms.SendMessage(message)
	assertNotHTTPError(t, err, 403)
}

func TestServerTLSCertNotInCertPool(t *testing.T) {
	c, pool := helper.Daemons["PartyB"].GetConfig("PartyB")
	pool["PartyB"] = c

	testDir, _ := os.Getwd()
	os.Chdir(fmt.Sprintf("../config/%s", "PartyB"))
	defer os.Chdir(testDir)

	ms, err := transport.NewMessageSenderHTTP(c, pool)
	assert.NoError(t, err)

	message := &protocol.Message{
		Sender:   "Eve", // Unused field during send
		Receiver: "PartyB",
		QueryID:  "1a2b3c",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err = ms.SendMessage(message)
	assert.Error(t, err)
}

func TestClientTLSWrongCertServerSide(t *testing.T) {
	c, pool := helper.Daemons["PartyA"].GetConfig("PartyA")

	configPartyB := pool["PartyB"]
	configPartyB.Port = pool["PartyC"].Port
	pool["PartyB"] = configPartyB

	testDir, _ := os.Getwd()
	os.Chdir(fmt.Sprintf("../config/%s", "PartyA"))
	defer os.Chdir(testDir)

	ms, err := transport.NewMessageSenderHTTP(c, pool)
	assert.NoError(t, err)

	message := &protocol.Message{
		Sender:   "PartyA", // Unused field during send
		Receiver: "PartyB",
		QueryID:  "1a2b3c",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err = ms.SendMessage(message)
	assert.NotNil(t, err)
	assert.IsType(t, (*transport.HTTPError)(nil), err)
	httpError, ok := err.(*transport.HTTPError)
	assert.True(t, ok)
	assert.IsType(t, (*url.Error)(nil), httpError.Err)
	assert.True(t, strings.Contains(err.Error(), "certificate signed by unknown authority"))
}
