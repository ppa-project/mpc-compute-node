// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package integrationtests contains integration tests for the MPC node.
// These check that the components work together properly.
package integrationtests

import (
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/http"
)

// Helper helps the setup a test environment with multiple test daemons
type Helper struct {
	Daemons map[string]*TestDaemon
	Pool    []string
	Configs []http.Config
}

// NewHelper construct a new Helper
func NewHelper(pool []string) Helper {
	h := Helper{
		Daemons: make(map[string]*TestDaemon),
		Pool:    pool,
		Configs: test.GetTestHTTPConfigs(),
	}
	for _, nodename := range pool {
		d := NewTestDaemon(nodename, pool, h.Configs)
		h.Daemons[nodename] = &d
	}
	return h
}

// SpawnServers spawns the servers of all test daemons
func (h *Helper) SpawnServers() {
	for _, d := range h.Daemons {
		d.SpawnServer()
	}
}

// InstantiateDpaiKeygenProtocol instantiates the keygen protocol for all test daemons
func (h *Helper) InstantiateDpaiKeygenProtocol() {
	for _, d := range h.Daemons {
		d.InstantiateDpaiKeygenProtocol()
	}
}

// InstantiateDpaiDecryptProtocol instantiates the decrypt protocol for all test daemons
func (h *Helper) InstantiateDpaiDecryptProtocol(sks []*dpaillier.PrivateKeyShare) {
	i := 0
	for _, d := range h.Daemons {
		d.InstantiateDpaiDecryptProtocol(sks[i])
		i++
	}
}

// InstantiateBasicStatsProtocol instantiates the basic stats protocol for all test daemons
func (h *Helper) InstantiateBasicStatsProtocol() {
	for _, d := range h.Daemons {
		d.InstantiateBasicStatsProtocol()
	}
}

// InstantiateSillyProtocol instantiates the silly protocol for all test daemons
func (h *Helper) InstantiateSillyProtocol() {
	for _, d := range h.Daemons {
		d.InstantiateSillyProtocol()
	}
}

// Close closes the servers of all test daemons
func (h *Helper) Close() {
	for _, d := range h.Daemons {
		_ = d.Server.Close()
	}
}
