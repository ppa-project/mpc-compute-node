// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package integrationtests

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/spf13/viper"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/basicstats"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaikeygen"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/rest"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
)

// TestDaemon mocks the daemon for testing
type TestDaemon struct {
	NodeName       string
	Pool           []string
	Configs        []h.Config
	Server         *http.Server
	HTTPController *server.HTTPController
	Service        *service.Service
	MessageSender  *transport.MessageSenderHTTP
}

// NewTestDaemon constructs a new TestDaemon
func NewTestDaemon(nodename string, pool []string, configs []h.Config) TestDaemon {
	return TestDaemon{NodeName: nodename, Pool: pool, Configs: configs}
}

// GetConfig returns the configurations for a given party
func (td *TestDaemon) GetConfig(party string) (h.Config, map[string]h.Config) {
	testDir, _ := os.Getwd()
	err := os.Chdir(fmt.Sprintf("../../test/testdata/%s", party))
	if err != nil {
		fmt.Printf("Failed to chdir: %s", err)
	}
	defer func() {
		if err = os.Chdir(testDir); err != nil {
			fmt.Printf("Failed to chdir: %s", err)
		}
	}()

	viper.Set("version", "testing")

	httpConfig, pool := test.GetHTTPConfig(td.Configs, party)

	httpConfig.TLSCertFile, _ = filepath.Abs(httpConfig.TLSCertFile)
	httpConfig.TLSKeyFile, _ = filepath.Abs(httpConfig.TLSKeyFile)

	for _, node := range pool {
		node.TLSCertFile, _ = filepath.Abs(node.TLSCertFile)
		node.TLSKeyFile, _ = filepath.Abs(node.TLSKeyFile)
		pool[node.Name] = node
	}

	return httpConfig, pool
}

// SpawnServer starts the test daemon server with default configs
func (td *TestDaemon) SpawnServer() {
	c, pool := td.GetConfig(td.NodeName)
	td.SpawnServerWithConfigs(td.NodeName, c, pool)
}

// SpawnServerWithConfigs starts the test daemon server with custom configs
func (td *TestDaemon) SpawnServerWithConfigs(party string, c h.Config, pool map[string]h.Config) {
	td.Service = service.NewService()
	td.Service.Precomputer = pailliercache.NewPrecomputer()
	td.Service.NodeName = td.NodeName
	p := make([]string, len(td.Pool))
	copy(p, td.Pool)
	td.Service.OrganizationList = func() ([]string, error) { return p, nil }

	td.MessageSender, _ = transport.NewMessageSenderHTTPWithDelay(c, pool, 5*time.Second, 120)
	td.Service.MessageSender = td.MessageSender

	td.HTTPController = server.NewHTTPControllerWithClientAuth(c, pool)
	rc := rest.NewRESTController(td.HTTPController, td.Service, nil)
	td.Server, _ = td.HTTPController.CreateHTTPServerWithRouter(rc.GetRouter())

	td.Server.RegisterOnShutdown(func() {
		fmt.Println("Shutdown!")
	})

	go func() {
		err := td.Server.ListenAndServeTLS(c.TLSCertFile, c.TLSKeyFile)
		if err != nil {
			fmt.Printf("%v\n", err)
		}
	}()
}

// SetupDatabase returns a testdatabase containing different attribute patterns
func (td *TestDaemon) SetupDatabase(thisNodeName string) database.Database {
	tdb := database.NewTestDatabase(thisNodeName, 10)
	tdb.FillAttributePattern1("a1", 1)
	tdb.FillAttributePattern2("a2", 1)
	tdb.FillAttributePattern3("a3", 1)
	tdb.FillAttributePattern2s("a4", "a", "z")
	return tdb
}

// InstantiateSillyProtocol sets up the silly protocol instantiator
func (td *TestDaemon) InstantiateSillyProtocol() {
	idx := -1
	for i, nodename := range td.Pool {
		if nodename == td.NodeName {
			idx = (i + 1) % len(td.Pool)
		}
	}
	td.Service.SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol(td.NodeName, td.Pool[idx], td.MessageSender), nil
	})
}

// InstantiateBasicStatsProtocol sets up the basicstats protocol instantiator
func (td *TestDaemon) InstantiateBasicStatsProtocol() {
	db := td.SetupDatabase(td.NodeName)
	precomputer := td.Service.Precomputer

	pool := make([]string, 0, len(td.Pool)-1)
	for _, nodename := range td.Pool {
		if nodename != td.NodeName {
			pool = append(pool, nodename)
		}
	}
	name := td.NodeName

	td.Service.SetProtocolInstantiator("basicstats", func() (protocol.StateMachine, error) {
		return basicstats.NewStateMachine(db, name, func() ([]string, error) { return pool, nil }, td.MessageSender, precomputer), nil
	})
}

// InstantiateDpaiDecryptProtocol sets up the dpaidecrypt protocol instantiator
func (td *TestDaemon) InstantiateDpaiDecryptProtocol(keyshare *dpaillier.PrivateKeyShare) {
	thisNodeI := -1
	for i, nodename := range td.Pool {
		if nodename == td.NodeName {
			thisNodeI = i
			break
		}
	}

	// We copy the pool, since it somehow gets overwritten otherwise
	pool := make([]string, len(td.Pool))
	copy(pool, td.Pool)

	td.Service.SetProtocolInstantiator("dpaidecrypt", func() (protocol.StateMachine, error) {
		return dpaidecrypt.NewStateMachine(thisNodeI, pool, keyshare, td.MessageSender), nil
	})
}

// InstantiateDpaiKeygenProtocol sets up the dpaikeygen protocol instantiator
func (td *TestDaemon) InstantiateDpaiKeygenProtocol() {
	thisNodeI := -1
	for i, nodename := range td.Pool {
		if nodename == td.NodeName {
			thisNodeI = i
			break
		}
	}

	// We copy the pool, since it somehow gets overwritten otherwise
	pool := make([]string, len(td.Pool))
	copy(pool, td.Pool)

	td.Service.SetProtocolInstantiator("dpaikeygen", func() (protocol.StateMachine, error) {
		return dpaikeygen.NewStateMachine(thisNodeI, pool, td.MessageSender), nil
	})
}
