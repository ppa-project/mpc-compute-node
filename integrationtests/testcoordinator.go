// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package integrationtests

import (
	"context"
	"errors"
	"io"
	"io/ioutil"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"

	"github.com/ethereum/go-ethereum/common"
)

// TestCoordinator mocks the coordinator for integration testing. It currently
// supports only the GetOrganizations and mutex-related features.
type TestCoordinator struct {
	Orgs     []string
	IsLocked bool
	Holder   common.Address
	Version  string
}

// NewTestCoordinator returns a new testing coordinator
func NewTestCoordinator(orgs []string) *TestCoordinator {
	return &TestCoordinator{Orgs: orgs}
}

// DPaillierLock locks the fake smart contract mutex or returns an error if it's already locked.
func (tc *TestCoordinator) DPaillierLock(context.Context) error {
	if tc.IsLocked {
		return errors.New("double lock")
	}
	tc.IsLocked = true
	tc.Holder = common.HexToAddress("0x1234")
	return nil
}

// DPaillierUnlock unlocks the fake smart contract mutex or returns an error if it's already unlocked.
// The given version string is set as the new "dpaillier key version".
func (tc *TestCoordinator) DPaillierUnlock(_ context.Context, ver string) error {
	if !tc.IsLocked {
		return errors.New("double unlock")
	}
	tc.Version = ver
	tc.Holder = common.HexToAddress("0x0")
	tc.IsLocked = false
	return nil
}

// DPaillierRead returns the current dpaillier key version and (fake) lock holder. Never fails.
func (tc *TestCoordinator) DPaillierRead(context.Context) (string, common.Address, error) {
	return tc.Version, tc.Holder, nil
}

// GetOrganizations returns the list of organizations given to the TestCoordinator's constructor.
func (tc *TestCoordinator) GetOrganizations() ([]string, error) { return tc.Orgs, nil }

// RegisterQueryListener is unimplemented.
func (tc *TestCoordinator) RegisterQueryListener(ql coordinator.QueryListener) {}

// RegisterNodeConfigListener is unimplemented.
func (tc *TestCoordinator) RegisterNodeConfigListener(nl coordinator.NodeConfigListener) {}

// RegisterGuiConfigListener is unimplemented.
func (tc *TestCoordinator) RegisterGuiConfigListener(nl coordinator.GuiConfigListener) {}

// RegisterAuditLogEntryListener is unimplemented.
func (tc *TestCoordinator) RegisterAuditLogEntryListener(alel coordinator.AuditLogEntryListener) {}

// Writer is unimplemented.
func (tc *TestCoordinator) Writer() io.Writer { return ioutil.Discard }

// ConnectAndListen is unimplemented.
func (tc *TestCoordinator) ConnectAndListen() error { return nil }

// Close is unimplemented.
func (tc *TestCoordinator) Close() {}

// UpdateNodeConfig is unimplemented.
func (tc *TestCoordinator) UpdateNodeConfig(ip, port, cert, paillierkey string) error { return nil }

// UpdateGuiConfig is unimplemented.
func (tc *TestCoordinator) UpdateGuiConfig(ip, cert string) error { return nil }

// UpdateHealth is unimplemented.
func (tc *TestCoordinator) UpdateHealth(nodeName string, healthy bool, err error) error { return nil }

// GetNodeConfig is unimplemented.
func (tc *TestCoordinator) GetNodeConfig(organization string) (*coordinator.LogEntryPresenceMPC, error) {
	return nil, nil
}

// GetGuiConfig is unimplemented.
func (tc *TestCoordinator) GetGuiConfig(organization string) (*coordinator.LogEntryPresenceGUI, error) {
	return nil, nil
}

// AuditLogLen is unimplemented.
func (tc *TestCoordinator) AuditLogLen(filter coordinator.LogKind) int { return 0 }

// AuditLog is unimplemented.
func (tc *TestCoordinator) AuditLog(filter coordinator.LogKind, start, count int) []coordinator.LogEntry {
	return nil
}

// SubmitNewQuery is unimplemented.
func (tc *TestCoordinator) SubmitNewQuery(newQuery types.Query, metadata coordinator.NewQueryMetadata) (string, error) {
	return "", nil
}

// GetQueryDetails is unimplemented.
func (tc *TestCoordinator) GetQueryDetails(queryid string) (coordinator.QueryDetails, error) {
	return coordinator.QueryDetails{}, nil
}

// GetBusinessRules is unimplemented.
func (tc *TestCoordinator) GetBusinessRules() []string { return nil }
