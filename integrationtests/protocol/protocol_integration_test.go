// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package protocol

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"os"
	"sync"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/cmd"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"

	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/integrationtests"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/basicstats"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

var helper integrationtests.Helper

var pool = []string{"PartyA", "PartyC", "PartyB"}

func TestMain(m *testing.M) {
	test.Setup()
	viper.Set("nodename", "nodename")
	viper.Set("host", "1.2.3.4")
	cmd.SetDefaults()

	helper = integrationtests.NewHelper(pool)

	helper.SpawnServers()

	code := m.Run()

	helper.Close()

	os.RemoveAll(service.ResultDir)

	os.Exit(code)
}

var dpaiTempfile string

func prepareDpaiKeys(t *testing.T) []*dpaillier.PrivateKeyShare {
	// prepare key shares
	var sks []*dpaillier.PrivateKeyShare
	smallDPaillierKeyJSON := []byte(`[{"N":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045617,"N2":209778646656642296749271564169716435154206000181211901645845443990756859382320064980049240628332055945358709270747994619573635753086588547275587543271938397783656541165017788073640113288482181797550603267318822456087455929997605515580768956278954944585073591734595899946148162222240596287954732721930208112312854463148379865558119323961850179358799056925591237458794165021393699203638906531536673975383374930361137806155186920045779641195790221069185409368028797096857693050933438144818025299526054991127294968689142875913556427115831802273500354086032078214284552267131120196170541534322049233362893389698713338910689,"Nplus1":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045618,"ParticipantIndex":1,"Hi":38598618361058290689683552551252671895009064433726420435883281930830942960130879311742900231874810616430701804809704481260853294142351452033848978297075083456515422921384446023856526995039522595415539369131876340898085962620736264353936672001742469362156119389616283663783330976935699947190370105024756337060974061892812915168663013539202976090506237228059975834699090221620892450170670607780349874509555289848066591632833898194928004740185449669368278491345074057284439268591769567145134857671694843934262707288500941029647613273012048389830442449015555586473613248701862190180451853573253856789136124321522560237345573521357015570,"FactorialOfNPart":6,"ThetaInv":219903885260910069580562022824807889139720993552095965227890474386129218532133953093489324621467850598988548432944660854936620703027344690851403487412929721812204182028503006805571433757853541183190091809872338767793747425128611423543182636857919809234268578486208003297193234800497776345679002624207800062207},{"N":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045617,"N2":209778646656642296749271564169716435154206000181211901645845443990756859382320064980049240628332055945358709270747994619573635753086588547275587543271938397783656541165017788073640113288482181797550603267318822456087455929997605515580768956278954944585073591734595899946148162222240596287954732721930208112312854463148379865558119323961850179358799056925591237458794165021393699203638906531536673975383374930361137806155186920045779641195790221069185409368028797096857693050933438144818025299526054991127294968689142875913556427115831802273500354086032078214284552267131120196170541534322049233362893389698713338910689,"Nplus1":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045618,"ParticipantIndex":2,"Hi":154394364835152049154388714914151234206762514555851936976356424366771256333172954511796368565424555716790194333309932879887465481957050361053739261546893001278059583528952134670432967247304833411656444332551449616150259520273575087544293304572294817269493273473472207098285808404419444037982464655214723953490795076380438752726896529545429491799239606559764030671837411598017470129779327397069512898727407705220414858631240523276111946432069384666966018662150539883227305823205901821422938842805239581481419800796654471768666510490343747108579832963393825170700525678431462249888306920728065186925938890478740987053048122059641329728,"FactorialOfNPart":6,"ThetaInv":219903885260910069580562022824807889139720993552095965227890474386129218532133953093489324621467850598988548432944660854936620703027344690851403487412929721812204182028503006805571433757853541183190091809872338767793747425128611423543182636857919809234268578486208003297193234800497776345679002624207800062207},{"N":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045617,"N2":209778646656642296749271564169716435154206000181211901645845443990756859382320064980049240628332055945358709270747994619573635753086588547275587543271938397783656541165017788073640113288482181797550603267318822456087455929997605515580768956278954944585073591734595899946148162222240596287954732721930208112312854463148379865558119323961850179358799056925591237458794165021393699203638906531536673975383374930361137806155186920045779641195790221069185409368028797096857693050933438144818025299526054991127294968689142875913556427115831802273500354086032078214284552267131120196170541534322049233362893389698713338910689,"Nplus1":458015989520717820921012121200193719018902371651812371072148553546200698723108137077741917652620659018713911347946087870231744066639334789456730482471395538006934688743649262987653211542829037727187360606225526151298314894139662210028794403218505322372487876827000857630140590067026594394717406460713437045618,"ParticipantIndex":3,"Hi":347387239422294433101309812711561729275011722190046075957118486155680565699103223987227663540274327421863868164151369531715506251755469070164345674211057511998573469894214609022062512926016925942331045961692405350993140922880344432704059967663853793662427921273975001032652642716947036942205998059200054247796622279705046230266808651161601253235334132473057965538945944160162239747599237557800532390970206850090365625397710158536860443009379992579062359319951257092542444691158302287961620788838211401207162514150880297591190758763008623275694210065384648438477063922757643541650741259914383935551782575717839844366238145165798836410,"FactorialOfNPart":6,"ThetaInv":219903885260910069580562022824807889139720993552095965227890474386129218532133953093489324621467850598988548432944660854936620703027344690851403487412929721812204182028503006805571433757853541183190091809872338767793747425128611423543182636857919809234268578486208003297193234800497776345679002624207800062207}]`)
	assert.NoError(t, json.Unmarshal(smallDPaillierKeyJSON, &sks))
	// prepare file for the share basicstats uses
	tmpfile, err := ioutil.TempFile("", "dpk")
	dpaiTempfile = tmpfile.Name()
	assert.NoError(t, err)

	// store the share in the temp file
	viper.Set("dpaillier.keyfile", tmpfile.Name())
	pailliercache.StoreSecretKeyShare("test", sks[0])
	// check that it's there
	tag, sks0, err := pailliercache.OwnSecretKeyShare()
	assert.NoError(t, err)
	assert.Equal(t, "test", tag)
	assert.Equal(t, sks[0], sks0)
	return sks
}

func removeDpaiKeys() {
	os.Remove(dpaiTempfile)
}

func RunProtocol(t *testing.T, protocolName string, queryID string, payload interface{}, options map[string]interface{}) (result *service.ProtocolResult) {
	doneChan := make(chan bool, 1)

	// Start query
	for _, nodename := range pool {
		nodename := nodename
		cb := func(madeByUs protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
			helper.Daemons[nodename].Service.GetCompletedCallback(protocolName, queryID, []string{})(madeByUs, resultKind, payload)
			if resultKind != protocol.SubProtocol {
				doneChan <- true
			}
		}
		err := helper.Daemons[nodename].Service.StartProtocolWithCallback(protocolName, queryID, payload, options, cb)
		assert.NoError(t, err)
	}

	// Wait for CompletedCallback
	count := len(pool)
	for count != 0 {
		select {
		case <-time.After(25 * time.Second):
			assert.FailNow(t, "Test timeout")
		case <-doneChan:
			count--
		}
	}

	time.Sleep(time.Second)

	// Get result
	for _, nodename := range pool {
		var err error
		result, err = helper.Daemons[nodename].Service.GetResult(protocolName, queryID)
		assert.NoError(t, err)
		assert.NotNil(t, result)
	}
	return result
}

func TestSillyResult(t *testing.T) {
	helper.InstantiateSillyProtocol()
	queryID := "1234"
	payload := &silly.StartPayload{ID: queryID, Starter: "PartyA"}

	result := RunProtocol(t, "silly", queryID, payload, nil)
	assert.NotNil(t, result)
	assert.Empty(t, result.AbortMessage)
	assert.Equal(t, 3, result.Result)
}

func TestBasicStatsCount(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"COUNT\"], \"filters\": [\"PartyA:a1 > 0\", \"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}

	result := RunProtocol(t, "basicstats", queryID, payload, nil)
	assert.NotNil(t, result)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(5), result.Result.(*basicstats.Result).Count)
	}
}

func TestBasicStatsSum(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}

	result := RunProtocol(t, "basicstats", queryID, payload, nil)
	assert.NotNil(t, result)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(5), result.Result.(*basicstats.Result).Count)
		assert.Equal(t, float64(5), result.Result.(*basicstats.Result).Aggregates[0][0])
	}
}

func TestBasicStatsMean(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"MEAN(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}

	result := RunProtocol(t, "basicstats", queryID, payload, nil)
	assert.NotNil(t, result)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(5), result.Result.(*basicstats.Result).Count)
		assert.Equal(t, float64(1), result.Result.(*basicstats.Result).Aggregates[0][0])
	}
}

func TestBasicStatsMeanStdev(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"MEANSTDEV(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}

	result := RunProtocol(t, "basicstats", queryID, payload, nil)
	assert.NotNil(t, result)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(5), result.Result.(*basicstats.Result).Count)
		assert.Equal(t, float64(1), result.Result.(*basicstats.Result).Aggregates[0][0])
		assert.Equal(t, float64(0), result.Result.(*basicstats.Result).Aggregates[0][1])
	}
}

func TestBasicStatsPercentageConstraintsOK(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{
		"percentageConstraints": []coordinator.PercentageConstraint{{
			Owner:         "PartyA",
			Attribute:     "a1",
			MinPercentage: big.NewInt(40),
			MaxPercentage: big.NewInt(90),
		}, {
			Owner:         "PartyA",
			Attribute:     "a2",
			MinPercentage: big.NewInt(60),
			MaxPercentage: big.NewInt(90),
		}},
	}

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(5), result.Result.(*basicstats.Result).Count)
		assert.Equal(t, float64(5), result.Result.(*basicstats.Result).Aggregates[0][0])
	}
}

func TestBasicStatsPercentageConstraintsFailLower(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{
		"percentageConstraints": []coordinator.PercentageConstraint{{
			Owner:         "PartyA",
			Attribute:     "a1",
			MinPercentage: big.NewInt(60),
			MaxPercentage: big.NewInt(90),
		}},
	}

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	assert.False(t, result.IsSuccess)
	assert.Nil(t, result.Result)
	assert.Equal(t, "Fewer results than minimum result size", result.AbortMessage)
}

func TestBasicStatsPercentageConstraintsFailUpper(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{
		"percentageConstraints": []coordinator.PercentageConstraint{{
			Owner:         "PartyA",
			Attribute:     "a1",
			MinPercentage: big.NewInt(30),
			MaxPercentage: big.NewInt(45),
		}},
	}

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	assert.False(t, result.IsSuccess)
	assert.Nil(t, result.Result)
	assert.Equal(t, "More results than maximum result size", result.AbortMessage)
}

func TestBasicStatsStdevConstraintsOK(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 != 2\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{
		"stdevConstraints": []coordinator.StdevConstraint{{
			Owner:     "PartyA",
			Attribute: "a1",
			MinStdev:  big.NewFloat(0.1),
		}, {
			Owner:     "PartyA",
			Attribute: "a2",
			MinStdev:  big.NewFloat(60),
		}},
	}

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	assert.Equal(t, "", result.AbortMessage)
	if assert.True(t, result.IsSuccess) {
		assert.Equal(t, int64(10), result.Result.(*basicstats.Result).Count)
		assert.Equal(t, float64(5), result.Result.(*basicstats.Result).Aggregates[0][0])
	}
}

func TestBasicStatsStdevConstraintsFailLower(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 != 2\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{
		"stdevConstraints": []coordinator.StdevConstraint{{
			Owner:     "PartyA",
			Attribute: "a1",
			MinStdev:  big.NewFloat(60),
		}},
	}

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	assert.False(t, result.IsSuccess)
	assert.Nil(t, result.Result)
	assert.Equal(t, "Standard deviation too small for column 0", result.AbortMessage)
}

func TestBasicStatsIntoDPaillierWithReview(t *testing.T) {
	sks := prepareDpaiKeys(t)
	defer removeDpaiKeys()

	helper.InstantiateBasicStatsProtocol()
	helper.InstantiateDpaiDecryptProtocol(sks)
	queryID := "1234"
	query := &types.Query{}
	json.Unmarshal([]byte("{\"aggregates\": [\"SUM(PartyA:a1)\"], \"filters\": [\"PartyB:a1 == 1\"]}"), query)
	log.Info().Interface("query", query).Msg("debug")
	payload := &types.TaggedQuery{
		ID:    queryID,
		Query: query,
	}
	options := map[string]interface{}{"reviewer": "PartyB"}

	// Start a reviewer, which watches the PartyB service to see if a review
	// is pending and submits it automatically
	reviewSubmitted := false
	var reviewSubmittedMutex sync.Mutex
	go func() {
		svc := helper.Daemons["PartyB"].Service
		for {
			time.Sleep(time.Second)
			list := svc.ListApprovalRequests("basicstats")
			if len(list) == 0 {
				continue
			}

			// approve the first (and only?) request
			svc.SubmitReview("basicstats", list[0].QueryID, true)
			reviewSubmittedMutex.Lock()
			defer reviewSubmittedMutex.Unlock()
			reviewSubmitted = true
			return
		}
	}()

	result := RunProtocol(t, "basicstats", queryID, payload, options)
	assert.NotNil(t, result)
	assert.True(t, result.IsSuccess)
	if br, ok := result.Result.(*basicstats.Result); assert.True(t, ok, "result is not of basicstats type but is %#v", result.Result) {
		assert.Equal(t, int64(5), br.Count)
		assert.Equal(t, float64(5), br.Aggregates[0][0])
	}
	reviewSubmittedMutex.Lock()
	defer reviewSubmittedMutex.Unlock()
	assert.True(t, reviewSubmitted, "Protocol finished but no review was submitted by the reviewer")
}

func TestDpaiKeygen(t *testing.T) {
	// set Service's dpai parameters to be much lower for quick completion
	service.DefaultKeygenParams.NumberOfParticipants = 3
	service.DefaultKeygenParams.SecretSharingStatisticalSecurity = 10
	service.DefaultKeygenParams.BiprimalityCheckTimes = 20
	viper.Set("dpaillier.keysize", 64)

	// prepare file for the share basicstats uses
	tmpfile, err := ioutil.TempFile("", "dpk")
	assert.NoError(t, err)
	defer os.Remove(tmpfile.Name())
	// store the share in the temp file
	viper.Set("dpaillier.keyfile", tmpfile.Name())

	helper.InstantiateDpaiKeygenProtocol()
	coor := integrationtests.NewTestCoordinator(pool)

	helper.Daemons["PartyA"].Service.StartDpaillierKeygenForce()
	helper.Daemons["PartyA"].Service.SetupDPaillier(coor)

	// Check the keys that were saved
	tagOnDisk, _, err := pailliercache.OwnSecretKeyShare()
	assert.NoError(t, err)

	tagOnCoor, _, err := coor.DPaillierRead(context.Background())
	assert.NoError(t, err)

	assert.Equal(t, tagOnDisk, tagOnCoor)
}
