// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package protocol

import "context"

// Message is an MPC protocol message
type Message struct {
	Protocol string
	Sender   string // Sender of the message
	Receiver string // Receiver of the message
	QueryID  string // ID of the query
	StepID   string // Identifier of the step in the MPC protocol
	Body     []byte // The message for the protocol
}

// MessageSender is an interface for sending MPC messages
type MessageSender interface {
	// SendMessage sends the message to the node specified in the message, returns error on failure
	SendMessage(m *Message) error
	// SendMessageWithDeliveryEnsurance is like SendMessage but it retries in the background on errors
	SendMessageWithDeliveryEnsurance(ctx context.Context, m *Message, errCb MessageDeliveryFailureCallback)
	// RequestStartProtocol sends a request to a node to start its own version of a protocol.
	// protocolData is a JSON-serialized instance of service.ProtocolStartData.
	RequestStartProtocol(receiver, protocolname string, protocolData []byte) error
}

// MessageDeliveryFailureCallback is a type for the function signature of a callback when a message delivery failure occurs
type MessageDeliveryFailureCallback func(queryID, receiver string, err error)

// MessageReceiver receives MPC messages
type MessageReceiver interface {
	Start() error
}

// NewMessage returns a new message
func NewMessage(protocol string, sender, receiver string, queryID string, stepID string, body []byte) *Message {
	return &Message{protocol, sender, receiver, queryID, stepID, body}
}

// ChannelMessageSender is a simple implementation of the MessageSender interface that uses a channel
type ChannelMessageSender struct {
	messageChannel chan *Message
}

// SendMessage sends the MPC message
func (cms *ChannelMessageSender) SendMessage(m *Message) error {
	cms.messageChannel <- m
	return nil
}

// SendMessageWithDeliveryEnsurance sends the MPC message with delivery ensurance
func (cms *ChannelMessageSender) SendMessageWithDeliveryEnsurance(_ context.Context, m *Message, _ MessageDeliveryFailureCallback) {
	// No support for delivery ensurance, fallback to SendMessage
	_ = cms.SendMessage(m)
}

// RequestStartProtocol is unimplemented -- we use it in conjunction with subprotocol callbacks
// that just return a result immediately
func (cms *ChannelMessageSender) RequestStartProtocol(receiver, protocolname string, protocolData []byte) error {
	return nil
}

// NewChannelMessageSender creates a ChannelMessageSender using the provided channel
func NewChannelMessageSender(messageChannel chan *Message) *ChannelMessageSender {
	return &ChannelMessageSender{messageChannel: messageChannel}
}
