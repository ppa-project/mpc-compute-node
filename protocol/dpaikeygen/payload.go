// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dpaikeygen

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
)

var (
	// ErrorMessageForWrongRound is emitted when messages are received for the wrong round
	// of the Keygen protocol (usually an earlier one).
	ErrorMessageForWrongRound = errors.New("received message is for wrong round")

	// ErrorWrongNumberOfMessages is emitted when an arriving message contains an incorrect
	// number of payload messages, indicating a mismatch of NParallelProtocols.
	ErrorWrongNumberOfMessages = errors.New("received message contains wrong number of payloads")

	// ErrorMessageForWrongStage is emitted when a message arrives for a past stage, likely
	// indicating a message was duplicated in transit.
	ErrorMessageForWrongStage = errors.New("received message is for wrong stage")

	// ErrorMemberListMismatch is emitted when a mismatch is discovered between the member lists
	// at the different nodes.
	ErrorMemberListMismatch = errors.New("mismatch between protocol member lists at the different nodes")
)

// Payload is the common interface to all payload types.
type Payload interface {
	// update uses the payload to update the state. State mutex must be held.
	update(s *State) error
	// quickCheck does some integrity checks on the payload and whether it is appropriate
	// for the current state. Does not make any changes. State mutex must be held.
	quickCheck(s *State) error
}

// getPayload takes a stepID and returns an instance of Payload
func getPayload(stepID string) (Payload, error) {
	switch stepID {
	case "restart":
		return &restartPayload{}, nil
	case "abort":
		return &abortPayload{}, nil
	case "1", "2", "3", "4", "5", "6":
		return &intermediateStepPayload{}, nil
	case "success":
		return &successPayload{}, nil
	default:
		return nil, fmt.Errorf("unknown stepID '%s'", stepID)
	}
}

// A startPayload is only made by the startRound() method of State. It creates the first
// set of messages for the keygen protocol and sends them.
type startPayload struct{}

// update starts a new round of the keygen protocol.
func (pl *startPayload) update(s *State) error {
	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage1)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage1, s.nParallelProtocols)
		}
	}
	successCount := 0
	s.currentState = make([]interface{}, s.nParallelProtocols)

	for indexParallel := 0; indexParallel != s.nParallelProtocols; indexParallel++ {
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage1
		var err error
		s.currentState[indexParallel], outBatch, err = dpaillier.NewKeyGenerationProtocol(*s.params)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "1", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages1:   outMessages[recipient],
			Members:     strings.Join(s.nodeNames, ","),
		})
	}

	// Set up the state's fields for the next step
	return s.update1()
}

// quickCheck always succeeds for startPayload
func (pl *startPayload) quickCheck(s *State) error { return nil }

// A restartPayload indicates all aisles in a protocol round have failed,
// and that the parties should restart. To help with synchronization, it
// includes the new round number.
type restartPayload struct {
	NewRound int
}

// update informs us that another party has seen fit to restart the protocol,
// indicating that all aisles have failed. This restarts the protocol if
// we haven't already. Always succeeds.
func (pl *restartPayload) update(s *State) error {
	if s.currentRound < pl.NewRound {
		s.startRound(pl.NewRound)
	}
	return nil
}

// quickCheck always succeeds for restartPayload
func (pl *restartPayload) quickCheck(s *State) error { return nil }

// An abortPayload signifies that an unrecoverable error has occurred.
// This node will no longer participate in the protocol after sending this.
type abortPayload struct {
	Reason string
}

// update informs us that another party has seen fit to abort the protocol,
// indicating an unrecoverable error. This stops the protocol and calls the
// completedCallback. Always succeeds.
func (pl *abortPayload) update(s *State) error {
	if s.completedCallback != nil {
		s.completedCallback(protocol.MadeElsewhere, protocol.Failure, pl.Reason)
	}
	return nil
}

// quickCheck always succeeds for abortPayload
func (pl *abortPayload) quickCheck(s *State) error { return nil }

// A successPayload indicates protocol success. It contains the public key
// the sending node ended up with, which can be used to check outcome
// correctness.
type successPayload struct {
	Pubkey paillier.PublicKey
}

// update informs us that another party has successfully finished the protocol.
// Can be used to check that the generated public key is equal to ours, so we
// call the completed callback with MadeElsewhere. Always succeeds.
func (pl *successPayload) update(s *State) error {
	if s.completedCallback != nil {
		s.completedCallback(protocol.MadeElsewhere, protocol.Success, pl.Pubkey)
	}
	return nil
}

// quickCheck always succeeds for successPayload
func (pl *successPayload) quickCheck(s *State) error { return nil }

// Step payload containing instances of KeyGenerationMessage1.
type intermediateStepPayload struct {
	RoundNumber int
	Members     string
	Messages1   []*dpaillier.KeyGenerationMessage1
	Messages2   []*dpaillier.KeyGenerationMessage2
	Messages3   []*dpaillier.KeyGenerationMessage3
	Messages4   []*dpaillier.KeyGenerationMessage4
	Messages5   []*dpaillier.KeyGenerationMessage5
	Messages6   []*dpaillier.KeyGenerationMessage6
}

func (pl *intermediateStepPayload) len() int {
	switch {
	case pl.Messages1 != nil:
		return len(pl.Messages1)
	case pl.Messages2 != nil:
		return len(pl.Messages2)
	case pl.Messages3 != nil:
		return len(pl.Messages3)
	case pl.Messages4 != nil:
		return len(pl.Messages4)
	case pl.Messages5 != nil:
		return len(pl.Messages5)
	case pl.Messages6 != nil:
		return len(pl.Messages6)
	default:
		return 0
	}
}

// update brings us a message from another party. Checks if we're in the same
// stage and round, and return an error if not.
// Saves the message, and advances the protocol if applicable.
//nolint:gocognit
func (pl *intermediateStepPayload) update(s *State) error {
	// Check that the round number matches
	if pl.RoundNumber != s.currentRound {
		return ErrorMessageForWrongRound
	}
	// Check that the number of simultaneous protocols matches
	if pl.len() != s.nParallelProtocols {
		return ErrorWrongNumberOfMessages
	}

	if len(pl.Members) != 0 && pl.Members != strings.Join(s.nodeNames, ",") {
		return ErrorMemberListMismatch
	}

	// Check that the stage matches the contained messages
	switch {
	case pl.Messages1 != nil && s.currentStage() <= 1:
		return pl.addThenUpdate1(s)
	case pl.Messages2 != nil && s.currentStage() <= 2:
		return pl.addThenUpdate2(s)
	case pl.Messages3 != nil && s.currentStage() <= 3:
		return pl.addThenUpdate3(s)
	case pl.Messages4 != nil && s.currentStage() <= 4:
		return pl.addThenUpdate4(s)
	case pl.Messages5 != nil && s.currentStage() <= 5:
		return pl.addThenUpdate5(s)
	case pl.Messages6 != nil && s.currentStage() <= 6:
		return pl.addThenUpdate6(s)
	default:
		return ErrorMessageForWrongStage
	}
}

// quickCheck checks whether the payload could be processed by the state machine
//nolint:gocognit
func (pl *intermediateStepPayload) quickCheck(s *State) error {
	// Check that the round number matches
	if pl.RoundNumber != s.currentRound {
		return ErrorMessageForWrongRound
	}
	// Check that the number of simultaneous protocols matches
	if pl.len() != s.nParallelProtocols {
		return ErrorWrongNumberOfMessages
	}

	// Check that the stage matches the contained messages
	if (pl.Messages1 != nil && s.currentStage() <= 1) ||
		(pl.Messages2 != nil && s.currentStage() <= 2) ||
		(pl.Messages3 != nil && s.currentStage() <= 3) ||
		(pl.Messages4 != nil && s.currentStage() <= 4) ||
		(pl.Messages5 != nil && s.currentStage() <= 5) ||
		(pl.Messages6 != nil && s.currentStage() <= 6) {
		return nil
	}
	return ErrorMessageForWrongStage
}

func (pl *intermediateStepPayload) addThenUpdate1(s *State) error {
	// Save the messages
	s.waitingMessages1 = append(s.waitingMessages1, pl.Messages1)
	return s.update1()
}

//nolint:funlen,dupl,gocognit // This is perhaps a candidate for rewrite with reflect or generics, but not now
func (s *State) update1() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 1 || len(s.waitingMessages1) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage2)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage2, s.nParallelProtocols)
		}
	}
	successCount := 0

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage2)(nil)
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage1, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages1 {
			if s.waitingMessages1[indexFrom][indexParallel] == nil {
				s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage2)(nil)
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages1[indexFrom][indexParallel]
		}
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage2
		var err error
		s.currentState[indexParallel], outBatch, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage1).Advance(inMessages)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "2", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages2:   outMessages[recipient],
		})
	}

	// Set up the state's fields for the next step
	s.waitingMessages1 = nil
	return s.update2()
}

func (pl *intermediateStepPayload) addThenUpdate2(s *State) error {
	// Save the messages
	s.waitingMessages2 = append(s.waitingMessages2, pl.Messages2)
	return s.update2()
}

//nolint:funlen,dupl,gocognit
func (s *State) update2() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 2 || len(s.waitingMessages2) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage3)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage3, s.nParallelProtocols)
		}
	}
	successCount := 0

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage3)(nil)
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage2, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages2 {
			if s.waitingMessages2[indexFrom][indexParallel] == nil {
				s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage3)(nil)
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages2[indexFrom][indexParallel]
		}
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage3
		var err error
		s.currentState[indexParallel], outBatch, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage2).Advance(inMessages)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "3", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages3:   outMessages[recipient],
		})
	}

	// Set up the state's fields for the next step
	s.waitingMessages2 = nil
	return s.update3()
}

func (pl *intermediateStepPayload) addThenUpdate3(s *State) error {
	// Save the messages
	s.waitingMessages3 = append(s.waitingMessages3, pl.Messages3)
	return s.update3()
}

//nolint:funlen,dupl,gocognit
func (s *State) update3() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 3 || len(s.waitingMessages3) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage4)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage4, s.nParallelProtocols)
		}
	}
	successCount := 0

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage4)(nil)
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage3, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages3 {
			if s.waitingMessages3[indexFrom][indexParallel] == nil {
				s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage4)(nil)
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages3[indexFrom][indexParallel]
		}
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage4
		var err error
		s.currentState[indexParallel], outBatch, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage3).Advance(inMessages)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "4", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages4:   outMessages[recipient],
		})
	}

	// Set up the state's fields for the next step
	s.waitingMessages3 = nil
	return s.update4()
}

func (pl *intermediateStepPayload) addThenUpdate4(s *State) error {
	// Save the messages
	s.waitingMessages4 = append(s.waitingMessages4, pl.Messages4)
	return s.update4()
}

//nolint:funlen,dupl,gocognit
func (s *State) update4() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 4 || len(s.waitingMessages4) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage5)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage5, s.nParallelProtocols)
		}
	}
	successCount := 0

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage5)(nil)
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage4, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages4 {
			if s.waitingMessages4[indexFrom][indexParallel] == nil {
				s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage5)(nil)
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages4[indexFrom][indexParallel]
		}
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage5
		var err error
		s.currentState[indexParallel], outBatch, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage4).Advance(inMessages)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "5", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages5:   outMessages[recipient],
		})
	}

	// Set up the state's fields for the next step
	s.waitingMessages4 = nil
	return s.update5()
}

func (pl *intermediateStepPayload) addThenUpdate5(s *State) error {
	// Save the messages
	s.waitingMessages5 = append(s.waitingMessages5, pl.Messages5)
	return s.update5()
}

//nolint:funlen,dupl,gocognit
func (s *State) update5() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 5 || len(s.waitingMessages5) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	outMessages := make(map[int][]*dpaillier.KeyGenerationMessage6)
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			outMessages[recipient] = make([]*dpaillier.KeyGenerationMessage6, s.nParallelProtocols)
		}
	}
	successCount := 0

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage6)(nil)
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage5, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages5 {
			if s.waitingMessages5[indexFrom][indexParallel] == nil {
				s.currentState[indexParallel] = (*dpaillier.KeyGenerationStage6)(nil)
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages5[indexFrom][indexParallel]
		}
		// Advance the protocols
		var outBatch []*dpaillier.KeyGenerationMessage6
		var err error
		s.currentState[indexParallel], outBatch, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage5).Advance(inMessages)
		// Store the outgoing messages
		if err == nil {
			for i := range outBatch {
				outMessages[outBatch[i].To][indexParallel] = outBatch[i]
			}
			successCount++
		}
	}

	// If all parallel runs have failed, restart the protocol
	if successCount == 0 {
		s.restartProtocol()
		return nil
	}

	// Send outgoing messages to the others
	for recipient := range outMessages {
		go s.send(recipient, "6", &intermediateStepPayload{
			RoundNumber: s.currentRound,
			Messages6:   outMessages[recipient],
		})
	}

	// Set up the state's fields for the next step
	s.waitingMessages5 = nil
	return s.update6()
}

func (pl *intermediateStepPayload) addThenUpdate6(s *State) error {
	// Save the messages
	s.waitingMessages6 = append(s.waitingMessages6, pl.Messages6)
	return s.update6()
}

//nolint:funlen,dupl,gocognit
func (s *State) update6() error {
	// If not yet enough messages, we're done
	if s.currentStage() != 6 || len(s.waitingMessages6) < s.params.NumberOfParticipants-1 { // - ourselves
		return nil
	}

	// Start the new protocol runs
	var privateKey *dpaillier.PrivateKeyShare

nextParallel:
	for indexParallel := range s.currentState {
		// Check if our own state or any of the messages from others are nil for this parallel run - that means it's failed
		if isNilState(s.currentState[indexParallel]) {
			continue nextParallel
		}
		// the incoming messages are stored in [from][run] order, and we need them in [run][from] order to feed them to (*stage).Advance
		inMessages := make([]*dpaillier.KeyGenerationMessage6, s.params.NumberOfParticipants-1)
		for indexFrom := range s.waitingMessages6 {
			if s.waitingMessages6[indexFrom][indexParallel] == nil {
				continue nextParallel
			}
			inMessages[indexFrom] = s.waitingMessages6[indexFrom][indexParallel]
		}
		// Advance the protocols
		var err error
		privateKey, err = s.currentState[indexParallel].(*dpaillier.KeyGenerationStage6).Advance(inMessages)
		// If no error, we're done!
		if err == nil {
			break
		}
	}

	// If all parallel runs have failed, restart the protocol
	if privateKey == nil {
		s.restartProtocol()
		return nil
	}

	// Report success!
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			// Do not cancel this message, and do not call me back if you can't deliver it
			go s.sendWithCallback(context.Background(), recipient, "success", &successPayload{Pubkey: privateKey.PublicKey}, nil)
		}
	}
	if s.completedCallback != nil {
		s.completedCallback(protocol.MadeByUs, protocol.Success, privateKey)
	}

	// Cleanup
	s.waitingMessages6 = nil
	return nil
}
