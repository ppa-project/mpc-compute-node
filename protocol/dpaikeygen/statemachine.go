// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package dpaikeygen the keygen protocol for the distributed Paillier cryptosystem.
//
// The Paillier key generation is highly probabilistic: its chance to fail is nearly 100%.
// This is because it has to generate two prime numbers and multiply them without actually
// seeing them. The way to do this is by multiplying two random numbers distributedly using
// secret sharing, and then checking whether the (publicly visible) product is bi-prime.
//
// This implementation of the protocol runs many keygen protocols simultaneously, on the
// assumption that traffic between nodes incurs significant latency, and restarts if all
// parallel runs have failed.
package dpaikeygen

import (
	"context"
	"fmt"
	"math/big"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"github.com/rs/zerolog/log"
	"github.com/ugorji/go/codec"
)

var protocolName = "dpaikeygen"

// NParallelProtocols is the number of parallel protocol runs executed in each round
const NParallelProtocols = 300

// State is an implementation of StateMachine providing a keygen protocol for basic statistics
type State struct {
	queryid           string
	thisNode          int
	nodeNames         []string
	ms                protocol.MessageSender
	params            *dpaillier.KeyGenerationParameters
	completedCallback protocol.CompletedCallback

	// Round number. Incremented each time an entire batch fails.
	currentRound int
	// Number of elements of the slices below
	nParallelProtocols int

	// ctx is the context used for sending protocol-content messages
	ctx context.Context
	// cancelFunc cancels all outgoing messages sent with context ctx
	cancelFunc context.CancelFunc

	// Fields below are protected by a mutex
	dPailStateMutex sync.Mutex
	// Paillier keygen uses a set of state types. These are stured in currentState,
	// as _, ok := currentState[0].(*dpaillier.KeyGenerationStage1)
	currentState []interface{}
	// Paillier keygen uses a set of message types.
	waitingMessages1 [][]*dpaillier.KeyGenerationMessage1
	waitingMessages2 [][]*dpaillier.KeyGenerationMessage2
	waitingMessages3 [][]*dpaillier.KeyGenerationMessage3
	waitingMessages4 [][]*dpaillier.KeyGenerationMessage4
	waitingMessages5 [][]*dpaillier.KeyGenerationMessage5
	waitingMessages6 [][]*dpaillier.KeyGenerationMessage6
	// futureRoundPayloads are payloads intended for future rounds, but have arrived
	// out-of-order with the restart message
	futureRoundPayloads []Payload
}

// ProcessMessage processes an keygen protocol message
func (s *State) ProcessMessage(m *protocol.Message) error {
	s.dPailStateMutex.Lock()
	defer s.dPailStateMutex.Unlock()
	log := log.With().
		Str("dpaikeygen", s.queryid).
		Str("node", s.nodeNames[s.thisNode]).
		Int("round", s.currentRound).
		Int("statemachine-current-stage", s.currentStage()).
		Logger()
	log.Info().Msgf("Processing message, message step ID: %v", m.StepID)

	if m.QueryID != s.queryid {
		return fmt.Errorf("query ID mismatch; state machine has %s but incoming message wants %s", s.queryid, m.QueryID)
	}

	// Yes, we've basically reinvented RPC :)
	pl, err := getPayload(m.StepID)
	if err != nil {
		return err
	}

	// Now parse/decode the message and return any errors immediately
	err = codec.NewDecoderBytes(m.Body, protocol.CodecHandle).Decode(pl)
	if err != nil {
		log.Err(err).Msgf("Codec unmarshal error trying to interpret \n%s", m.Body)
		return err
	}

	return s.processPayload(pl)
}

// processPayload assumes the mutex is held, and calls quickCheck and update
func (s *State) processPayload(pl Payload) error {
	// Do a quick check on the payload content and immediately return in case of
	// errors
	err := pl.quickCheck(s)
	if err == ErrorMessageForWrongRound {
		if pl.(*intermediateStepPayload).RoundNumber > s.currentRound {
			s.futureRoundPayloads = append(s.futureRoundPayloads, pl)
		} else {
			log.Warn().Msgf("Received a message for round %v but we're already in round %v, dropping it", pl.(*intermediateStepPayload).RoundNumber, s.currentRound)
		}
		return nil
	} else if err != nil {
		return err
	}

	// Now that we've decoded the payload and checked on the easy stuff,
	// let's let the statemachine handle the payload further
	err = pl.update(s)
	if err != nil {
		log.Err(err).Msgf("ProcessMessage with payload %v", pl)
	}
	return err
}

func (s *State) getErrCb() protocol.MessageDeliveryFailureCallback {
	return func(queryID, receiver string, err error) {
		log.Err(err).Str("queryID", queryID).Str("receiver", receiver).Msg("Failed to deliver message, abort protocol")
		s.sendAborts(fmt.Sprintf("protocol message delivery failure (%s -> %s): %s", s.nodeNames[s.thisNode], receiver, err.Error()))
	}
}

// send is a convenience function for sending protocol messages. Should be called as goroutine
func (s *State) send(receiver int, stepID string, messageBody Payload) {
	s.sendWithCallback(s.ctx, receiver, stepID, messageBody, s.getErrCb())
}

func (s *State) sendWithCallback(ctx context.Context, receiver int, stepID string, messageBody Payload, errCb protocol.MessageDeliveryFailureCallback) {
	if receiver == s.thisNode {
		s.dPailStateMutex.Lock()
		defer s.dPailStateMutex.Unlock()

		log.Debug().
			Str("stepid", stepID).
			Msg("Statemachine sends message to self")
		if err := messageBody.quickCheck(s); err != nil {
			log.Err(err).
				Str("stepid", stepID).
				Msg("quickCheck on message to self returned error")
		}
		if err := messageBody.update(s); err != nil {
			log.Err(err).
				Str("stepid", stepID).
				Msg("update on message to self returned error")
		}
		return
	}
	var mb []byte
	err := codec.NewEncoderBytes(&mb, protocol.CodecHandle).Encode(messageBody)
	if err != nil {
		log.Err(err).
			Str("stepid", stepID).
			Msg("Could not marshal outgoing message")
		return
	}

	s.ms.SendMessageWithDeliveryEnsurance(ctx, protocol.NewMessage(protocolName, s.nodeNames[s.thisNode], s.nodeNames[receiver], s.queryid, stepID, mb), errCb)
}

// abort aborts this protocol and sends a message to all other nodes
func (s *State) sendAborts(reason string) {
	// Cancel all outgoing content messages
	s.cancelFunc()

	// Send abort messages to all nodes
	am := abortPayload{reason}

	for i := range s.nodeNames {
		if i == s.thisNode {
			continue
		}
		go s.sendWithCallback(context.Background(), i, "abort", &am, nil)
	}

	// Record aborted query in result cache and on blockchain
	if s.completedCallback != nil {
		s.completedCallback(protocol.MadeByUs, protocol.Failure, reason)
	}
}

func (s *State) restartProtocol() {
	for recipient := 0; recipient != s.params.NumberOfParticipants; recipient++ {
		if recipient != s.params.ParticipantIndex {
			go s.sendWithCallback(context.Background(), recipient, "restart", &restartPayload{s.currentRound + 1}, nil)
		}
	}
	log.Info().Str("node", s.nodeNames[s.thisNode]).Msgf("Starting a new round")
	s.startRound(s.currentRound + 1)
}

// Start starts the keygen protocol.
// input is expected to be a string naming the query ID, and
// options["params"] is expected to be a pointer to a dpaillier.KeyGenerationParameters.
func (s *State) Start(input protocol.Input, options protocol.Options, callback protocol.CompletedCallback) error {
	s.completedCallback = callback

	var ok bool
	s.queryid, ok = input.(string)
	if !ok {
		return fmt.Errorf("input to dpaikeygen.Start() is not a string, but is %T", input)
	}

	switch params := options["params"].(type) {
	case *dpaillier.KeyGenerationParameters:
		s.params = params
	case map[string]interface{}:
		s.params = paramsFromMap(params)
	default:
		return fmt.Errorf("options[params] to dpaikeygen.Start() is not a *KeyGenerationParameters or a map, but is %#v", options["params"])
	}

	// To prevent mix-ups with the message sending, we hardcode the participant index to s.thisNode.
	// This requires that all three nodes have the same value of s.nodeNames, which is checked in
	// quickCheck for intermediatePayloads of step 1.
	s.params.ParticipantIndex = s.thisNode
	log.Debug().Msgf("Starting DPaillier keygen: This is node %v in %v with params %#v", s.thisNode, s.nodeNames, s.params)

	// Start computation
	s.dPailStateMutex.Lock()
	defer s.dPailStateMutex.Unlock()
	s.startRound(0)

	return nil
}

func paramsFromMap(m map[string]interface{}) *dpaillier.KeyGenerationParameters {
	params := new(dpaillier.KeyGenerationParameters)
	if n, ok := m["NumberOfParticipants"].(float64); ok {
		params.NumberOfParticipants = int(n)
	}
	if n, ok := m["ParticipantIndex"].(float64); ok {
		params.ParticipantIndex = int(n)
	}
	if n, ok := m["PaillierBitSize"].(float64); ok {
		params.PaillierBitSize = int(n)
	}
	if n, ok := m["SecretSharingDegree"].(float64); ok {
		params.SecretSharingDegree = int(n)
	}
	if n, ok := m["SecretSharingStatisticalSecurity"].(float64); ok {
		params.SecretSharingStatisticalSecurity = int(n)
	}
	if n, ok := m["BiprimalityCheckTimes"].(float64); ok {
		params.BiprimalityCheckTimes = int(n)
	}
	if n, ok := m["NumProcessors"].(float64); ok {
		params.NumProcessors = int(n)
	}
	if s, ok := m["SecretSharingModulus"].(string); ok {
		params.SecretSharingModulus, _ = big.NewInt(0).SetString(s, 10)
	}
	return params
}

// SubmitReview does not apply to this protocol
func (s *State) SubmitReview(_ bool) error {
	return fmt.Errorf("can not submit reviews to dpaikeygen")
}

// in startRound, mutex dPailStateMutex must be held.
func (s *State) startRound(roundNumber int) {
	// Reset all internal states
	s.currentState = nil
	s.waitingMessages1 = make([][]*dpaillier.KeyGenerationMessage1, 0)
	s.waitingMessages2 = make([][]*dpaillier.KeyGenerationMessage2, 0)
	s.waitingMessages3 = make([][]*dpaillier.KeyGenerationMessage3, 0)
	s.waitingMessages4 = make([][]*dpaillier.KeyGenerationMessage4, 0)
	s.waitingMessages5 = make([][]*dpaillier.KeyGenerationMessage5, 0)
	s.waitingMessages6 = make([][]*dpaillier.KeyGenerationMessage6, 0)

	s.currentRound = roundNumber
	// Start computation
	pl := &startPayload{}
	go func() {
		s.dPailStateMutex.Lock()
		defer s.dPailStateMutex.Unlock()
		if err := pl.update(s); err != nil {
			log.Err(err).Msg("update on startRound returned error")
		}

		oldFutureRoundPayloads := s.futureRoundPayloads
		s.futureRoundPayloads = make([]Payload, 0)
		for _, pl := range oldFutureRoundPayloads {
			_ = s.processPayload(pl)
		}
	}()
}

// in currentStage, mutex dPailStateMutex must be held.
func (s *State) currentStage() int {
	if len(s.currentState) == 0 {
		return 0
	}
	switch s.currentState[0].(type) {
	case *dpaillier.KeyGenerationStage1:
		return 1
	case *dpaillier.KeyGenerationStage2:
		return 2
	case *dpaillier.KeyGenerationStage3:
		return 3
	case *dpaillier.KeyGenerationStage4:
		return 4
	case *dpaillier.KeyGenerationStage5:
		return 5
	case *dpaillier.KeyGenerationStage6:
		return 6
	}
	return 0
}

// isNilState checks whether this is interface nil or any of the six
// *dpaillier.KeyGenerationStageN nil pointers.
func isNilState(x interface{}) bool {
	switch v := x.(type) {
	case nil:
		return true
	case *dpaillier.KeyGenerationStage1:
		return v == nil
	case *dpaillier.KeyGenerationStage2:
		return v == nil
	case *dpaillier.KeyGenerationStage3:
		return v == nil
	case *dpaillier.KeyGenerationStage4:
		return v == nil
	case *dpaillier.KeyGenerationStage5:
		return v == nil
	case *dpaillier.KeyGenerationStage6:
		return v == nil
	default:
		return false
	}
}

// GetInputType returns the input type of the protocol
func (s *State) GetInputType() interface{} {
	return ""
}

// NewStateMachine creates a new keygen protocol instance.
//
// thisNode and otherNodes contain the names of the nodes participating in the query.
func NewStateMachine(thisNode int, nodeNames []string, ms protocol.MessageSender) *State {
	s := new(State)
	s.thisNode = thisNode
	s.nodeNames = nodeNames
	s.ms = ms
	s.nParallelProtocols = NParallelProtocols
	s.ctx, s.cancelFunc = context.WithCancel(context.Background())
	return s
}
