// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dpaikeygen

import (
	"os"
	"sync"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	test.Setup()

	code := m.Run()
	os.Exit(code)
}

func TestKeygen(t *testing.T) {
	assert := assert.New(t)

	c := make(chan *protocol.Message, 6)
	ms := protocol.NewChannelMessageSender(c)

	names := []string{"A", "B", "C"}
	stateMachines := map[string]*State{
		"A": NewStateMachine(0, names, ms),
		"B": NewStateMachine(1, names, ms),
		"C": NewStateMachine(2, names, ms),
	}

	var callbackResult *dpaillier.PrivateKeyShare
	var m sync.Mutex
	done := make(chan struct{}, 3)

	queryID := "AAAA111"
	params0 := dpaillier.KeyGenerationParameters{
		NumberOfParticipants:             3,
		ParticipantIndex:                 0,
		PaillierBitSize:                  64,
		SecretSharingDegree:              1,
		SecretSharingStatisticalSecurity: 20,
		SecretSharingModulus:             nil,
		BiprimalityCheckTimes:            20,
	}
	params0.Validate()
	params1, params2 := params0, params0
	params1.ParticipantIndex = 1
	params2.ParticipantIndex = 2

	options := make([]map[string]interface{}, 3)
	options[0] = map[string]interface{}{"params": &params0}
	options[1] = map[string]interface{}{"params": &params1}
	options[2] = map[string]interface{}{"params": &params2}

	for k := range options {
		assert.NoError(stateMachines[names[k]].Start(queryID, options[k], func(origin protocol.ResultOrigin, isSuccess protocol.ResultKind, payload interface{}) {
			if origin == protocol.MadeByUs {
				assert.Equal(protocol.Success, isSuccess, "not successful")

				m.Lock()
				var ok bool
				callbackResult, ok = payload.(*dpaillier.PrivateKeyShare)
				assert.True(ok, "cast failed")
				m.Unlock()
				done <- struct{}{}
			}
		}))
	}

	doneFlag := false
	for !doneFlag {
		select {
		case m := <-c: // Read a message from the channel
			t.Logf("Message from %v to %v, step: %v, length: %d", m.Sender, m.Receiver, m.StepID, len(m.Body))
			// Send the message along to the right recipient
			go func() {
				if err := stateMachines[m.Receiver].ProcessMessage(m); err != nil {
					t.Logf("ProcessMessage error: %v", err)
				}
			}()
		case <-done:
			doneFlag = true
		default:
			time.Sleep(50 * time.Millisecond)
		}
	}

	// Now check the outcome
	m.Lock()
	defer m.Unlock()

	assert.NotNil(callbackResult)
}
