// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package protocol

import (
	"math/big"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/ugorji/go/codec"
)

type testStruct struct {
	First  *big.Int
	Second *big.Int
}

func TestBigIntEncodingJSON(t *testing.T) {
	assert := assert.New(t)
	var h codec.JsonHandle
	h.SetInterfaceExt(reflect.TypeOf(&big.Int{}), 1, BigIntExt{})
	ts := &testStruct{big.NewInt(42), big.NewInt(102815712457)}
	out := make([]byte, 0, 128)
	enc := codec.NewEncoderBytes(&out, &h)
	err := enc.Encode(ts)
	assert.Nil(err, "Unexpected error")
	t.Logf("m: %v", string(out))
	dec := codec.NewDecoderBytes(out, &h)
	tsFromText := new(testStruct)
	err = dec.Decode(tsFromText)
	assert.Nil(err, "Unexpected error")
	t.Logf("from text: %v", tsFromText)
	assert.Equal(ts.First, tsFromText.First, "Should be equal")
	assert.Equal(ts.Second, tsFromText.Second, "Should be equal")
}

func TestBigIntEncodingMsgPack(t *testing.T) {
	assert := assert.New(t)
	var h codec.MsgpackHandle

	h.SetBytesExt(reflect.TypeOf(&big.Int{}), 1, BigIntExt{})
	ts := &testStruct{big.NewInt(42), big.NewInt(102815712457)}
	out := make([]byte, 0, 128)
	enc := codec.NewEncoderBytes(&out, &h)
	err := enc.Encode(ts)
	assert.Nil(err, "Unexpected error")
	t.Logf("m: %v", string(out))
	dec := codec.NewDecoderBytes(out, &h)
	tsFromText := new(testStruct)
	err = dec.Decode(tsFromText)
	assert.Nil(err, "Unexpected error")
	t.Logf("from text: %v", tsFromText)
	assert.Equal(ts.First, tsFromText.First, "Should be equal")
	assert.Equal(ts.Second, tsFromText.Second, "Should be equal")
}
