// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package protocol

import (
	"encoding/base64"
	"math/big"
	"reflect"

	"github.com/ugorji/go/codec"
)

var CodecHandle codec.Handle

// BigIntExt is a go-codec extension to better support *big.Int
type BigIntExt struct{}

// ConvertExt marshals a *big.Int to a base64 encoded string
func (x BigIntExt) ConvertExt(v interface{}) interface{} {
	bi := v.(*big.Int)
	return base64.StdEncoding.EncodeToString(bi.Bytes())
}

// UpdateExt converts a unmarshals a base64 string to a *big.Int
func (x BigIntExt) UpdateExt(dest interface{}, v interface{}) {
	bi := dest.(*big.Int)
	buf, err := base64.StdEncoding.DecodeString(v.(string))
	if err != nil {
		panic(err)
	}
	bi.SetBytes(buf)
}

// WriteExt converts a *big.Int to a []byte
func (x BigIntExt) WriteExt(v interface{}) []byte {
	bi := v.(*big.Int)
	return bi.Bytes()
}

// ReadExt updates a *big.Int from a []byte
func (x BigIntExt) ReadExt(dst interface{}, src []byte) {
	bi := dst.(*big.Int)
	bi.SetBytes(src)
}

func init() {
	// Initialize the codec
	jsonHandle := new(codec.JsonHandle)
	_ = jsonHandle.SetInterfaceExt(reflect.TypeOf(&big.Int{}), 1, BigIntExt{})
	msgpackHandle := new(codec.MsgpackHandle)
	_ = msgpackHandle.SetBytesExt(reflect.TypeOf(&big.Int{}), 1, BigIntExt{})
	CodecHandle = msgpackHandle
}
