// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// Options represents the computation options assigned to this query, for instance the
// minimum size of the set over which the aggregation takes place.
type Options struct {
	// Minimum amount of records, absolute
	MinResultSize int64
	// Maximum amount of records, absolute
	MaxResultSize int64
	// Constraints on standard deviations
	StdevConstraints []coordinator.StdevConstraint
	// Manual review node, empty string for none
	Reviewer string
}

// NewOptions contructs a new Options
func NewOptions(query *types.Query, options protocol.Options, dbSize int) Options {
	// Fill the options, providing sensible defaults if any are missing
	minResultSize, _ := options["minResultSize"].(int64)
	minResultPercentage, _ := options["minResultPercentage"].(int64)
	maxResultPercentage, ok := options["maxResultPercentage"].(int64)
	if !ok {
		maxResultPercentage = 100
	}

	opt := Options{
		MinResultSize:    minResultPercentage * int64(dbSize) / 100,
		MaxResultSize:    maxResultPercentage * int64(dbSize) / 100,
		StdevConstraints: make([]coordinator.StdevConstraint, 0),
	}

	// Take the stricter of the two "minimum size" bounds
	if minResultSize > opt.MinResultSize {
		opt.MinResultSize = minResultSize
	}

	// Apply per-column percentage constraints to the min/max result size bounds
	percentageConstraints, _ := options["percentageConstraints"].([]coordinator.PercentageConstraint)
	for _, c := range percentageConstraints {
		if queryContainsConstraint(query, c.Owner, c.Attribute) {
			if minResSize := c.MinPercentage.Int64() * int64(dbSize) / 100; opt.MinResultSize < minResSize {
				opt.MinResultSize = minResSize
			}
			if maxResSize := c.MaxPercentage.Int64() * int64(dbSize) / 100; opt.MaxResultSize > maxResSize {
				opt.MaxResultSize = maxResSize
			}
		}
	}

	// Filter the stdev constraints so only those occurring in the query remain
	stdevConstraints, _ := options["stdevConstraints"].([]coordinator.StdevConstraint)
	for _, c := range stdevConstraints {
		if queryContainsConstraint(query, c.Owner, c.Attribute) {
			opt.StdevConstraints = append(opt.StdevConstraints, c)
		}
	}

	opt.Reviewer, _ = options["reviewer"].(string)

	return opt
}

func queryContainsConstraint(query *types.Query, owner, attr string) bool {
	for i := range query.Aggregates {
		if query.Aggregates[i].Owner == owner && query.Aggregates[i].Attribute == attr {
			return true
		}
	}
	return false
}

func queryContainsOwner(query *types.Query, owner string) bool {
	if owner == "" {
		return false
	}
	for i := range query.Aggregates {
		if query.Aggregates[i].Owner == owner {
			return true
		}
	}
	for i := range query.Filters {
		if query.Filters[i].Owner == owner {
			return true
		}
	}
	return false
}

// ColumnNeedsStdev checks a query column to see if it needs to have a StDev component, either
// because the query function itself is MEANSTDEV, or because o.StdevConstraints
// contains a constraint for this column.
func (o *Options) ColumnNeedsStdev(col *types.Aggregate) bool {
	if col.Function == types.MeanStdDevFunction {
		return true
	}
	for i := range o.StdevConstraints {
		if o.StdevConstraints[i].Owner == col.Owner && o.StdevConstraints[i].Attribute == col.Attribute {
			return true
		}
	}
	return false
}

// GetStdevConstraint gives the first stdev constraint for the given column, or nil is none exists.
func (o *Options) GetStdevConstraint(col *types.Aggregate) *big.Float {
	for i := range o.StdevConstraints {
		if o.StdevConstraints[i].Owner == col.Owner && o.StdevConstraints[i].Attribute == col.Attribute {
			return o.StdevConstraints[i].MinStdev
		}
	}
	return nil
}
