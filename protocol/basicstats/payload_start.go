// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/rs/zerolog/log"
)

type startPayload struct {
	query     *types.TaggedQuery
	queryPlan *QueryPlan
}

// QuickCheck always succeeds.
func (pl *startPayload) QuickCheck() error {
	return nil
}

// The startPayload is not received from another node, but made by us. It does the initial
// work of encrypting and adding up all of the data, and sends it to the nodes who do the
// filtering.
func (pl *startPayload) Update(s *State) {
	log := log.With().Str("queryid", pl.query.ID).Logger()
	// Set the variables we got from the start message
	s.query = pl.query
	s.queryPlan = pl.queryPlan

	log.Info().Interface("query", s.query).Msg("New accepted basicstats query")

	if s.thisNode != s.queryPlan.Start {
		return
	}

	dpaitag, dpaiKey, err := pailliercache.OwnSecretKeyShare()
	if err != nil {
		log.Err(err).Msg("Error loading Paillier keypair")
		s.sendAborts("Error loading Paillier keypair", s.query.ID)
		return
	}
	pubkey := &dpaiKey.PublicKey
	s.dpaillierKeyTag = dpaitag

	// Make sure the keystore has our key (nop if it was added before)
	s.keystore.AddPaillierPublicKey(dpaitag, pubkey)
	// Get the buffer corresponding to our key
	encrypter := s.keystore.Encrypter(dpaitag)

	ids := s.database.GetIDs()
	// Let's create a filter message
	fm := filterPayload{
		Ids:             ids,
		EncryptedCounts: make([]*big.Int, len(ids)),
		EncryptedValues: make([][]*big.Int, 0, len(s.query.Aggregates)),
		Pk:              pubkey,
		DPaillierTag:    dpaitag,
	}

	// Set up the slices for the encrypted attribute values
	for _, c := range s.query.Aggregates {
		// We skip the columns for the Count function (since nothing needs to be done)
		if c.Function != types.CountFunction {
			fm.EncryptedValues = append(fm.EncryptedValues, make([]*big.Int, len(fm.Ids)))
		}
	}

	log.Info().Msg("Encrypting values")
	// Now go through each row
	for i, id := range fm.Ids {
		// Check whether the item matches the query condition
		var match bool
		match, err = s.database.Matches(id, s.query.Filters)
		if err != nil {
			log.Err(err).Msg("Error querying database")
			s.sendAborts("Error querying database", s.query.ID)
			return
		}
		if match {
			// Set count to encrypted 1
			fm.EncryptedCounts[i] = encrypter.Encrypt(bigOne)
			// Encrypt attribute values
			encryptedCol := 0
			for _, c := range s.query.Aggregates {
				// Skip reading attribute values for COUNT columns
				var v int64
				if c.Function != types.CountFunction {
					v, err = s.database.GetIntValue(id, c.Attribute)
					if err != nil {
						log.Err(err).Msg("Error getting value from database")
						s.sendAborts("Error getting value from database", s.query.ID)
						return
					}
					fm.EncryptedValues[encryptedCol][i] = encrypter.Encrypt(big.NewInt(v))
					encryptedCol++
				}
			}
		} else {
			// Set count to encrypted 0
			fm.EncryptedCounts[i] = encrypter.Encrypt(bigZero)
			// Set encrypted values to 0
			for j := range fm.EncryptedValues {
				fm.EncryptedValues[j][i] = encrypter.Encrypt(bigZero)
			}
		}
	}
	log.Info().Msg("About to send filter message")
	if err = s.send(s.queryPlan.Next[s.thisNode], "filter", &fm); err != nil {
		log.Err(err).Str("receiver", s.queryPlan.Next[s.thisNode]).Msg("startPayload -> filter: failed to send message")
		s.sendAborts("startPayload -> filter: failed to send message", s.query.ID)
	}
}
