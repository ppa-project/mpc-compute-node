// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

type abortPayload struct {
	Reason string
}

// QuickCheck always succeeds.
func (pl *abortPayload) QuickCheck() error {
	return nil
}

// On receipt of an abortPayload, the node is informed that the computation failed
// elsewhere. This is a "FYI" kind of message.
func (pl *abortPayload) Update(s *State) {
	s.aborted = true
	s.completedCallback(protocol.MadeElsewhere, protocol.Failure, pl.Reason)
}
