// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"errors"
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"github.com/rs/zerolog/log"
)

type filterPayload struct {
	Ids             []string            `json:"ids"`
	EncryptedCounts []*big.Int          `json:"encryptedcounts"`
	EncryptedValues [][]*big.Int        `json:"encryptedvalues"`
	Pk              *paillier.PublicKey `json:"pk"`
	DPaillierTag    string              `json:"dpailliertag"`
}

// QuickCheck makes sure each column contains the same number of rows
func (pl *filterPayload) QuickCheck() error {
	// Make sure the lengths are ok
	for i := 0; i < len(pl.EncryptedValues); i++ {
		if len(pl.Ids) != len(pl.EncryptedValues[i]) {
			return errors.New("length mismatch of arrays in FilterMessage")
		}
	}
	return nil
}

// On receipt of a filter payload, the node sets the attributes of 'non-matching'
// rows to zero, and sends it either to the next node that needs to apply a filter,
// or it sums the contents of the columns and prepares to send the aggregates back
// to the starting node. It first sends the number of unfiltered nodes, to make sure
// there are enough people left in the data set.
func (pl *filterPayload) Update(s *State) {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()
	// Prefer the Distributed Paillier key if one is present. Otherwise,
	// use the cached/audited key of the starting node unless none are present
	// but post a warning log if we need to fall back to the key in the message
	keyTag := s.queryPlan.Start
	if len(pl.DPaillierTag) != 0 {
		keyTag = pl.DPaillierTag
		s.dpaillierKeyTag = pl.DPaillierTag
	}
	encrypter := s.keystore.Encrypter(keyTag)
	if encrypter == nil {
		log.Warn().
			Str("startNode", s.queryPlan.Start).
			Str("dpaillierTag", pl.DPaillierTag).
			Msg("Key was not present in Paillier cache (dpaillierTag if it's set, startNode otherwise). Falling back to the pubkey attached to the message.")
		encrypter = pl.Pk
		// Add this encrypter so we can use it the rest of this protocol
		s.keystore.AddPaillierPublicKey(keyTag, pl.Pk)
	}

	log.Info().Msgf("Processing filter message")

	// For each item, check if it matches the query condition
	missingEntries := 0
	for i, id := range pl.Ids {
		match, err := s.database.Matches(id, s.query.Filters)
		if err != nil {
			missingEntries++
			match = false
		}
		if match {
			// randomize encrypted count for this item
			pl.EncryptedCounts[i] = encrypter.Randomize(pl.EncryptedCounts[i])
			// randomize encrypted attribute values for this item
			for j := range pl.EncryptedValues {
				pl.EncryptedValues[j][i] = encrypter.Randomize(pl.EncryptedValues[j][i])
			}
		} else {
			// Set count to (encrypted) 0 for this item
			pl.EncryptedCounts[i] = encrypter.Encrypt(bigZero)
			// Set values to (encrypted) 0 for this item
			for j := range pl.EncryptedValues {
				pl.EncryptedValues[j][i] = encrypter.Encrypt(bigZero)
			}
		}
	}
	if missingEntries > 0 {
		log.Warn().
			Int("missing-entries", missingEntries).
			Msg("Some entries in the filter payload were not present in our database. These were treated as non-matching.")
	}

	// If we're the last one, we'll now do some aggregation
	if s.thisNode == s.queryPlan.Last {
		// Initialize to zero
		encryptedCount := encrypter.Encrypt(bigZero)
		encryptedAggregates := make([]*big.Int, len(pl.EncryptedValues))
		for j := 0; j < len(pl.EncryptedValues); j++ {
			encryptedAggregates[j] = encrypter.Encrypt(bigZero)
		}
		// Now some magic: homomorphic encryption in action!
		for i := range pl.Ids {
			encryptedCount = encrypter.Add(encryptedCount, pl.EncryptedCounts[i])
			for j, encryptedValues := range pl.EncryptedValues {
				encryptedAggregates[j] = encrypter.Add(encryptedAggregates[j], encryptedValues[i])
			}
		}
		// Store the encrypted result here
		s.encryptedAggregates = encryptedAggregates

		// Share the encrypted count to the party with the key
		countChecker := s.queryPlan.Start
		if len(s.options.Reviewer) != 0 {
			countChecker = s.options.Reviewer
		}

		err := s.send(countChecker, "checkCount",
			&checkCountPayload{EncryptedCount: encryptedCount})
		if err != nil {
			log.Err(err).Msg("Error sending message")
		}
	} else {
		// Let's pass this along to the next party
		err := s.send(s.queryPlan.Next[s.thisNode], "filter", pl)
		if err != nil {
			log.Err(err).Msg("Error sending message")
		}
	}
}
