// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"errors"
	"math/big"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/rs/zerolog/log"
)

// ReviewData tracks the data needed to do a manual review of a
// query result. Since two messages are needed to do a review,
// and we do not know the order in which they will arrive, the
// ReviewData collects both, and provides functionality to start
// the review once ready.
type ReviewData struct {
	// Note that fields for different roles must not overlap / be
	// re-used, as a node can have multiple roles!

	// Fields for the reviewer
	sync.Mutex
	aggregates []*big.Int
	count      int64
	countSet   bool
	status     ReviewStatus
}

// SetAggregates saves the encrypted blinded aggregates
// in a thread-safe manner
func (rd *ReviewData) SetAggregates(aggregates []*big.Int) {
	rd.Lock()
	defer rd.Unlock()
	rd.aggregates = aggregates
}

// SetCount saves the encrypted blinded aggregates
// in a thread-safe manner
func (rd *ReviewData) SetCount(count int64) {
	rd.Lock()
	defer rd.Unlock()
	rd.count = count
	rd.countSet = true
}

// StartReviewOnceIfReady checks if both parts are present and starts the
// review if they are
func (rd *ReviewData) StartReviewOnceIfReady(s *State) {
	rd.Lock()
	defer rd.Unlock()
	if rd.aggregates != nil && rd.countSet && rd.status == ReviewWaitingForData {
		rd.status = ReviewStarted
		go rd.review(s)
	}
}

// At function start, blinds and blindedAggregates must be non-nil, and
// started must be ReviewStarted. This will prevent review() from being called more
// than once.
func (rd *ReviewData) review(s *State) {
	rd.Lock()
	defer rd.Unlock()

	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()
	log.Info().Msg("Manual review initiated")

	approvalRequest := &ApprovalRequest{
		Query: s.query,
		Count: rd.count,
	}
	approvalRequest.ComputeAggregates(rd.aggregates)
	s.completedCallback(protocol.MadeByUs, protocol.ManualReview, approvalRequest)
}

// SubmitReview submits a manual review and continues the protocol run if approved
func (s *State) SubmitReview(approved bool) error {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()

	// Error checking: is a review in progress?
	s.reviewData.Lock()
	defer s.reviewData.Unlock()
	if s.reviewData.status != ReviewStarted || s.reviewData.aggregates == nil {
		log.Warn().
			Stringer("review-status", s.reviewData.status).
			Int("n-blinded-aggregates", len(s.reviewData.aggregates)).
			Int64("count", s.reviewData.count).
			Msg("SubmitReview called but no review is expected")
		return errors.New("SubmitReview called but no review is expected")
	}
	s.reviewData.status = ReviewFinished

	if approved {
		err := s.send(s.queryPlan.Start, "sendReviewedAggregates",
			&sendReviewedAggregatesPayload{Count: s.reviewData.count, Aggregates: s.reviewData.aggregates})
		if err != nil {
			log.Err(err).Msg("Error sending reviewed aggregates")
		}
	} else {
		s.sendAborts("Query aborted by manual review", s.query.ID)
	}
	return nil
}

// ReviewStatus represents the review status. Its default value is
// "waiting for data".
type ReviewStatus int

const (
	// ReviewWaitingForData represents the review status waiting for data
	ReviewWaitingForData ReviewStatus = iota
	// ReviewStarted represents the review status started
	ReviewStarted
	// ReviewFinished represents the review status finished
	ReviewFinished
)

func (rs ReviewStatus) String() string {
	return [...]string{"Waiting for data", "Started", "Finished"}[rs]
}

// ApprovalRequest contains a tagged query and the aggregates computed
// for it. The aggregates are an array of big.Floats of the same length as
// Query.Aggregates, containing the value of Count for all COUNT functions
// and sums and means for SUM and MEAN(STDEV) functions.
type ApprovalRequest struct {
	Query      *types.TaggedQuery
	Count      int64
	Aggregates []float64
}

// ComputeAggregates fills the Aggregates member given the sums resulting
// from the protocol. The Query and Count members should be filled when
// calling this function, otherwise it does nothing.
func (ra *ApprovalRequest) ComputeAggregates(sums []*big.Int) {
	if ra.Query == nil || ra.Count == 0 {
		return
	}
	ra.Aggregates = make([]float64, len(ra.Query.Aggregates))
	sumsCursor := 0
	for i := range ra.Aggregates {
		switch ra.Query.Aggregates[i].Function {
		case types.CountFunction:
			ra.Aggregates[i] = float64(ra.Count)
		case types.SumFunction:
			ra.Aggregates[i], _ = big.NewFloat(0.0).SetInt(sums[sumsCursor]).Float64()
			sumsCursor++
		case types.MeanFunction, types.MeanStdDevFunction:
			ra.Aggregates[i], _ = big.NewRat(sums[sumsCursor].Int64(), ra.Count).Float64()
			sumsCursor++
		}
	}
}

// Protocol implements protcol.ApprovalRequest
func (ra *ApprovalRequest) Protocol() string {
	return "basicstats"
}

// QueryID implements protcol.ApprovalRequest
func (ra *ApprovalRequest) QueryID() string {
	return ra.Query.ID
}

// Summary implements protcol.ApprovalRequest
func (ra *ApprovalRequest) Summary() string {
	return ra.Query.String()
}
