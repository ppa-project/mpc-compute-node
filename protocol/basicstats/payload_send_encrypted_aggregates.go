// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"github.com/rs/zerolog/log"
)

type sendEncryptedAggregatesPayload struct {
	EncryptedAggregates []*big.Int
}

// QuickCheck always passes.
func (pl *sendEncryptedAggregatesPayload) QuickCheck() error {
	return nil
}

// Update decrypts the received aggregates and then proceeds as reviewedAggregates.Update.
func (pl *sendEncryptedAggregatesPayload) Update(s *State) {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()

	// The second round needs a different decryption query id
	// EncryptedAggregates is processed by the reviewer, or if none exists, the start node.
	// As reviewer, we detect the second round by s.reviewData.status == ReviewFinished,
	// as starter we detect it by s.plaintextMeans != nil
	queryidSuffix := ".encryptedAggregates"
	if s.reviewData.status == ReviewFinished || s.plaintextMeans != nil {
		queryidSuffix = ".encryptedStdev"
	}

	// Initiate a sub-protocol to decrypt the aggregates
	s.completedCallback(protocol.MadeByUs, protocol.SubProtocol, protocol.SubProtocolRequest{
		SubProtocolName: "dpaidecrypt",
		SubQueryID:      s.query.ID + queryidSuffix,
		Input: &dpaidecrypt.Input{
			QueryID:     s.query.ID + queryidSuffix,
			Ciphertexts: pl.EncryptedAggregates,
			ReplyTo:     s.thisNode,
		},
		Options: map[string]interface{}{},
		CompletedCallback: func(resultOrigin protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
			plaintexts, ok := payload.([]*big.Int)
			if resultOrigin == protocol.MadeByUs && resultKind == protocol.Success && ok && len(plaintexts) == len(pl.EncryptedAggregates) {
				pl.finishUpdate(s, plaintexts)
			} else {
				log.Error().
					Int("result-kind", int(resultKind)).
					Bool("cast-ok", ok).
					Int("n-plaintexts", len(plaintexts)).
					Int("n-ciphertexts", len(pl.EncryptedAggregates)).
					Msgf("Result of paillier decryption protocol failed, it was %#v", payload)
				s.sendAborts("Error decrypting aggregates", s.query.ID)
			}
		},
	})
}

func (pl *sendEncryptedAggregatesPayload) finishUpdate(s *State, aggregates []*big.Int) {
	// These are the plaintext aggregates.
	// If this protocol has a reviewer, we are the reviewer. Start a review.
	if len(s.options.Reviewer) != 0 && s.reviewData.status != ReviewFinished {
		log.Info().
			Str("queryid", s.query.ID).
			Str("node", s.thisNode).
			Str("reviewer", s.options.Reviewer).
			Msg("Received aggregates for review")
		s.reviewData.SetAggregates(aggregates)
		s.reviewData.StartReviewOnceIfReady(s)

		return
	}

	// If this protocol has no reviewer, carry on with the remainder.
	sraPl := &sendReviewedAggregatesPayload{Count: s.plaintextCount, Aggregates: aggregates}
	go func() {
		if err := s.send(s.queryPlan.Start, "sendReviewedAggregates", sraPl); err != nil {
			log.Err(err).Str("recipient", s.queryPlan.Start).Msg("Error sending reviewed aggregates")
		}
	}()
}
