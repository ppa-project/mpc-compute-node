// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"fmt"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// QueryCheck checks the given query by making sure all columns for the present
// party that the query mentions, are present in the database, and that any filters
// satisfy the sanity check (*types.DatabaseAttribute)FilterDiscriminates(*types.Filter).
func (s *State) QueryCheck(query *types.Query) error {
	attr := s.database.GetAttributes()

	for i := range query.Aggregates {
		if s.thisNode == query.Aggregates[i].Owner {
			contains := false
			for j := range attr {
				if query.Aggregates[i].Attribute == attr[j].Name {
					contains = true
					break
				}
			}
			if !contains {
				return fmt.Errorf("query aggregate %d is attribute %v:%v, but our (%v's) database does not contain an attribute by that name", i, query.Aggregates[i].Owner, query.Aggregates[i].Attribute, s.thisNode)
			}
		}
	}

	for i := range query.Filters {
		if s.thisNode == query.Filters[i].Owner {
			contains := false
			for j := range attr {
				if query.Filters[i].Attribute == attr[j].Name {
					contains = true
					if !attr[j].FilterDiscriminates(&query.Filters[i]) {
						switch attr[j].Kind {
						case "string":
							return fmt.Errorf("query filter %d is '%s', which is not sufficiently discriminating on string attribute %s", i, query.Filters[i].String(), attr[j].Name)
						case "enum":
							return fmt.Errorf("query filter %d is '%s', which is not sufficiently discriminating on enum attribute %s with values %v", i, query.Filters[i].String(), attr[j].Name, attr[j].EnumValues)
						default:
							return fmt.Errorf("query filter %d is '%s', which is not sufficiently discriminating on attribute %s with unexpected type %s", i, query.Filters[i].String(), attr[j].Name, attr[j].Kind)
						}
					}
				}
			}
			if !contains {
				return fmt.Errorf("query filter %d concerns attribute %v:%v, but our (%v's) database does not contain an attribute by that name", i, query.Filters[i].Owner, query.Filters[i].Attribute, s.thisNode)
			}
		}
	}
	return nil
}
