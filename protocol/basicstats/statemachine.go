// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package basicstats implements the MPC protocol for computing sums, means and standard
// deviations of the node's data.
package basicstats

import (
	"context"
	"errors"
	"fmt"
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/rs/zerolog/log"
	"github.com/ugorji/go/codec"
)

var protocolName = "basicstats"

var bigZero = big.NewInt(0)
var bigOne = big.NewInt(1)

// State is an implementation of StateMachine providing a MPC protocol for basic statistics
type State struct {
	database   database.Database
	thisNode   string
	otherNodes func() ([]string, error)
	ms         protocol.MessageSender
	options    Options
	aborted    bool

	ctx        context.Context
	cancelFunc context.CancelFunc

	dpaillierKeyTag     string // tag into the keystore
	plaintextCount      int64
	plaintextMeans      []float64
	scalingFactors      []float64
	encryptedAggregates []*big.Int
	reviewData          ReviewData
	keystore            Keystore
	query               *types.TaggedQuery
	queryPlan           *QueryPlan
	incomingPayloads    chan Payload
	completedCallback   protocol.CompletedCallback
}

// Keystore keeps Paillier public keys and allows retrieving them by the name of
// the organization they belong to. Currently implemented by pailliercache.Precomputer.
type Keystore interface {
	AddPaillierPublicKey(org string, pk *paillier.PublicKey)
	Encrypter(organization string) paillier.Encrypter
}

// Result contains the aggregates demanded by the corresponding query,
// as well as the number of records passing the query's filter (QueryConditions).
type Result struct {
	// The number of people in the filtered data set
	Count int64 `json:"count"`
	// The aggregates. First index is the query function, slice of values is the aggregate:
	// for COUNT, there is no aggregate, and the contents of the slice are unspecified;
	// for SUM and MEAN, the aggregate slice contains one value, which is the result;
	// For MEANSTDDEV, the aggregate slice contains 0: mean, 1: stddev.
	Aggregates [][]float64 `json:"aggregates"`
}

// ProcessMessage processes an MPC protocol message
func (s *State) ProcessMessage(m *protocol.Message) error {
	log := log.With().Str("queryid", m.QueryID).Str("node", s.thisNode).Logger()
	log.Info().Msgf("Processing message, step: %v", m.StepID)

	// Yes, we've basically reinvented RPC :)
	pl, err := getPayload(m.StepID)
	if err != nil {
		return err
	}

	if s.aborted {
		return errors.New("Protocol is aborted")
	}

	// Now parse/decode the message and return any errors immediately
	err = codec.NewDecoderBytes(m.Body, protocol.CodecHandle).Decode(pl)
	if err != nil {
		log.Err(err).Msgf("Codec unmarshal error trying to interpret \n%s", m.Body)
		return err
	}

	// Do a quick check on the payload content and immediately return in case of
	// errors
	err = pl.QuickCheck()
	if err != nil {
		return err
	}

	// Now that we've decoded the payload and checked on the easy stuff,
	// let's let the statemachine handle the payload further
	s.incomingPayloads <- pl

	return nil
}
func (s *State) getErrCb() protocol.MessageDeliveryFailureCallback {
	return func(queryID, receiver string, err error) {
		log.Err(err).Str("queryID", queryID).Str("receiver", receiver).Msg("Failed to deliver message, abort protocol")
		s.sendAborts(fmt.Sprintf("%s: Failed to deliver message to %s", s.thisNode, receiver), queryID)
	}
}

// send is a convenience function for sending protocol messages
func (s *State) send(receiver string, stepID string, messageBody Payload) error {
	return s.sendWithErrCb(s.ctx, receiver, stepID, messageBody, s.getErrCb())
}

// sendWithErrCb is a convenience function for sending protocol messages and providing an error callback
func (s *State) sendWithErrCb(ctx context.Context, receiver string, stepID string, messageBody Payload, errCb protocol.MessageDeliveryFailureCallback) error {
	if receiver == s.thisNode {
		log.Debug().
			Str("queryid", s.query.ID).
			Str("stepid", stepID).
			Msg("Statemachine sends message to self")
		if err := messageBody.QuickCheck(); err != nil {
			return err
		}
		go messageBody.Update(s)
		return nil
	}
	mb := []byte{}
	err := codec.NewEncoderBytes(&mb, protocol.CodecHandle).Encode(messageBody)
	if err != nil {
		return err
	}
	msg := protocol.NewMessage(protocolName, s.thisNode, receiver, s.query.ID, stepID, mb)
	s.ms.SendMessageWithDeliveryEnsurance(ctx, msg, errCb)
	return nil
}

// abort aborts this protocol and sends a message to all other nodes
func (s *State) sendAborts(reason string, _ string) {
	// Cancel all stalled outgoing payloads
	s.cancelFunc()

	// Send abort messages to all nodes
	am := abortPayload{reason}
	otherNodes, err := s.otherNodes()
	if err != nil {
		log.Err(err).
			Msgf("Can not retrieve list of other nodes to send basicstats abort notification to")
	} else {
		for _, otherNode := range otherNodes {
			err := s.sendWithErrCb(context.Background(), otherNode, "abort", &am, nil)
			if err != nil {
				log.Err(err).Msg("Unable to send abort message")
			}
		}
	}
	// Record aborted query in result cache and on blockchain
	s.completedCallback(protocol.MadeByUs, protocol.Failure, reason)
}

// Start starts the basicstats protocol.
// input is expected to be a pointer to a types.TaggedQuery.
func (s *State) Start(input protocol.Input, options protocol.Options, callback protocol.CompletedCallback) error {
	s.completedCallback = callback

	pl := new(startPayload)
	var ok bool
	pl.query, ok = input.(*types.TaggedQuery)
	if !ok {
		return fmt.Errorf("input to basicstats.Start() is not a database query, but is %T", input)
	}
	err := pl.QuickCheck()
	if err != nil {
		return err
	}
	err = s.QueryCheck(pl.query.Query)
	if err != nil {
		return err
	}

	log := log.With().Str("queryid", pl.query.ID).Logger()
	otherNodes, err := s.otherNodes()
	if err != nil {
		log.Err(err).
			Msg("Can not retrieve list of other nodes")
		return err
	}
	allNodes := append(otherNodes, s.thisNode)
	pl.queryPlan, err = QueryPlanFromQuery(pl.query.Query, allNodes)
	if err != nil {
		log.Err(err).Msg("Error creating query plan from query")
		return err
	}

	s.options = NewOptions(pl.query.Query, options, s.database.Len())
	if len(s.options.StdevConstraints) != 0 {
		pl.queryPlan.HasStdDevs = true
	}

	// We'll have a goroutine for this instance and communicate with it using
	// a channel. The goroutine waits for payloads to process
	go func() {
		// First update the StartPayload
		pl.Update(s)
		// Then wait for payloads to come in on the channel
		for sm := range s.incomingPayloads {
			sm.Update(s)
		}
	}()

	return nil
}

// GetInputType returns the input type of the protocol
func (s *State) GetInputType() interface{} {
	return types.Query{}
}

// NewStateMachine creates a new basic statistics MPC protocol instance.
//
// thisNode and otherNodes contain the names of the nodes participating in the query.
// These should be the same as the names occurring in the query.
//
// keystore is the Paillier public key store (and precomputation buffer) that will be
// used for encryption. If the Paillier public key has been added to it in advance,
// encryption will be much faster.
func NewStateMachine(database database.Database, thisNode string, otherNodes func() ([]string, error), ms protocol.MessageSender, keystore Keystore) *State {
	s := new(State)
	// Yes, we've basically reinvented RPC :)
	s.database = database
	s.thisNode = thisNode
	s.otherNodes = otherNodes
	s.ms = ms
	s.keystore = keystore
	s.incomingPayloads = make(chan Payload)
	s.ctx, s.cancelFunc = context.WithCancel(context.Background())
	return s
}
