// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"math"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

type sendResultsPayload = Result

// Creates a SendResultsPayload based on the query and cached aggregates in State
// and the newly received aggregates in latestAggregates
func newSendResultsPayload(s *State, latestAggregates []float64) *sendResultsPayload {
	rm := &sendResultsPayload{
		Count:      s.plaintextCount,
		Aggregates: make([][]float64, 0),
	}
	// If there are no standard deviations, the only aggregates are in Latest
	// and those are the ones we have to use. But if there are standard deviations,
	// the means and sums were cached in State, and Latest contains just stddevs.
	meanAggregates := s.plaintextMeans
	if s.plaintextMeans == nil {
		meanAggregates = latestAggregates
	}
	for c := range s.query.Aggregates {
		switch s.query.Aggregates[c].Function {
		case types.CountFunction:
			// Count function yields an empty aggregate column
			rm.Aggregates = append(rm.Aggregates, nil)
		case types.SumFunction:
			// Sum yields one aggregate: the sum. But we have the mean. Mult by count.
			rm.Aggregates = append(rm.Aggregates, []float64{math.Round(meanAggregates[c] * float64(rm.Count))})
		case types.MeanFunction:
			// Mean yields one aggregate: the mean
			rm.Aggregates = append(rm.Aggregates, []float64{meanAggregates[c]})
		case types.MeanStdDevFunction:
			// MeanStdDev yields two aggregates: the mean and the stddev
			rm.Aggregates = append(rm.Aggregates, []float64{meanAggregates[c], latestAggregates[c]})
		}
	}
	return rm
}

// QuickCheck always passes.
func (pl *sendResultsPayload) QuickCheck() error {
	return nil
}

// On receipt of a sendResultsPayload, Update calles the state machine's callback.
func (pl *sendResultsPayload) Update(s *State) {
	s.completedCallback(protocol.MadeElsewhere, protocol.Success, pl)
}
