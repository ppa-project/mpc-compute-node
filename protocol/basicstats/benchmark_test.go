// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

// For the benchmarks, we run the normal basicstats tests under a few different
// conditions, in order to see how different factors affect performance.
type Benchmark struct {
	// The database size is scaled to 100, 1000, 10000, 100000 persons
	DBSize int
	// The query input is varied between SUM and MEANSTDEV
	Function types.AggregateFunction
	// The Paillier precompute buffer is used (and pre-charged) or not
	Precompute bool
}

// Run sets up the benchmark according to the parameters, resets the timer, and
// runs the benchmark. It is intended to only run successful queries - if the
// normal tests do not pass, the benchmark will likely hang.
func (p *Benchmark) Run(b *testing.B) {
	zerolog.SetGlobalLevel(zerolog.Disabled)
	assert := assert.New(b)

	// Fill the database with p.DbSize elements
	dbA := database.NewTestDatabase("A", p.DBSize)
	dbA.FillAttributePattern1("a1", 1)

	dbB := database.NewTestDatabase("B", p.DBSize)
	dbB.FillAttributePattern2("b1", 15)
	dbB.FillAttributePattern1("b2", 4)
	dbB.FillAttributePattern3("b3", 7)

	dbC := database.NewTestDatabase("C", p.DBSize)
	dbC.FillAttributePattern2("c1", 1)

	// Set up message senders
	c := make(chan *protocol.Message)
	ms := protocol.NewChannelMessageSender(c)

	// Set up the keystore
	// prepare key shares
	var sks []*dpaillier.PrivateKeyShare
	assert.NoError(json.Unmarshal(smallDPaillierKeyJSON, &sks))
	// prepare file for the share basicstats uses
	tmpfile, err := ioutil.TempFile("", "dpk")
	assert.NoError(err)
	defer os.Remove(tmpfile.Name())
	// store the share in the temp file
	viper.SetDefault("dpaillier.keyfile", tmpfile.Name())
	pailliercache.StoreSecretKeyShare("test", sks[0])
	// check that it's there
	tag, sks0, err := pailliercache.OwnSecretKeyShare()
	assert.NoError(err)
	assert.Equal("test", tag)
	assert.Equal(sks[0], sks0)

	viper.SetDefault("paillierkey", FILENAME)
	var keystore Keystore
	if p.Precompute {
		// There is one secret here: the Precomputer will give you your own key,
		// but assumes there is only one key on the machine (and saves it to disk).
		// So all crypto happens using the same key for all the nodes.
		cache := pailliercache.NewPrecomputer()
		sk := sks0
		assert.Nil(err, "Error generating own Paillier key")

		numberOfEncryptionOperations := (p.DBSize* // Number of rows
			2* // Number of columns (COUNT and p.Function)
			3 + // Encryption (Compute) + Update + Update (For)
			2) // Sum of each column
		if p.Function == types.MeanStdDevFunction {
			numberOfEncryptionOperations *= 2
		}

		cache.AddPaillierPublicKeyExt("A", &sk.PublicKey,
			b.N*numberOfEncryptionOperations, // bufferSize
			8,                                // numProc
			true,                             // waitForCompletion
		)
		keystore = cache
	} else {
		keystore = &TestKeystore{M: make(map[string]paillier.Encrypter)}
	}

	done := make(chan struct{})

	query := &types.TaggedQuery{
		ID: "benchmark",
		Query: &types.Query{
			Aggregates: []types.Aggregate{
				{Function: p.Function, Owner: "B", Attribute: "b1"},
			},
			Filters: []types.Filter{
				{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
				{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
			},
		},
	}
	options := map[string]interface{}{"minResultSize": int64(2)}

	// Preparation is complete. Start the timer!
	b.ResetTimer()

	for benchIt := 0; benchIt != b.N; benchIt++ {
		// Start state machines
		stateMachines := map[string]*State{
			"A": NewStateMachine(dbA, "A", func() ([]string, error) { return []string{"B", "C"}, nil }, ms, keystore),
			"B": NewStateMachine(dbB, "B", func() ([]string, error) { return []string{"A", "C"}, nil }, ms, keystore),
			"C": NewStateMachine(dbC, "C", func() ([]string, error) { return []string{"A", "B"}, nil }, ms, keystore),
		}
		for k := range stateMachines {
			assert.Nil(stateMachines[k].Start(query, options, func(origin protocol.ResultOrigin, isSuccess protocol.ResultKind, payload interface{}) {
				if origin == protocol.MadeByUs {
					assert.Equal(protocol.Success, isSuccess, "Callback result was a failure - fix tests before benchmarking")
					done <- struct{}{}
				}
			}))
		}

		// Pump messages
		nMessages := 0
		maxMessages := 7
		if p.Function == "MEANSTDEV" {
			maxMessages = 12
		}
		for nMessages < maxMessages {
			select {
			case m := <-c: // Read a message from the channel
				// Send the message along to the right recipient
				assert.Nil(stateMachines[m.Receiver].ProcessMessage(m))
				nMessages++
			default:
				time.Sleep(50 * time.Millisecond)
			}
		}

		// Wait for the other goroutine to take the message from the channel and process it
		<-done
	}

	os.Remove(FILENAME) // Remove test key file
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	// log.Info().
	// 	Int("DbSize", p.DbSize).
	// 	Str("Function", p.Function).
	// 	Bool("Precompute", p.Precompute).
	// 	Msg("Benchmark complete")
}

func benchF(dbsize int, function types.AggregateFunction, precompute bool) func(b *testing.B) {
	return func(b *testing.B) {
		bm := &Benchmark{
			DBSize:     dbsize,
			Function:   function,
			Precompute: precompute,
		}
		bm.Run(b)
	}
}

func BenchmarkBasicstats______Sum__Buf(b *testing.B) {
	b.Run("   100", benchF(100, types.SumFunction, true))
	b.Run("  1000", benchF(1000, types.SumFunction, true))
	b.Run(" 10000", benchF(10000, types.SumFunction, true))
}

func BenchmarkBasicstats______SumNobuf(b *testing.B) {
	b.Run("   100", benchF(100, types.SumFunction, false))
	b.Run("  1000", benchF(1000, types.SumFunction, false))
	b.Run(" 10000", benchF(10000, types.SumFunction, false))
}

func BenchmarkBasicstatsMeanstdev__Buf(b *testing.B) {
	b.Run("   100", benchF(100, types.MeanStdDevFunction, true))
	b.Run("  1000", benchF(1000, types.MeanStdDevFunction, true))
	b.Run(" 10000", benchF(10000, types.MeanStdDevFunction, true))
}

func BenchmarkBasicstatsMeanstdevNobuf(b *testing.B) {
	b.Run("   100", benchF(100, types.MeanStdDevFunction, false))
	b.Run("  1000", benchF(1000, types.MeanStdDevFunction, false))
	b.Run(" 10000", benchF(10000, types.MeanStdDevFunction, false))
}
