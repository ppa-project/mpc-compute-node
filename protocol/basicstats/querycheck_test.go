// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

// QueryCheck checks the given query by making sure all columns for the present
// party that the query mentions, are present in the database, and that any filters
// satisfy the sanity check (*types.DatabaseAttribute)FilterDiscriminates(*types.Filter).
func TestQueryCheck(t *testing.T) {
	assert := assert.New(t)
	db := database.NewTestDatabase("PartyA", 2)
	db.FillAttributePattern2e("a", "x", "y")
	db.FillAttributePattern2s("b", "u", "v")
	db.FillAttributePattern2("c", 1)
	s := State{database: db, thisNode: "PartyA"}

	// Test existence
	assert.NoError(s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "PartyA", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "A", Attribute: "a", Operator: "==", ReferenceValue: "2"}},
	}))
	err := s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "PartyA", Attribute: "ggg"}},
		Filters:    []types.Filter{{Owner: "A", Attribute: "a", Operator: "==", ReferenceValue: "2"}},
	})
	if assert.Error(err) {
		assert.Equal("query aggregate 0 is attribute PartyA:ggg, but our (PartyA's) database does not contain an attribute by that name", err.Error())
	}
	assert.NoError(s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "a", Operator: "==", ReferenceValue: "y"}},
	}))
	err = s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "ggg", Operator: "==", ReferenceValue: "y"}},
	})
	if assert.Error(err) {
		assert.Equal("query filter 0 concerns attribute PartyA:ggg, but our (PartyA's) database does not contain an attribute by that name", err.Error())
	}

	// Test whether filter selects no or all possible values
	err = s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "a", Operator: "==", ReferenceValue: "z"}},
	})
	if assert.Error(err) {
		assert.Equal("query filter 0 is 'PartyA:a == z', which is not sufficiently discriminating on enum attribute a with values [x y]", err.Error())
	}
	err = s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "a", Operator: ">=", ReferenceValue: "x"}},
	})
	if assert.Error(err) {
		assert.Equal("query filter 0 is 'PartyA:a >= x', which is not sufficiently discriminating on enum attribute a with values [x y]", err.Error())
	}
	err = s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "b", Operator: "==", ReferenceValue: ""}},
	})
	if assert.Error(err) {
		assert.Equal("query filter 0 is 'PartyA:b == ', which is not sufficiently discriminating on string attribute b", err.Error())
	}
	// Nothing implemented yet for int columns
	assert.NoError(s.QueryCheck(&types.Query{
		Aggregates: []types.Aggregate{{Function: types.MeanFunction, Owner: "A", Attribute: "a"}},
		Filters:    []types.Filter{{Owner: "PartyA", Attribute: "c", Operator: ">=", ReferenceValue: "0"}},
	}))
}
