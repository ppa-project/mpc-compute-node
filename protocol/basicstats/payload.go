// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"fmt"
)

// Payload is an interface for a "step message": a message for a specific protocol step
//
// Payloads are processed by MPC nodes. The general 'happy flow' path is as follows.
// Here, we refer to nodes as Start, Middle and Final to indicate their position in
// the MPC protocol, where Start encrypts data, Middle and Final filter it, and Final
// computes encrypted aggregates to be decrypted by Start.
// The review process adds an additional role of Reviewer, which may coincide with
// any of the other roles.
//
// Start processes `start`, sends `filter` to Middle or Final
//
// Middle processes `filter`, sends `filter` to another Middle or Final
//
// Final processes `filter` and sends `checkCount` to Start
//
// Start processes `checkCount` and sends `countCheckSuccessful` to Final
//
// Final processes `countCheckSuccessful` and sends `sendEncryptedAggregates` to Start
// OR a combination of `sendEncryptedBlindedAggregates` to Start and `sendBlinds` to
// Reviewer.
//
// Start processes `sendEncryptedAggregates` and sends `sendResults` to all nodes, OR
// decides that standard deviations are needed, which requires another set of `filter`
// messages to Middle or Final.
//
// Start processes `sendEncryptedBlindedAggregates` and sends `sendBlindedAggregates`
// to Reviewer.
//
// Reviewer processes `sendBlinds` and `sendEncryptedBlindedAggregates` once both have
// been received, and sends `sendReviewedAggregates` to Start.
//
// Start processes `sendReviewedAggregates` and sends `sendResults` to all nodes, OR
// decides that standard deviations are needed, which requires another set of `filter`
// messages to Middle or Final.
type Payload interface {
	Update(s *State)
	QuickCheck() error
}

// getPayload takes a stepID and returns an instance of Payload
func getPayload(stepID string) (Payload, error) {
	switch stepID {
	case "filter":
		return &filterPayload{}, nil
	case "checkCount":
		return &checkCountPayload{}, nil
	case "countCheckSuccessful":
		return &countCheckSuccessfulPayload{}, nil
	case "sendEncryptedAggregates":
		return &sendEncryptedAggregatesPayload{}, nil
	case "sendReviewedAggregates":
		return &sendReviewedAggregatesPayload{}, nil
	case "sendResults":
		return &sendResultsPayload{}, nil
	case "abort":
		return &abortPayload{}, nil
	default:
		return nil, fmt.Errorf("unknown stepID '%s'", stepID)
	}
}
