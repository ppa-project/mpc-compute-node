// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"fmt"
	"math"
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/rs/zerolog/log"
)

type sendReviewedAggregatesPayload struct {
	Count      int64
	Aggregates []*big.Int
}

// QuickCheck always passes.
func (pl *sendReviewedAggregatesPayload) QuickCheck() error {
	return nil
}

// This payload contains the aggregates. The starting node either sends the
// result to everyone, or computes standard deviation columns and starts the second round.
func (pl *sendReviewedAggregatesPayload) Update(s *State) {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()
	dpaitag, dpaiKey, err := pailliercache.OwnSecretKeyShare()
	if err != nil {
		log.Err(err).Msg("Error loading Paillier keypair")
		s.sendAborts("Error loading Paillier keypair", s.query.ID)
		return
	}
	pubkey := &dpaiKey.PublicKey

	if len(dpaitag) == 0 {
		log.Err(err).Msgf("Invalid DPaillier state: there is not currently a dpaillier key available")
		s.sendAborts("DPaillier state invalid during protocol execution", s.query.ID)
		return
	}

	s.plaintextCount = pl.Count

	// We keep an aggregate array corresponding to the Compute function columns.
	aggregates := make([]float64, len(s.query.Aggregates))

	// While decrypting and scaling the aggregates, we keep in mind that our result payload
	// will have len(s.query.Aggregates) columns, but the incoming Aggregates will only
	// have as many columns as needed. It never contains a column for COUNT, and will not contain
	// anything for SUM and MEAN in the second round of a MEANSTDDEV (unless they're subject to a
	// constraint).
	// We keep a payloadCursor for the Aggregates for this reason.
	if s.plaintextCount != 0 {
		payloadCursor := 0
		for i := range s.query.Aggregates {
			// There is no payload for Counts
			if s.query.Aggregates[i].Function == types.CountFunction {
				continue
			}
			// If we're in round 2 of a MEANSTDDEV, there is no entry either for SUM and MEAN
			// (unless they're subject to a constraint)
			if s.plaintextMeans != nil && !s.options.ColumnNeedsStdev(&s.query.Aggregates[i]) {
				continue
			}

			// We store the MEAN for all columns, even for SUM, since we might need to compute
			// any column's standard deviation in case this is required by a StdevConstraint.
			// We'll multiply it back to the sum at the end, if needed.
			// Decrypt the sum, divide it by the count, and convert it to a float64.
			aggregates[i], _ = new(big.Rat).
				SetFrac(pl.Aggregates[payloadCursor], new(big.Int).SetInt64(s.plaintextCount)).
				Float64()

			// In the special case of mean/standard deviation, we need to compute the square
			// root in the final step and scale the value back using the scaling factor.
			if s.plaintextMeans != nil && s.options.ColumnNeedsStdev(&s.query.Aggregates[i]) {
				aggregates[i] = math.Sqrt(aggregates[i] / s.scalingFactors[payloadCursor])

				// We also check here if there is a StdevConstraint on this column, and if so,
				// whether it is violated. If it is, we abort.
				if minimumStdev := s.options.GetStdevConstraint(&s.query.Aggregates[i]); minimumStdev != nil {
					minimumStdev, _ := minimumStdev.Float64()
					if aggregates[i] < minimumStdev {
						log.Warn().
							Float64("stdev-actual", aggregates[i]).
							Float64("stdev-minimum", minimumStdev).
							Stringer("column", &s.query.Aggregates[i]).
							Msgf("Aborting protocol, standard deviation of column %d is not high enough", i)
						s.sendAborts(fmt.Sprintf("Standard deviation too small for column %d", i), s.query.ID)
						return
					}
				}
			}

			payloadCursor++
		}
	}
	// Depending on the query, we are done (COUNT SUM MEAN) or we need to do another round (MEANSTDDEV)
	// (!s.queryPlan.HasStdDevs) = there are no MEANSTDDEV, these aggregates are all we need (but can
	//   also be 'true' if a stdev constraint in the metadata forced stdev calculation for a column)
	// (s.plaintextMeans != nil) = there are MEANSTDDEV, but we already have the MEANs cached
	if !s.queryPlan.HasStdDevs || s.plaintextMeans != nil {
		// There are no standard deviations; we are done
		rm := newSendResultsPayload(s, aggregates)
		var otherNodes []string
		if otherNodes, err = s.otherNodes(); err != nil {
			log.Err(err).
				Msgf("Can not retrieve list of other nodes to send basicstats result to")
			// The protocol was otherwise successful, so we do send the "completed" callback
		} else {
			for _, otherNode := range otherNodes {
				go func(otherNode string) {
					if err := s.send(otherNode, "sendResults", rm); err != nil {
						log.Err(err).Str("receiver", otherNode).Msg("sendReviewedAggregatesPayload -> sendResults: failed to send message")
					}
				}(otherNode)
			}
		}
		s.completedCallback(protocol.MadeByUs, protocol.Success, rm)
		return
	}

	// Cache what we now know to be the means for later
	// We should do standard deviations. Make a new data set and send it to be filtered.
	s.plaintextMeans = aggregates

	encrypter := s.keystore.Encrypter(s.dpaillierKeyTag)
	if encrypter == nil {
		log.Error().
			Str("startNode", s.queryPlan.Start).
			Str("dpaillierTag", s.dpaillierKeyTag).
			Str("key-wanted", s.dpaillierKeyTag).
			Msg("Key was not present in Paillier cache (dpaillierTag if it's set, startNode otherwise), query can not proceed.")
		s.sendAborts(fmt.Sprintf("No Paillier key present in keystore for tag %s", s.dpaillierKeyTag), s.query.ID)
		return
	}
	ids := s.database.GetIDs()

	// Let's create a filter message
	fm := filterPayload{
		Ids:             ids,
		EncryptedCounts: make([]*big.Int, len(ids)),
		EncryptedValues: make([][]*big.Int, 0),
		Pk:              pubkey,
	}

	// Set up the slices for the encrypted attribute values
	for i := range s.query.Aggregates {
		// We only make columns for the StdDev function, or for columns we'd need to check
		// the StdDev for to see if it's not too small
		if s.options.ColumnNeedsStdev(&s.query.Aggregates[i]) {
			fm.EncryptedValues = append(fm.EncryptedValues, make([]*big.Int, len(fm.Ids)))
		}
	}

	// Collect the means belonging specifically to the standard deviation Compute entries
	// If the query consists of the entries
	//     SUM, COUNT, MEANSTDDEV, MEAN, MEANSTDDEV
	// we will now make only two* new columns with the standard deviations, and we'd like
	// to know the contents of s.plaintextMeans[2] and [4].
	// *) If a StdevConstraintOnThisColumn() is true for a SUM/MEAN column, it's included anyway
	relevantMeans := []float64{}
	for i := range s.query.Aggregates {
		if s.options.ColumnNeedsStdev(&s.query.Aggregates[i]) {
			relevantMeans = append(relevantMeans, s.plaintextMeans[i])
		}
	}

	// To create the standard deviation using integers, we need to normalize the values.
	// We compute the deviation (x - m)^2 for each entry in the database, and compute
	// the log2 of the mean of that. Then, we multiply the data by a power of 2 that brings
	// this log2 to 128, which (thanks to the sqrt at the end) leads to a result of 64
	// significant bits. See #11.
	log.Info().Msg("Normalizing values, round 2 (StdDev)")
	normalizedValues := make([][]float64, len(fm.EncryptedValues))
	for i := range normalizedValues {
		normalizedValues[i] = make([]float64, len(fm.Ids))
	}

	encryptedCol := 0
	s.scalingFactors = make([]float64, len(fm.EncryptedValues))
	for i, c := range s.query.Aggregates {
		if !s.options.ColumnNeedsStdev(&s.query.Aggregates[i]) {
			continue
		}

		// Get the values and store (x - m)^2
		total := 0.0
		for i, id := range fm.Ids {
			// For StdDev, we encrypt (x - mean)^2
			// Only read attribute values for MeanStdDev
			var v int64
			v, err = s.database.GetIntValue(id, c.Attribute)
			if err != nil {
				log.Err(err).Msg("Error getting value from database")
				s.sendAborts("Error getting value from database", s.query.ID)
				return
			}

			normalizedValues[encryptedCol][i] = math.Pow(float64(v)-relevantMeans[encryptedCol], 2.0)
			total += normalizedValues[encryptedCol][i]
		}
		logOfMean := math.Log2(total / float64(len(normalizedValues[encryptedCol])))

		// Compute scaling factor
		s.scalingFactors[encryptedCol] = math.Pow(2.0, 128.0-logOfMean)
		for i := range normalizedValues[encryptedCol] {
			normalizedValues[encryptedCol][i] *= s.scalingFactors[encryptedCol]
		}

		encryptedCol++
	}

	log.Info().Msg("Encrypting values, round 2 (StdDev)")
	// Now go through each row
	for i, id := range fm.Ids {
		// Check whether the item matches the query condition
		var match bool
		match, err = s.database.Matches(id, s.query.Filters)
		if err != nil {
			log.Err(err).Msg("Error querying database")
			s.sendAborts("Error querying database", s.query.ID)
			return
		}
		if match {
			// Set count to encrypted 1
			fm.EncryptedCounts[i] = encrypter.Encrypt(bigOne)
			// Round the normalized (x-m)^2 and encrypt it
			normalizedIntValue := big.NewInt(0)
			for j := range fm.EncryptedValues {
				normalizedIntValue, _ = big.NewFloat(math.RoundToEven(normalizedValues[j][i])).Int(normalizedIntValue)
				fm.EncryptedValues[j][i] = encrypter.Encrypt(normalizedIntValue)
			}
		} else {
			// Set count to encrypted 0
			fm.EncryptedCounts[i] = encrypter.Encrypt(bigZero)
			// Set encrypted values to 0
			for j := range fm.EncryptedValues {
				fm.EncryptedValues[j][i] = encrypter.Encrypt(bigZero)
			}
		}
	}
	log.Info().Msg("About to send filter message")
	if err = s.send(s.queryPlan.Next[s.thisNode], "filter", &fm); err != nil {
		log.Err(err).Str("receiver", s.queryPlan.Next[s.thisNode]).Msg("sendReviewedAggregatesPayload -> filter: failed to send message")
		s.sendAborts("sendReviewedAggregatesPayload -> filter: failed to send message", s.query.ID)
	}
}
