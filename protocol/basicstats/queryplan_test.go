// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

func TestQueryPlan(t *testing.T) {
	assert := assert.New(t)

	// Test some correct queries
	q := &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "B", Attribute: "attr1"},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
			{Owner: "E", Attribute: "attr3", Operator: "==", ReferenceValue: "3"},
			{Owner: "B", Attribute: "attrB1", Operator: "==", ReferenceValue: "yes"},
			{Owner: "A", Attribute: "attrA1", Operator: "<", ReferenceValue: "15000"},
		},
	}
	qp, err := QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.NoError(err, "Couldn't make queryplan %v", q)
	assert.Equal("B", qp.Start, "Unexpected start node for query plan %v", q)
	assert.Equal("A", qp.Next["B"], "Unexpected next node in query plan %v", q)
	assert.Equal("C", qp.Next["A"], "Unexpected next node in query plan %v", q)
	assert.Equal("E", qp.Next["C"], "Unexpected next node in query plan %v", q)
	assert.Equal("E", qp.Last, "Unexpected last node in query plan %v", q)

	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "B", Attribute: "attr1"},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	qp, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.NoError(err, "Couldn't make queryplan %v", q)
	assert.Equal("B", qp.Start, "Unexpected start node for query plan %v", q)
	assert.Equal("C", qp.Next["B"], "Unexpected next node in query plan %v", q)
	assert.Equal("C", qp.Last, "Unexpected last node in query plan %v", q)

	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "C", Attribute: "attr1"},
		},
		Filters: []types.Filter{
			{Owner: "B", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	qp, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.NoError(err, "Couldn't make queryplan %v", q)
	assert.Equal("C", qp.Start, "Unexpected start node for query plan %v", q)
	assert.Equal("B", qp.Next["C"], "Unexpected next node in query plan %v", q)
	assert.Equal("B", qp.Last, "Unexpected last node in query plan %v", q)

	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.CountFunction},
		},
		Filters: []types.Filter{
			{Owner: "E", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	qp, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.NoError(err, "Couldn't make queryplan %v", q)
	assert.Equal("C", qp.Start, "Unexpected start node for query plan %v", q)
	assert.Equal("E", qp.Next["C"], "Unexpected next node in query plan %v", q)
	assert.Equal("E", qp.Last, "Unexpected last node in query plan %v", q)

	// Test for error: no aggregate
	q = &types.Query{
		Aggregates: []types.Aggregate{},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject zero-aggregate query %v", q)

	// Test for error: two counts
	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.CountFunction},
			{Function: types.SumFunction, Owner: "A", Attribute: "attr2"},
			{Function: types.CountFunction},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject multi-count query %v", q)

	// Test for error: non-count without owner
	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "", Attribute: "attr1"},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject aggregate without owner, query %v", q)

	// Test for error: too many aggregate nodes
	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "B", Attribute: "attr1"},
			{Function: types.SumFunction, Owner: "A", Attribute: "attr2"},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject multi-node-aggregate query %v", q)

	// Test for error: nonexistent node
	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "Z", Attribute: "attr1"},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject query with nonexistent node %v", q)

	// Test for error: too few nodes
	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.CountFunction},
		},
		Filters: []types.Filter{
			{Owner: "C", Attribute: "attr15", Operator: ">", ReferenceValue: "4"},
		},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject one-node query %v", q)

	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.SumFunction, Owner: "C", Attribute: "attr15"},
		},
		Filters: []types.Filter{},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject one-node query %v", q)

	q = &types.Query{
		Aggregates: []types.Aggregate{
			{Function: types.CountFunction},
		},
		Filters: []types.Filter{},
	}
	_, err = QueryPlanFromQuery(q, []string{"A", "B", "C", "D", "E"})
	assert.Error(err, "Should reject zero-node query %v", q)
}
