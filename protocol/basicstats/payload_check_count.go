// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"errors"
	"math/big"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"github.com/rs/zerolog/log"
)

type checkCountPayload struct {
	EncryptedCount *big.Int
}

// QuickCheck makes sure there is a value in EncryptedCount
func (pl *checkCountPayload) QuickCheck() error {
	if pl.EncryptedCount == nil {
		return errors.New("missing encrypted count value")
	}
	return nil
}

// Contains the encrypted count of people who matched the filters. The starting node
// decrypts it, checks if it's large enough, and if it is, it requests the encrypted aggregates.
func (pl *checkCountPayload) Update(s *State) {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()
	// We should be able to decrypt this
	log.Info().Msg("Attempting decryption of count using dpaillier")

	// The second round does not need a checkCount round, since we saved it in the first round
	// CheckCount is processed by the reviewer, or if none exists, the start node.
	// As reviewer, we detect the second round by s.reviewData.status == ReviewFinished,
	// as starter we detect it by s.plaintextMeans != nil
	if s.reviewData.status == ReviewFinished || s.plaintextMeans != nil {
		pl.finishUpdate(s, big.NewInt(s.plaintextCount))
		return
	}

	// Initiate a sub-protocol to decrypt the count
	s.completedCallback(protocol.MadeByUs, protocol.SubProtocol, protocol.SubProtocolRequest{
		SubProtocolName: "dpaidecrypt",
		SubQueryID:      s.query.ID + ".checkCount",
		Input: &dpaidecrypt.Input{
			QueryID:     s.query.ID + ".checkCount",
			Ciphertexts: []*big.Int{pl.EncryptedCount},
			ReplyTo:     s.thisNode,
		},
		Options: map[string]interface{}{},
		CompletedCallback: func(madeByUs protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
			plaintexts, ok := payload.([]*big.Int)
			if resultKind == protocol.Success && ok && len(plaintexts) == 1 {
				pl.finishUpdate(s, plaintexts[0])
			} else {
				log.Error().Msgf("Result of paillier decryption protocol was not []*Int, it was %#v", payload)
				s.sendAborts("Error decrypting count", s.query.ID)
			}
		},
	})
}

func (pl *checkCountPayload) finishUpdate(s *State, count *big.Int) {
	s.plaintextCount = count.Int64()
	if s.plaintextCount < s.options.MinResultSize {
		// We will abort!
		log.Warn().Msgf("Aborting protocol, fewer results than minimum result size (%d < %d)", s.plaintextCount, s.options.MinResultSize)
		s.sendAborts("Fewer results than minimum result size", s.query.ID)
		return
	}
	if s.plaintextCount > s.options.MaxResultSize {
		// We will abort!
		log.Warn().Msgf("Aborting protocol, more results than maximum result size (%d > %d)", s.plaintextCount, s.options.MaxResultSize)
		s.sendAborts("More results than maximum result size", s.query.ID)
		return
	}
	log.Info().Msgf("count: %d", s.plaintextCount)

	// All checks passed. Request aggregates
	reply := countCheckSuccessfulPayload{}
	if err := s.send(s.queryPlan.Last, "countCheckSuccessful", &reply); err != nil {
		log.Err(err).Str("receiver", s.queryPlan.Last).Msg("checkCountPayload -> countCheckSuccessful: failed to send message")
		s.sendAborts("checkCountPayload -> countCheckSuccessful: failed to send message", s.query.ID)
		return
	}

	// Also submit the count for review if needed (the aggregates are submitted later in sendEncryptedAggregates)
	if len(s.options.Reviewer) != 0 && s.reviewData.status != ReviewFinished {
		if s.options.Reviewer != s.thisNode {
			log.Error().Str("reviewer", s.options.Reviewer).Str("us", s.thisNode).Msg("ended up with message for reviewer, but we are not the reviewer")
			s.sendAborts("ended up with message for reviewer, but we are not the reviewer", s.query.ID)
			return
		}
		s.reviewData.SetCount(s.plaintextCount)
		s.reviewData.StartReviewOnceIfReady(s)
	}
}
