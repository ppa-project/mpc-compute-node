// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"errors"
	"sort"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// QueryPlan specifies the order in which the nodes communicate
type QueryPlan struct {
	Start      string
	Next       map[string]string
	Last       string
	HasCounts  bool
	HasStdDevs bool
}

// QueryPlanFromQuery derives a QueryPlan from the provided query, also checks the query
func QueryPlanFromQuery(q *types.Query, allNodes []string) (*QueryPlan, error) {
	if len(q.Aggregates) < 1 {
		return nil, errors.New("missing compute functions in query")
	}

	// Count the number of count and standard deviation functions
	numCounts, numStdDevs := 0, 0
	for _, c := range q.Aggregates {
		if c.Function == types.CountFunction {
			numCounts++
		}
		if c.Function == types.MeanStdDevFunction {
			numStdDevs++
		}
	}
	if numCounts > 1 {
		return nil, errors.New("multiple COUNT in query is ambiguous")
	}

	// Exclude queries that contain multiple owners in the Aggregates
	// (this is not compatible with the current protocol)
	aggregateOwner := ""
	for _, c := range q.Aggregates {
		// Skip count functions
		if c.Owner == "" {
			if c.Function == types.CountFunction {
				continue
			} else {
				return nil, errors.New("query contains non-COUNT column without an owner")
			}
		}
		if len(aggregateOwner) == 0 {
			aggregateOwner = c.Owner
		} else if aggregateOwner != c.Owner {
			return nil, errors.New("multiple attribute owners in query function not supported")
		}
	}

	qp := &QueryPlan{
		Next:       make(map[string]string),
		HasCounts:  numCounts > 0,
		HasStdDevs: numStdDevs > 0,
	}

	// Get list of nodes involved in the Conditions, omitting the aggregate owner if present
	dataOwners := []string{}
	for i := range q.Filters {
		if q.Filters[i].Owner != aggregateOwner && !strSliceContains(dataOwners, q.Filters[i].Owner) {
			dataOwners = append(dataOwners, q.Filters[i].Owner)
		}
	}
	sort.Strings(dataOwners)

	// Add aggregate owner at the start
	if len(aggregateOwner) != 0 {
		dataOwners = append([]string{aggregateOwner}, dataOwners...)
	}

	// Check for error conditions: too few nodes or nodes that aren't in allNodes
	if len(dataOwners) < 2 {
		return nil, errors.New("query needs to contain data from at least two nodes")
	}
	for i := range dataOwners {
		if !strSliceContains(allNodes, dataOwners[i]) {
			return nil, errors.New("query contains node " + dataOwners[i] + " which is not in allNodes")
		}
	}

	// Set up the node order
	qp.Start = dataOwners[0]
	qp.Last = dataOwners[len(dataOwners)-1]
	for i := 1; i != len(dataOwners); i++ {
		qp.Next[dataOwners[i-1]] = dataOwners[i]
	}

	return qp, nil
}

func strSliceMin(slice []string) string {
	if len(slice) == 0 {
		return ""
	}
	min := 0
	for i := range slice {
		if slice[i] < slice[min] {
			min = i
		}
	}
	return slice[min]
}

func strSliceContains(haystack []string, needle string) bool {
	for i := range haystack {
		if haystack[i] == needle {
			return true
		}
	}
	return false
}
