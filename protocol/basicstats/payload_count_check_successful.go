// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"github.com/rs/zerolog/log"
)

type countCheckSuccessfulPayload struct {
	// We trust the Start node to do its check correctly (honest-but-curious model)
	// and therefore do not send the count at this stage (but at the end)
}

// QuickCheck always passes.
func (pl *countCheckSuccessfulPayload) QuickCheck() error {
	return nil
}

// This payload signifies that there are enough people in the filtered data set, and
// requests that the last filter node send back the aggregates.
//
// A branch occurs at this payload, depending on the value of options.Reviewer.
// If there is a reviewer, we blind the aggregates by adding an uniformly random value.
// The encrypted blinded aggregates are then sent to the party with the key (queryplan.Start)
// whereas the blinds are sent to the Reviewer. The blinded aggregates, once decrypted,
// are also sent to the reviewer, who then decides to either publish them or abort the
// protocol run.
// The decrypting party is responsible for publishing the Manual Review notice to the
// coordinator, whereas the Reviewer is responsible for checking stdev constraints.
//
// If there is no reviewer, the encrypted aggregates are sent to the party with the key,
// who performs final checks (standard deviation etc.) and then publishes the results.
func (pl *countCheckSuccessfulPayload) Update(s *State) {
	log := log.With().Str("queryid", s.query.ID).Str("node", s.thisNode).Logger()

	target := s.options.Reviewer
	if len(s.options.Reviewer) == 0 {
		target = s.queryPlan.Start
	}
	err := s.send(target, "sendEncryptedAggregates", &sendEncryptedAggregatesPayload{
		EncryptedAggregates: s.encryptedAggregates,
	})
	if err != nil {
		log.Err(err).Str("recipient", target).Msg("Error sending encrypted aggregates")
	}
}
