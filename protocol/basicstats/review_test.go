// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"sync"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

// These tests depend on the initialization in statemachine_test.go
func TestBasicstatsReviewsCountQuery(t *testing.T) {
	countQuery := &types.TaggedQuery{
		ID: "aaabbbaab",
		Query: &types.Query{
			Aggregates: []types.Aggregate{
				{Function: types.CountFunction},
			},
			Filters: []types.Filter{
				{Owner: "A", Attribute: "a1", Operator: "!=", ReferenceValue: "0"},
				{Owner: "B", Attribute: "b2", Operator: ">", ReferenceValue: "0"},
				{Owner: "C", Attribute: "c1", Operator: "==", ReferenceValue: "0"},
			},
		},
	}

	t.Run("Reviewer is Start, accepts", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"A",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{nil}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Middle, accepts", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		3, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"B",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{nil}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Final, accepts", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		3, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"C",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{nil}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Start, declines", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"A",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Middle, declines", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"B",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Final, declines", testQueryResultWithReview(
		countQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      countQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"C",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))
}

func TestBasicstatsReviewsOneRoundQuery(t *testing.T) {
	oneRoundQuery := &types.TaggedQuery{
		ID: "aaabbbaab",
		Query: &types.Query{
			Aggregates: []types.Aggregate{
				{Function: types.SumFunction, Owner: "A", Attribute: "a1"},
			},
			Filters: []types.Filter{
				{Owner: "B", Attribute: "b2", Operator: ">", ReferenceValue: "0"},
				{Owner: "C", Attribute: "c1", Operator: "==", ReferenceValue: "0"},
			},
		},
	}

	t.Run("Reviewer is Start, accepts", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"A",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{{2}}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Middle, accepts", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		3, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"B",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{{2}}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Final, accepts", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		3, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"C",              // Reviewer name
		true,             // Judgement
		2,                // Count
		[][]float64{{2}}, // Aggregates
		"",               // Error
	))

	t.Run("Reviewer is Start, declines", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"A",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Middle, declines", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"B",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Final, declines", testQueryResultWithReview(
		oneRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      oneRoundQuery,
			Count:      2,
			Aggregates: []float64{2},
		},
		"C",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))
}

func TestBasicstatsReviewsTwoRoundQuery(t *testing.T) {
	twoRoundQuery := &types.TaggedQuery{
		ID: "aaabbbaab",
		Query: &types.Query{
			Aggregates: []types.Aggregate{
				{Function: types.MeanStdDevFunction, Owner: "A", Attribute: "a1"},
			},
			Filters: []types.Filter{
				{Owner: "B", Attribute: "b2", Operator: ">", ReferenceValue: "0"},
				{Owner: "C", Attribute: "c1", Operator: "==", ReferenceValue: "0"},
			},
		},
	}

	t.Run("Reviewer is Start, accepts", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		7, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"A",                 // Reviewer name
		true,                // Judgement
		2,                   // Count
		[][]float64{{1, 0}}, // Aggregates
		"",                  // Error
	))

	t.Run("Reviewer is Middle, accepts", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		8, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"B",                 // Reviewer name
		true,                // Judgement
		2,                   // Count
		[][]float64{{1, 0}}, // Aggregates
		"",                  // Error
	))

	t.Run("Reviewer is Final, accepts", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		6, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"C",                 // Reviewer name
		true,                // Judgement
		2,                   // Count
		[][]float64{{1, 0}}, // Aggregates
		"",                  // Error
	))

	t.Run("Reviewer is Start, declines", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "A",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"A",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Middle, declines", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "B",
		},
		5, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"B",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))

	t.Run("Reviewer is Final, declines", testQueryResultWithReview(
		twoRoundQuery,
		map[string]interface{}{
			"minResultSize": int64(1),
			"reviewer":      "C",
		},
		2, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      twoRoundQuery,
			Count:      2,
			Aggregates: []float64{1},
		},
		"C",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))
}

func TestBasicstatsUninvolvedReviewer(t *testing.T) {
	query := &types.TaggedQuery{
		ID: "aaabbbaab",
		Query: &types.Query{
			Aggregates: []types.Aggregate{
				{Function: types.SumFunction, Owner: "B", Attribute: "b2"},
			},
			Filters: []types.Filter{
				{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
			},
		},
	}

	t.Run("A review is also requested if reviewer is not involved in query; accepts", testQueryResultWithReview(
		query,
		map[string]interface{}{"reviewer": "C"},
		4, // Messages before review
		3, // Messages after review
		ApprovalRequest{
			Query:      query,
			Count:      5,
			Aggregates: []float64{20},
		},
		"C",               // Reviewer name
		true,              // Judgement
		5,                 // Count
		[][]float64{{20}}, // Aggregates
		"",                // Error
	))

	t.Run("A review is also requested if reviewer is not involved in query; declines", testQueryResultWithReview(
		query,
		map[string]interface{}{"reviewer": "C"},
		4, // Messages before review
		2, // Messages after review
		ApprovalRequest{
			Query:      query,
			Count:      5,
			Aggregates: []float64{20},
		},
		"C",                              // Reviewer name
		false,                            // Judgement
		-1,                               // Count
		nil,                              // Aggregates
		"Query aborted by manual review", // Error
	))
}

func testQueryResultWithReview(
	query *types.TaggedQuery,
	options map[string]interface{},
	maxMessagesBeforeReview int,
	maxMessagesAfterReview int,
	resultingApprovalRequest ApprovalRequest,
	reviewerName string,
	reviewerJudgement bool,
	resultingCount int64,
	resultingAggregates [][]float64,
	errorMessage string,
) func(t *testing.T) {
	return func(t *testing.T) {
		assert := assert.New(t)
		numItems := 10
		dbA := database.NewTestDatabase("A", numItems)
		dbA.FillAttributePattern1("a1", 1)

		dbB := database.NewTestDatabase("B", numItems)
		dbB.FillAttributePattern2("b1", 15)
		dbB.FillAttributePattern1("b2", 4)
		dbB.FillAttributePattern3("b3", 7)

		dbC := database.NewTestDatabase("C", numItems)
		dbC.FillAttributePattern2("c1", 1)

		c := make(chan *protocol.Message)
		ms := protocol.NewChannelMessageSender(c)

		tk := &TestKeystore{M: make(map[string]paillier.Encrypter)}
		stateMachines := map[string]*State{
			"A": NewStateMachine(dbA, "A", func() ([]string, error) { return []string{"B", "C"}, nil }, ms, tk),
			"B": NewStateMachine(dbB, "B", func() ([]string, error) { return []string{"A", "C"}, nil }, ms, tk),
			"C": NewStateMachine(dbC, "C", func() ([]string, error) { return []string{"A", "B"}, nil }, ms, tk),
		}

		var callbackResult Result
		var callbackFailure string
		var approvalRequest ApprovalRequest
		var approvalRequestSetBy string
		var m sync.Mutex
		done := make(chan struct{})

		callbackFn := func(k string) protocol.CompletedCallback {
			return func(origin protocol.ResultOrigin, isSuccess protocol.ResultKind, payload interface{}) {
				if isSuccess == protocol.SubProtocol {
					spr := payload.(protocol.SubProtocolRequest)
					dpaiinput := spr.Input.(*dpaidecrypt.Input)
					spr.CompletedCallback(protocol.MadeByUs, protocol.Success, test.DPaiDecrypt(dpaiinput.Ciphertexts))
				} else if origin == protocol.MadeByUs {
					m.Lock()
					defer m.Unlock()
					switch isSuccess {
					case protocol.Success:
						callbackResult = *payload.(*Result)
					case protocol.Failure:
						callbackFailure = payload.(string)
					case protocol.ManualReview:
						approvalRequest = *payload.(*ApprovalRequest)
						approvalRequestSetBy = k
					}
					done <- struct{}{}
				}
			}
		}
		for k := range stateMachines {
			assert.Nil(stateMachines[k].Start(query, options, callbackFn(k)))
		}

		// No need to continue to pump messages if we failed
		if t.Failed() {
			t.FailNow()
		}

		// Pump messages
		nMessages := 0
		for nMessages < maxMessagesBeforeReview {
			select {
			case m := <-c: // Read a message from the channel
				t.Logf("Message from %v to %v, step: %v, length: %d", m.Sender, m.Receiver, m.StepID, len(m.Body))
				// Send the message along to the right recipient
				assert.Nil(stateMachines[m.Receiver].ProcessMessage(m))
				// Reset count
				nMessages++
			default:
				time.Sleep(50 * time.Millisecond)
			}
		}

		// Wait for the other goroutine to take the message from the channel and process it
		<-done

		// Check that the review was received
		m.Lock()
		assert.Equal(resultingApprovalRequest, approvalRequest, "The review assignment did not match")
		assert.Equal(reviewerName, approvalRequestSetBy, "The wrong node got the review assignment")
		m.Unlock()
		// No need to continue to pump messages if we failed
		if t.Failed() {
			t.FailNow()
		}

		// Submit the review judgement (sends a message, so use a goroutine)
		go func() {
			assert.Nil(stateMachines[reviewerName].SubmitReview(reviewerJudgement), "Error submitting review")
		}()

		// Pump messages
		nMessages = 0
		for nMessages < maxMessagesAfterReview {
			select {
			case m := <-c: // Read a message from the channel
				t.Logf("Message from %v to %v, step: %v, length: %d", m.Sender, m.Receiver, m.StepID, len(m.Body))
				// Send the message along to the right recipient
				assert.Nil(stateMachines[m.Receiver].ProcessMessage(m))
				// Reset count
				nMessages++
			default:
				time.Sleep(50 * time.Millisecond)
			}
		}

		// Wait again for messages to be processed
		<-done

		// Now check the outcome
		m.Lock()
		defer m.Unlock()

		if resultingCount == -1 {
			assert.Equal(errorMessage, callbackFailure, "The error message does not match")
		} else {
			assert.Equal(resultingCount, callbackResult.Count, "The result count does not match")
			assert.Equal(resultingAggregates, callbackResult.Aggregates, "The result aggregates do not match")
		}
	}
}
