// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package basicstats

import (
	"math/big"
	"os"
	"sync"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

type TestKeystore struct {
	M map[string]paillier.Encrypter
}

func (tk *TestKeystore) AddPaillierPublicKey(org string, pk *paillier.PublicKey) {
	tk.M[org] = pk
}

func (tk *TestKeystore) Encrypter(org string) paillier.Encrypter {
	return tk.M[org]
}

const FILENAME = "_testkey"

func TestMain(m *testing.M) {
	cleanup := test.Setup()

	code := m.Run()

	cleanup()
	os.Exit(code)
}

func TestBasicstatsQueries(t *testing.T) {
	t.Run("Just COUNT", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.CountFunction, Owner: "", Attribute: ""},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 6, 3, [][]float64{nil}, "",
	))
	t.Run("Just SUM", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 3, [][]float64{{45}}, "",
	))
	t.Run("Just MEAN", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 3, [][]float64{{15}}, "",
	))
	t.Run("Just MEANSTDEV", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: "!=", ReferenceValue: "7"},
				},
			},
		},
		nil, 12, 5, [][]float64{{9, 7.348469228349534}}, "",
	))
	t.Run("COUNT before MEANSTDEV", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "-1"},
				},
			},
		},
		nil, 10, 10, [][]float64{nil, {7.5, 7.5}}, "",
	))
	t.Run("Four different columns, COUNT first", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 12, 3, [][]float64{nil, {45}, {15}, {15, 0}}, "",
	))
	t.Run("Four different columns, SUM first", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
					{Function: types.CountFunction, Owner: "", Attribute: ""},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 12, 3, [][]float64{{45}, {15}, {15, 0}, nil}, "",
	))
	t.Run("Four different columns, MEAN first", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 12, 3, [][]float64{{15}, {15, 0}, nil, {45}}, "",
	))
	t.Run("Four different columns, MEANSTDEV first", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 12, 3, [][]float64{{15, 0}, nil, {45}, {15}}, "",
	))
	t.Run("Multiple SUM columns", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.SumFunction, Owner: "B", Attribute: "b2"},
					{Function: types.SumFunction, Owner: "B", Attribute: "b3"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 3, [][]float64{{45}, {12}, {7}}, "",
	))
	t.Run("Multiple MEAN columns", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b2"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b3"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 3, [][]float64{{15}, {4}, {7.0 / 3.0}}, "",
	))
}

func TestBasicstatsLimits(t *testing.T) {
	t.Run("MinResultSize is violated", testQueryResult(
		nil,
		map[string]interface{}{
			"minResultSize":       int64(4),
			"minResultPercentage": int64(0),
			"maxResultPercentage": int64(100),
		}, 4, -1, nil,
		"Fewer results than minimum result size",
	))
	t.Run("MinResultPercentage is violated", testQueryResult(
		nil,
		map[string]interface{}{
			"minResultSize":       int64(1),
			"minResultPercentage": int64(40),
			"maxResultPercentage": int64(100),
		}, 4, -1, nil,
		"Fewer results than minimum result size",
	))
	t.Run("MaxResultPercentage is violated", testQueryResult(
		nil,
		map[string]interface{}{
			"minResultSize":       int64(1),
			"minResultPercentage": int64(0),
			"maxResultPercentage": int64(20),
		}, 4, -1, nil,
		"More results than maximum result size",
	))

	t.Run("StdevConstraint is violated", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b1"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		map[string]interface{}{
			"stdevConstraints": []coordinator.StdevConstraint{
				{Owner: "B", Attribute: "b1", MinStdev: big.NewFloat(0.1)},
			},
		}, 12, -1, nil,
		"Standard deviation too small for column 1", // first violator
	))

	t.Run("StdevConstraint violation found in the presence of other columns", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.SumFunction, Owner: "B", Attribute: "b2"},
					{Function: types.MeanStdDevFunction, Owner: "B", Attribute: "b3"},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b1"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		map[string]interface{}{
			"stdevConstraints": []coordinator.StdevConstraint{
				{Owner: "B", Attribute: "b1", MinStdev: big.NewFloat(0.1)},
			},
		}, 12, -1, nil,
		"Standard deviation too small for column 3", // first violator
	))

	t.Run("StdevConstraint is checked even if no STDEVs need to be computed", testQueryResult(
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "B", Attribute: "b1"},
					{Function: types.CountFunction, Owner: "", Attribute: ""},
					{Function: types.MeanFunction, Owner: "B", Attribute: "b3"},
				},
				Filters: []types.Filter{
					{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		map[string]interface{}{
			"stdevConstraints": []coordinator.StdevConstraint{
				{Owner: "B", Attribute: "b1", MinStdev: big.NewFloat(0.1)},
			},
		}, 12, -1, nil,
		"Standard deviation too small for column 0",
	))
}

func TestBasicstatsMismatchingIdentifiers(t *testing.T) {
	numItems := 10
	var dbA, dbB, dbC *database.TestDatabase

	dbA = database.NewTestDatabase("A", numItems-2)
	dbA.FillAttributePattern1("a1", 1)

	dbB = database.NewTestDatabase("B", numItems)
	dbB.FillAttributePattern2("b1", 15)
	dbB.FillAttributePattern1("b2", 4)
	dbB.FillAttributePattern3("b3", 7)

	dbC = database.NewTestDatabase("C", numItems)
	dbC.FillAttributePattern2("c1", 1)

	t.Run("Query succeeds even if starting party lacks some identifiers", testQueryResultOnDatabase(
		[]*database.TestDatabase{dbA, dbB, dbC},
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "A", Attribute: "a1"},
				},
				Filters: []types.Filter{
					{Owner: "B", Attribute: "b1", Operator: "==", ReferenceValue: "15"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 4, [][]float64{{2}}, "",
	))

	dbA = database.NewTestDatabase("A", numItems)
	dbA.FillAttributePattern1("a1", 1)

	dbB = database.NewTestDatabase("B", numItems-2)
	dbB.FillAttributePattern2("b1", 15)
	dbB.FillAttributePattern1("b2", 4)
	dbB.FillAttributePattern3("b3", 7)

	dbC = database.NewTestDatabase("C", numItems)
	dbC.FillAttributePattern2("c1", 1)

	t.Run("Query succeeds even if middle party lacks some identifiers", testQueryResultOnDatabase(
		[]*database.TestDatabase{dbA, dbB, dbC},
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "A", Attribute: "a1"},
				},
				Filters: []types.Filter{
					{Owner: "B", Attribute: "b1", Operator: "==", ReferenceValue: "15"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 4, [][]float64{{3}}, "",
	))

	dbA = database.NewTestDatabase("A", numItems)
	dbA.FillAttributePattern1("a1", 1)

	dbB = database.NewTestDatabase("B", numItems)
	dbB.FillAttributePattern2("b1", 15)
	dbB.FillAttributePattern1("b2", 4)
	dbB.FillAttributePattern3("b3", 7)

	dbC = database.NewTestDatabase("C", numItems-2)
	dbC.FillAttributePattern2("c1", 1)

	t.Run("Query succeeds even if final party lacks some identifiers", testQueryResultOnDatabase(
		[]*database.TestDatabase{dbA, dbB, dbC},
		&types.TaggedQuery{
			ID: "aaabbbaab",
			Query: &types.Query{
				Aggregates: []types.Aggregate{
					{Function: types.SumFunction, Owner: "A", Attribute: "a1"},
				},
				Filters: []types.Filter{
					{Owner: "B", Attribute: "b1", Operator: "==", ReferenceValue: "15"},
					{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
				},
			},
		},
		nil, 7, 4, [][]float64{{3}}, "",
	))
}

func testQueryResult(query *types.TaggedQuery, options map[string]interface{}, maxMessages int, resultingCount int64, resultingAggregates [][]float64, errorMessage string) func(t *testing.T) {
	numItems := 10
	dbA := database.NewTestDatabase("A", numItems)
	dbA.FillAttributePattern1("a1", 1)

	dbB := database.NewTestDatabase("B", numItems)
	dbB.FillAttributePattern2("b1", 15)
	dbB.FillAttributePattern1("b2", 4)
	dbB.FillAttributePattern3("b3", 7)

	dbC := database.NewTestDatabase("C", numItems)
	dbC.FillAttributePattern2("c1", 1)

	dbs := []*database.TestDatabase{dbA, dbB, dbC}
	return testQueryResultOnDatabase(dbs, query, options, maxMessages, resultingCount, resultingAggregates, errorMessage)
}

func testQueryResultOnDatabase(databases []*database.TestDatabase, query *types.TaggedQuery, options map[string]interface{}, maxMessages int, resultingCount int64, resultingAggregates [][]float64, errorMessage string) func(t *testing.T) {
	return func(t *testing.T) {
		assert := assert.New(t)

		c := make(chan *protocol.Message)
		ms := protocol.NewChannelMessageSender(c)

		tk := &TestKeystore{M: make(map[string]paillier.Encrypter)}
		stateMachines := map[string]*State{
			"A": NewStateMachine(databases[0], "A", func() ([]string, error) { return []string{"B", "C"}, nil }, ms, tk),
			"B": NewStateMachine(databases[1], "B", func() ([]string, error) { return []string{"A", "C"}, nil }, ms, tk),
			"C": NewStateMachine(databases[2], "C", func() ([]string, error) { return []string{"A", "B"}, nil }, ms, tk),
		}

		var callbackResult Result
		var callbackFailure string
		var m sync.Mutex
		done := make(chan struct{})

		if query == nil {
			query = &types.TaggedQuery{
				ID: "aaabbbaab",
				Query: &types.Query{
					Aggregates: []types.Aggregate{
						{Function: types.CountFunction, Owner: "", Attribute: ""},
					},
					Filters: []types.Filter{
						{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
						{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
					},
				},
			}
		}
		if options == nil {
			options = map[string]interface{}{"minResultSize": int64(2)}
		}

		for k := range stateMachines {
			assert.Nil(stateMachines[k].Start(query, options, func(origin protocol.ResultOrigin, isSuccess protocol.ResultKind, payload interface{}) {
				if isSuccess == protocol.SubProtocol {
					spr := payload.(protocol.SubProtocolRequest)
					dpaiinput := spr.Input.(*dpaidecrypt.Input)
					spr.CompletedCallback(protocol.MadeByUs, protocol.Success, test.DPaiDecrypt(dpaiinput.Ciphertexts))
				} else if origin == protocol.MadeByUs {
					m.Lock()
					defer m.Unlock()
					if isSuccess == protocol.Success {
						callbackResult = *payload.(*Result)
					} else {
						callbackFailure = payload.(string)
					}
					done <- struct{}{}
				}
			}))
		}
		nMessages := 0
		for nMessages < maxMessages {
			select {
			case m := <-c: // Read a message from the channel
				t.Logf("Message from %v to %v, step: %v, length: %d", m.Sender, m.Receiver, m.StepID, len(m.Body))
				// Send the message along to the right recipient
				assert.Nil(stateMachines[m.Receiver].ProcessMessage(m))
				// Reset count
				nMessages++
			default:
				time.Sleep(50 * time.Millisecond)
			}
		}

		// Wait for the other goroutine to take the message from the channel and process it
		<-done

		// Now check the outcome
		m.Lock()
		defer m.Unlock()

		if resultingCount == -1 {
			assert.Equal(errorMessage, callbackFailure, "Error message is incorrect")
		} else {
			assert.Equal(resultingCount, callbackResult.Count, "Count is incorrect")
			assert.Equal(resultingAggregates, callbackResult.Aggregates, "Resulting aggregates are incorrect")
		}
	}
}
