// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package protocol

// StateMachine is the interface for a state machine of an MPC protocol
type StateMachine interface {
	// ProcessMessage takes in a MPC message and processes it the state machine
	ProcessMessage(m *Message) error

	// Start starts a protocol
	Start(input Input, options Options, callback CompletedCallback) error

	// GetInputType gets the input type of the protocol
	GetInputType() interface{}

	// SubmitReview submits a review after one has been requested via CompletedCallback
	SubmitReview(approved bool) error
}
