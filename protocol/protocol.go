// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package protocol provides the common interface for the supported MPC protocols.
package protocol

// Input represents the input of a statemachine
type Input = interface{}

// Result represents the result of a statemachine
type Result = interface{}

// Options represents any metadata supplied with a query.
type Options = map[string]interface{}

// ResultOrigin indicates whether a given result was made at the present node
// or passed from another node.
type ResultOrigin bool

const (
	// MadeByUs indicates that the result originate from the current state machine
	MadeByUs ResultOrigin = true
	// MadeElsewhere indicates that the result originate else where
	MadeElsewhere ResultOrigin = false
)

// ResultKind indicates whether a given result indicates success or failure,
// or an intermediate manual review step
type ResultKind int

const (
	// Failure indicates that the protocol resulted in a failure state
	Failure ResultKind = iota
	// Success indicates that the protocol resulted in a success state
	Success
	// ManualReview indicates that the protocol is awaiting manual review
	ManualReview
	// SubProtocol indicates that the protocol would like to start a sub-protocol,
	// and is awaiting a result from that (e.g. Paillier shared decryption)
	SubProtocol
)

func (rk ResultKind) String() string {
	return [...]string{"Failure", "Success", "Call for Manual Review", "Sub-protocol instantiation"}[rk]
}

// CompletedCallback is the kind of function that a Protocol should call when it is
// done (or has aborted). Depending on the flags, payload will be a Result or a string
// describing the cause of failure.
type CompletedCallback func(madeByUs ResultOrigin, resultKind ResultKind, payload interface{})

// ApprovalRequest is a package that contains a review to be processed by a human
type ApprovalRequest interface {
	Protocol() string
	QueryID() string
	Summary() string
}

// SubProtocolRequest is a package that contains information necessary to start a subprotocol
type SubProtocolRequest struct {
	SubProtocolName   string
	SubQueryID        string
	Input             Input
	Options           Options
	CompletedCallback CompletedCallback
}
