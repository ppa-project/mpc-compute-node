// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package silly implements a trivial MPC protocol, mainly for use in tests.
package silly

import (
	"encoding/json"
	"errors"
	"reflect"
	"sync"

	"github.com/rs/zerolog/log"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

var protocolName = "silly"

// State is a trivial protocol to test the protocol state machines
type State struct {
	mux         sync.Mutex
	count       int
	started     bool
	contributed bool
	queryID     string
	ms          protocol.MessageSender
	me          string
	neighbor    string // The neighbor we're going to send the message to
	c           chan payload
	done        sync.WaitGroup
	callback    protocol.CompletedCallback
}

// Start starts the protocol. Input, a string, should be the name of the node
// that starts the protocol.
func (p *State) Start(input protocol.Input, options protocol.Options, callback protocol.CompletedCallback) error {
	startPayload, ok := input.(*StartPayload)
	if !ok {
		log.Error().Stringer("inputType", reflect.TypeOf(input)).Msg("Input is not of type *StartPayload")
		return errors.New("input is not of type *StartPayload")
	}

	p.queryID = startPayload.ID
	p.callback = callback
	p.done.Add(1)

	p.mux.Lock()
	p.c = make(chan payload)
	p.mux.Unlock()

	// Start a goroutine for this instance of the protocol
	go func() {
		// Wait for payloads to come in on the channel
		for sm := range p.c {
			sm.Update(p)
		}
	}()

	// Place the start message on the channel
	p.c <- startPayload
	return nil
}

// StartPayload is the payload that starts the protocol
type StartPayload struct {
	ID      string
	Starter string
}

// Update this payload contains the start payload and starts to protocol
func (r *StartPayload) Update(p *State) {
	if p.me == r.Starter {
		p.started = true
		mBody, err := json.Marshal(roundPayload{Count: p.count + 1})
		if err != nil {
			log.Fatal().Err(err).Msg("Marshal error")
		}
		_ = p.ms.SendMessage(protocol.NewMessage(protocolName, p.me, p.neighbor, p.queryID, "0", mBody))
	}
}

// payload is a message that can be processed and checked
type payload interface {
	Update(p *State)
}

// roundPayload is the message sent for each step of this protocol
type roundPayload struct {
	Count int
}

// Update processes this payload and updates the protocol state
func (r *roundPayload) Update(p *State) {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.count = r.Count
	if p.started && p.contributed {
		return
	}
	if p.started || p.contributed {
		// We started, let's stop here, we do not want this to
		// go on forever, right?
		log.Debug().Msgf("[%v] We are full circle and there are %d of us!\n", p.me, p.count)
		newBody, err := json.Marshal(roundPayload{Count: p.count})
		if err != nil {
			log.Err(err).Msg("Marshal error")
		}
		_ = p.ms.SendMessage(protocol.NewMessage(protocolName, p.me, p.neighbor, p.queryID, "sendResult", newBody))
		p.contributed = true
		p.done.Done()
		if p.callback != nil {
			p.callback(protocol.MadeByUs, protocol.Success, p.count)
		}
	} else {
		newBody, err := json.Marshal(roundPayload{Count: p.count + 1})
		if err != nil {
			log.Err(err).Msg("Marshal error")
		}
		_ = p.ms.SendMessage(protocol.NewMessage(protocolName, p.me, p.neighbor, p.queryID, "0", newBody))
		p.contributed = true
	}
}

// ProcessMessage processes the provided messages in its state machine
func (p *State) ProcessMessage(m *protocol.Message) error {
	var mBody roundPayload
	err := json.Unmarshal(m.Body, &mBody)
	if err != nil {
		return err
	}

	p.mux.Lock()
	defer p.mux.Unlock()
	p.c <- &mBody

	return nil
}

// GetCount returns the count value
func (p *State) GetCount() int {
	p.done.Wait()
	// Technically, we can still get data-races here!
	p.mux.Lock()
	defer p.mux.Unlock()
	r := p.count
	return r
}

// GetInputType returns the input type of the protocol
func (p *State) GetInputType() interface{} {
	return "string"
}

// SubmitReview submits a review (not implemented)
func (p *State) SubmitReview(approved bool) error {
	return errors.New("Unimplemented")
}

// NewProtocol creates a new protocol. me is the name of this node; neighbor is
// the name of the node that we should send our message to when we're done.
func NewProtocol(me string, neighbor string, ms protocol.MessageSender) *State {
	return &State{count: 0, me: me, neighbor: neighbor, ms: ms}
}
