// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package silly

import (
	"os"
	"sync"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	test.Setup()

	code := m.Run()
	os.Exit(code)
}

func TestProtocol(t *testing.T) {
	assert := assert.New(t)
	c := make(chan *protocol.Message)
	ms := protocol.NewChannelMessageSender(c)
	stateMachines := map[string]*State{
		"A": NewProtocol("A", "B", ms),
		"B": NewProtocol("B", "C", ms),
		"C": NewProtocol("C", "D", ms),
		"D": NewProtocol("D", "A", ms),
	}
	var m sync.Mutex
	callbackCount := 0
	for k := range stateMachines {
		startPayload := &StartPayload{ID: "1", Starter: "A"}
		stateMachines[k].Start(startPayload, nil, func(madeByUs protocol.ResultOrigin, isSuccess protocol.ResultKind, payload interface{}) {
			if isSuccess == protocol.Success {
				m.Lock()
				defer m.Unlock()
				callbackCount = payload.(int)
			}
		})
	}
	// Wait for the channel to fill
	time.Sleep(100 * time.Millisecond)
	running := true
	for running {
		select {
		case m := <-c: // Read a message from the channel
			t.Logf("Message: %v", m)
			// Send the message along to the right recipient
			assert.Nil(stateMachines[m.Receiver].ProcessMessage(m))
			// Wait for the channel to fill
			time.Sleep(100 * time.Millisecond)

		default:
			t.Log("No values on channel, protocol done.")
			running = false
		}
	}
	// Now check the outcome

	// Wait a while before we go read stuff
	time.Sleep(3 * time.Second)
	assert.Equal(4, stateMachines["A"].GetCount(), "Unexpected protocol result (GetCount)")
	m.Lock()
	defer m.Unlock()
	assert.Equal(4, callbackCount, "Unexpected protocol result (callback)")
}
