// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dpaidecrypt

import (
	"fmt"
	"math/big"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"
)

// Payload is the common interface to all payload types.
// All methods are private because you have to run them with the state mutex held.
type Payload interface {
	// Update uses the payload to update the state
	update(s *State)
	// QuickCheck does some integrity checks on the payload and whether it is appropriate
	// for the current state. Does not make any changes.
	quickCheck(s *State) error
}

// getPayload takes a stepID and returns an instance of Payload
func getPayload(stepID string) (Payload, error) {
	if stepID == "partials" {
		return &partialsPayload{}, nil
	}
	return nil, fmt.Errorf("unknown stepID '%s'", stepID)
}

type startPayload struct{}

// QuickCheck always succeeds for a startPayload.
func (pl *startPayload) quickCheck(s *State) error { return nil }

// Update sends the messages with partial decryptions
func (pl *startPayload) update(s *State) {
	partials := make([]dpaillier.PartialDecryption, len(s.ciphertexts))
	for i := range s.ciphertexts {
		partials[i] = *s.keyshare.PartiallyDecrypt(s.ciphertexts[i])
	}

	payload := &partialsPayload{Partials: partials, From: s.nodeNames[s.thisNode]}

	// Send the partials to the others (and ourselves)
	for recipient := range s.nodeNames {
		// If there is a replyTo node, only send the reply to that node
		if len(s.replyTo) != 0 && s.nodeNames[recipient] != s.replyTo {
			continue
		}
		go func(recipient int) {
			// Wait a while before sending the first partials
			// This is not strictly necessary, but speeds up protocol execution.
			// The three state machines are necessarily started sequentially, and
			// immediately send the "partials" payload. This guarantees that at
			// least one such payload will initially be refused with 404 "unknown
			// query id", causing a retry in a minute. Two seconds delay removes
			// the sixty second back-off.
			time.Sleep(2 * time.Second)
			s.send(recipient, "partials", payload)
		}(recipient)
	}

	// If there is a replyTo node and it's not us, this concludes the protocol
	if len(s.replyTo) != 0 && s.nodeNames[s.thisNode] != s.replyTo && s.completedCallback != nil {
		s.completedCallback(protocol.MadeElsewhere, protocol.Success, []*big.Int{})
	}
}

// Partial decryptions from another node.
type partialsPayload struct {
	From     string
	Partials []dpaillier.PartialDecryption
}

// QuickCheck performs quick integrity checks on the partialsPayload.
func (pl *partialsPayload) quickCheck(s *State) error {
	if len(pl.Partials) != len(s.ciphertexts) {
		return fmt.Errorf("wrong number of partial decryptions, payload has %d but state needs %d", len(pl.Partials), len(s.ciphertexts))
	}
	return nil
}

// Update completes the decryptions
func (pl *partialsPayload) update(s *State) {
	// Save the messages
	s.partials = append(s.partials, pl.Partials)
	// If not yet enough messages, we're done
	if len(s.partials) < len(s.nodeNames) {
		return
	}

	// Decrypt each of the ciphertexts
	plaintexts := make([]*big.Int, len(s.ciphertexts))
	ciphertexts := make([]*dpaillier.PartialDecryption, len(s.nodeNames))
	for i := range plaintexts {
		for j := range s.partials {
			ciphertexts[j] = &s.partials[j][i]
		}
		plaintexts[i] = s.keyshare.Decrypt(ciphertexts)
	}

	// Report success
	if s.completedCallback != nil {
		s.completedCallback(protocol.MadeByUs, protocol.Success, plaintexts)
	}
}
