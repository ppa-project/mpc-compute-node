// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package transport

import (
	"context"
	"errors"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"github.com/rs/zerolog/log"
)

// MessageSenderMock implements the MessageSender interface.
// This struct is able to call services of other parties directly and will be used to test protocols.
type MessageSenderMock struct {
	cbMap map[string]func(protocol string, m *protocol.Message) error
}

// NewMessageSenderMock Constructor for MessageSenderMock
func NewMessageSenderMock(cbMap map[string]func(protocol string, m *protocol.Message) error) (*MessageSenderMock, error) {
	return &MessageSenderMock{cbMap: cbMap}, nil
}

// SendMessage sends a message by calling the service of the other party directly
func (ms *MessageSenderMock) SendMessage(m *protocol.Message) error {
	receiver := m.Receiver

	cb, ok := ms.cbMap[receiver]
	if !ok {
		log.Error().Str("receiver", receiver).Msg("Failed to send message: receiver does not exist")
		return errors.New("failed to send message: receiver does not exist")
	}
	return cb(m.Protocol, m)
}

// SendMessageWithDeliveryEnsurance sends a message by calling the service of the other party directly
func (ms *MessageSenderMock) SendMessageWithDeliveryEnsurance(_ context.Context, m *protocol.Message, _ protocol.MessageDeliveryFailureCallback) {
	// No support for delivery ensurance, fallback to SendMessage
	_ = ms.SendMessage(m)
}

// RequestStartProtocol is unimplemented
func (ms *MessageSenderMock) RequestStartProtocol(receiver, protocolname string, protocolData []byte) error {
	// Unsupported
	panic("Unimplemented")
}
