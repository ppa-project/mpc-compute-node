// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package transport

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"regexp"
	"strconv"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/stretchr/testify/assert"
)

var testDataPath = path.Join("..", "test", "testdata")

var config h.Config
var pool map[string]h.Config

func TestMain(m *testing.M) {
	// TODO: test.Setup()
	viper.SetConfigFile(path.Join(testDataPath, "PartyA", "mpc_node-localhost.toml"))
	viper.ReadInConfig()

	code := m.Run()
	Teardown()
	os.Exit(code)
}

func Setup(t *testing.T, clientCertInPool int) *httptest.Server {
	handlerFunc := func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		if bytes.Equal(body, []byte("Return error")) {
			http.Error(w, "Error!", http.StatusInternalServerError)
			return
		}
		if r.URL.Path == "/health" {
			fmt.Fprintf(w, "{\"status\": \"ok\"}")
			return
		}
		fmt.Fprintf(w, "Hello, %s", r.Proto)
	}
	return SetupWithHandler(t, clientCertInPool, handlerFunc)
}

func SetupWithHandler(t *testing.T, clientCertInPool int, handlerFunc func(http.ResponseWriter, *http.Request)) *httptest.Server {
	assert.GreaterOrEqual(t, clientCertInPool, 0)
	assert.LessOrEqual(t, clientCertInPool, 2)

	// Setup HTTPS Mock Server
	ts := httptest.NewUnstartedServer(http.HandlerFunc(handlerFunc))

	// Add PartyA to client pool
	caCertPool := x509.NewCertPool()
	if clientCertInPool > 0 {
		caCert, _ := ioutil.ReadFile(path.Join(testDataPath, "PartyA", "tls", "PartyA-localhost.crt"))
		caCertPool.AppendCertsFromPEM(caCert)
	}
	if clientCertInPool > 1 {
		caCert, _ := ioutil.ReadFile(path.Join(testDataPath, "PartyC", "tls", "PartyC-localhost.crt"))
		caCertPool.AppendCertsFromPEM(caCert)
	}

	tlsConfig := &tls.Config{
		ClientCAs:  caCertPool,
		ClientAuth: tls.RequireAndVerifyClientCert,
	}
	ts.TLS = tlsConfig

	// Start HTTPS Mock Server
	ts.StartTLS()

	// Obtain Mock Server information (host, port, certificate)
	log.Debug().Msgf("Run tests on mock HTTPS server on: %s\n", ts.URL)
	re := regexp.MustCompile(`https://(?P<host>.*):(?P<port>[0-9]+)`)
	res := re.FindStringSubmatch(ts.URL)
	host := res[1]
	port, _ := strconv.Atoi(res[2])

	cert := ts.Certificate()

	err := os.MkdirAll("testdata", 0744)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create testdata directory")
	}
	certOut, err := os.Create("testdata/cert.crt")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to open cert.crt for writing")
	}
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Raw}); err != nil {
		log.Fatal().Err(err).Msg("Failed to write data to cert.crt")
	}
	if err := certOut.Close(); err != nil {
		log.Fatal().Err(err).Msg("Error closing cert.crt")
	}

	// Bootstrap initial test dependencies

	config = h.Config{
		Name:        "PartyA",
		Port:        1234,
		Host:        "1.2.3.4",
		TLSCertFile: path.Join(testDataPath, "PartyA", "tls", "PartyA-localhost.crt"),
		TLSKeyFile:  path.Join(testDataPath, "PartyA", "tls", "PartyA-localhost.key"),
	}

	pool = make(map[string]h.Config)

	pool["PartyB"] = h.Config{
		Name:        "PartyB",
		Port:        port,
		Host:        host,
		TLSCertFile: "testdata/cert.crt",
	}

	pool["PartyC"] = h.Config{
		Name:        "PartyC",
		Port:        port,
		Host:        host,
		TLSCertFile: "testdata/cert.crt",
	}

	return ts
}

func Teardown() {
	os.RemoveAll("testdata")
}

func TestMessageSenderSingleMsg(t *testing.T) {
	Setup(t, 1)
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	assert.NoError(t, ms.SendMessage(msg))
}

func TestMessageSenderMultiMsg(t *testing.T) {
	Setup(t, 2)
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	assert.NoError(t, ms.SendMessage(msg))

	msg = &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyC",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	assert.NoError(t, ms.SendMessage(msg))

	msg = &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "1",
		Body:     []byte("Hello!"),
	}
	assert.NoError(t, ms.SendMessage(msg))

	msg = &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyC",
		QueryID:  "1234",
		StepID:   "1",
		Body:     []byte("Hello!"),
	}
	assert.NoError(t, ms.SendMessage(msg))
}

func TestMessageSenderInvalidClientCert(t *testing.T) {
	Setup(t, 0)
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	assert.Error(t, ms.SendMessage(msg))
}

func TestMessageSenderReceiverNotInPool(t *testing.T) {
	Setup(t, 2)

	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "SomeoneElse",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	assert.Error(t, ms.SendMessage(msg))
}

func TestMessageSenderReceiverReturnsErrorStatus(t *testing.T) {
	Setup(t, 2)

	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Return error"),
	}
	assert.Error(t, ms.SendMessage(msg))
}

func TestNewMessageSenderInvalidX509Keypair(t *testing.T) {
	Setup(t, 2)
	config.TLSKeyFile = "/path/does/not/exist"
	_, err := NewMessageSenderHTTP(config, pool)
	assert.Error(t, err)
}

func TestNewMessageSenderInvalidPoolCertPath(t *testing.T) {
	Setup(t, 2)
	c := pool["PartyB"]
	c.TLSCertFile = "/path/does/not/exist"
	pool["PartyB"] = c
	_, err := NewMessageSenderHTTP(config, pool)
	assert.Error(t, err)
}

func TestNewMessageSenderInvalidPoolCert(t *testing.T) {
	Setup(t, 2)
	c := pool["PartyB"]
	c.TLSCertFile = "messagesender_test.go"
	pool["PartyB"] = c
	_, err := NewMessageSenderHTTP(config, pool)
	assert.Error(t, err)
}

func TestMessageSenderHealth(t *testing.T) {
	Setup(t, 1)
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	_, err = ms.CheckHealth("PartyB")
	assert.NoError(t, err)
}

func TestMessageSenderNotHTTPError(t *testing.T) {
	Setup(t, 2)
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyBd",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err = ms.SendMessage(msg)
	assert.Error(t, err)
	_, ok := err.(*HTTPError)
	assert.False(t, ok)
}

func TestMessageSenderHTTPError(t *testing.T) {
	Setup(t, 2)
	c := pool["PartyB"]
	c.Port = 60002
	pool["PartyB"] = c
	ms, err := NewMessageSenderHTTP(config, pool)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	err = ms.SendMessage(msg)
	assert.Error(t, err)
	_, ok := err.(*HTTPError)
	assert.True(t, ok)
}
