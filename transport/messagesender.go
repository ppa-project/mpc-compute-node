// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package transport provides the layer that translates MPC messages to HTTP requests.
package transport

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/messages"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/spf13/viper"
)

// MessageSender generalizes the MessageSender methods
type MessageSender interface {
	SendMessage(m *protocol.Message) error
	CheckHealth(node string) (map[string]string, error)
}

// MessageSenderHTTP implements the MessageSender interface
type MessageSenderHTTP struct {
	sync.Mutex
	config          h.Config
	pool            map[string]h.Config
	clients         map[string]*http.Client
	messageQueue    chan DelayedMessageHTTP
	DelayedMessages int
}

// DelayedMessageHTTP wraps protocol.Message with metadata
type DelayedMessageHTTP struct {
	message    *protocol.Message
	err        error
	retryCount int
	errCb      protocol.MessageDeliveryFailureCallback
	ctx        context.Context
}

// NewMessageSenderHTTP Constructor for MessageSenderHTTP
func NewMessageSenderHTTP(config h.Config, pool map[string]h.Config) (*MessageSenderHTTP, error) {
	return NewMessageSenderHTTPWithDelay(config, pool, 1*time.Minute, 120) // 120 * 1 minute = 2 hours
}

// NewMessageSenderHTTPWithDelay Constructor for MessageSenderHTTP and dynamic delay for delayed messages
func NewMessageSenderHTTPWithDelay(config h.Config, pool map[string]h.Config, delay time.Duration, retryCount int) (*MessageSenderHTTP, error) {
	clients := make(map[string]*http.Client)

	for _, otherNode := range pool {
		// Add certificates of other nodes, to be able to do client-auth
		cli, err := createClient(config, otherNode)
		if err == nil {
			clients[otherNode.Name] = cli
		} else {
			return nil, err
		}
	}
	messageQueue := make(chan DelayedMessageHTTP, 100)
	ms := &MessageSenderHTTP{config: config, pool: pool, clients: clients, messageQueue: messageQueue, DelayedMessages: 0}

	go ms.messagePump(delay, retryCount)
	return ms, nil
}

func (ms *MessageSenderHTTP) messagePump(delay time.Duration, retryCount int) {
	for {
		m := <-ms.messageQueue
		if m.retryCount >= retryCount {
			log.Error().Int("retryCount", m.retryCount).Str("receiver", m.message.Receiver).Str("queryID", m.message.QueryID).Msg("Failed to deliver message, maximum number of retries reached.")
			if m.errCb != nil {
				m.errCb(m.message.QueryID, m.message.Receiver, fmt.Errorf("failed to deliver message, maximum number of retries reached"))
			}
			continue
		}
		ms.Lock()
		ms.DelayedMessages++
		ms.Unlock()
		go func() {
			select {
			case <-time.After(delay):
				log.Trace().Int("retryCount", m.retryCount).Str("receiver", m.message.Receiver).Str("queryID", m.message.QueryID).Msg("Retrying to send delayed message")
				ms.Lock()
				ms.DelayedMessages--
				ms.Unlock()
				ms.sendMessageEnsureWithRetryCount(m.ctx, m.message, m.retryCount, m.errCb)
			case <-m.ctx.Done():
				log.Err(m.ctx.Err()).Str("receiver", m.message.Receiver).Str("queryID", m.message.QueryID).Msg("Failed to deliver message, canceled by sender.")
				ms.Lock()
				ms.DelayedMessages--
				ms.Unlock()
				if m.errCb != nil {
					m.errCb(m.message.QueryID, m.message.Receiver, fmt.Errorf("failed to deliver message, canceled by sender: %v", m.ctx.Err()))
				}
			}
		}()
	}
}

// RequestStartProtocol tells another MPC node to start a protocol by executing a HTTP request
func (ms *MessageSenderHTTP) RequestStartProtocol(receiver, protocolname string, protocolData []byte) error {
	urlPath := fmt.Sprintf("/%v/query/%v", viper.GetString("version"), protocolname)
	_, err := ms.SendRequest(receiver, "POST", urlPath, &protocolData)
	return err
}

// SendMessage sends a message to another MPC node by executing a HTTP request
func (ms *MessageSenderHTTP) SendMessage(m *protocol.Message) error {
	urlPath := fmt.Sprintf("/%v/query/%v/%v/step/%v",
		viper.GetString("version"),
		m.Protocol,
		m.QueryID,
		m.StepID)

	_, err := ms.SendRequest(m.Receiver, "POST", urlPath, &m.Body)
	return err
}

// SendMessageWithDeliveryEnsurance sends a message to another MPC node and ensures the delivery of the message to the receiver
func (ms *MessageSenderHTTP) SendMessageWithDeliveryEnsurance(ctx context.Context, m *protocol.Message, errCb protocol.MessageDeliveryFailureCallback) {
	ms.sendMessageEnsureWithRetryCount(ctx, m, 0, errCb)
}

func (ms *MessageSenderHTTP) sendMessageEnsureWithRetryCount(ctx context.Context, m *protocol.Message, retryCount int, errCb protocol.MessageDeliveryFailureCallback) {
	err := ms.SendMessage(m)
	if err != nil {
		log.Warn().Err(err).Msg("SendMessageEnsure: failed to send message")
		if ctx == nil {
			ctx = context.Background()
		}
		ms.messageQueue <- DelayedMessageHTTP{
			message:    m,
			err:        err,
			retryCount: retryCount + 1,
			errCb:      errCb,
			ctx:        ctx,
		}
	}
}

// CheckHealth check the health and connectivity to another node
func (ms *MessageSenderHTTP) CheckHealth(node string) (map[string]string, error) {
	urlPath := "/health"

	resp, err := ms.SendRequest(node, "GET", urlPath, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to send request: %w", err)
	}

	var healthResponse messages.HealthResponse
	err = json.Unmarshal(resp, &healthResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal json: %w", err)
	}
	return healthResponse.Neighbors, err
}

// SendRequest sends a HTTP request to another MPC node
func (ms *MessageSenderHTTP) SendRequest(receiverName string, method string, urlPath string, reqBody *[]byte) ([]byte, error) {
	ms.Lock()
	receiver, exists := ms.pool[receiverName]
	if !exists {
		ms.Unlock()
		return []byte(""), fmt.Errorf("cannot find receiver %q in pool %v", receiverName, ms.pool)
	}

	client, exists := ms.clients[receiverName]
	if !exists {
		cli, err := createClient(ms.config, ms.pool[receiverName])
		if err == nil {
			ms.clients[receiverName] = cli
			client = cli
		} else {
			ms.Unlock()
			return []byte(""), fmt.Errorf("failed to intitialize client for receiver %q: %w", receiverName, err)
		}
	}
	ms.Unlock()

	connectionURL := fmt.Sprintf("https://%v:%d%v",
		receiver.Host,
		receiver.Port,
		urlPath)

	log.Trace().
		Str("sender", ms.config.Name).
		Str("receiver", receiver.Name).
		Str("method", method).
		Str("url", urlPath).
		Msgf("  [%s] <<< %s %s", receiver.Name, method, urlPath)

	var body *bytes.Reader
	if reqBody != nil {
		body = bytes.NewReader(*reqBody)
	}

	var resp *http.Response
	var err error

	switch method {
	case "POST":
		resp, err = client.Post(
			connectionURL,
			"application/octet-stream",
			body)
	case "GET":
		resp, err = client.Get(connectionURL)
	default:
		return []byte(""), fmt.Errorf("unknown HTTP method %s", method)
	}
	if err != nil {
		log.Err(err).
			Str("sender", ms.config.Name).
			Str("receiver", receiver.Name).
			Str("method", method).
			Str("url", urlPath).
			Msgf("  [%s] <xx %s %s", receiver.Name, method, urlPath)
		ms.Lock()
		delete(ms.clients, receiverName)
		ms.Unlock()
		return []byte(""), &HTTPError{Err: err}
	}

	defer func() {
		if err = resp.Body.Close(); err != nil {
			log.Trace().Err(err).Msg("Failed to close body")
		}
	}()

	var event *zerolog.Event
	var httpError error
	switch code := resp.StatusCode; {
	case (code >= 400) && (code <= 599):
		event = log.Error()
		httpError = &HTTPError{StatusCode: resp.StatusCode}
	default:
		event = log.Trace()
	}
	event.
		Str("sender", ms.config.Name).
		Str("receiver", receiver.Name).
		Str("method", method).
		Str("url", urlPath).
		Int("status", resp.StatusCode)

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte(""), fmt.Errorf("failed to read body from http response: %w", &HTTPError{Err: err})
	}
	event.
		Str("response-body", string(respBody)).
		Msgf("  [%s] >>> %s %s %d", receiver.Name, "POST", urlPath, resp.StatusCode)

	return respBody, httpError
}

func createClient(config h.Config, otherNode h.Config) (*http.Client, error) {
	// Load certificates and keys
	cert, err := tls.LoadX509KeyPair(config.TLSCertFile, config.TLSKeyFile)
	if err != nil {
		return nil, fmt.Errorf("failed to load X509 Keypair: %w", err)
	}

	caCertPool := x509.NewCertPool()
	caCert, err := ioutil.ReadFile(otherNode.TLSCertFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read TLS Certificate: %w", err)
	}
	if ok := caCertPool.AppendCertsFromPEM(caCert); !ok {
		return nil, fmt.Errorf("failed to parse TLS Certificate - node %s", otherNode.Name)
	}

	// Create TLS Config
	tlsConfig := &tls.Config{
		MinVersion:   tls.VersionTLS12,
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
	}

	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
		MaxIdleConns:    3,
		IdleConnTimeout: 30 * time.Second,
	}
	return &http.Client{Transport: tr}, nil
}

// NodeConfigurationUpdate implements coordinator.NodeConfigListener
func (ms *MessageSenderHTTP) NodeConfigurationUpdate(org, _, _, _, paillierkey string) {
	ms.Lock()
	defer ms.Unlock()
	delete(ms.clients, org)
}
