// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package transport

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"github.com/stretchr/testify/assert"
)

func SetupFaulty(t *testing.T, clientCertInPool int) *httptest.Server {
	faultyCount := 0
	handlerFunc := func(w http.ResponseWriter, r *http.Request) {
		if faultyCount < 2 {
			http.Error(w, "faulty", http.StatusConflict)
			faultyCount++
			return
		}
		fmt.Fprintf(w, "Hello, %s", r.Proto)
	}
	return SetupWithHandler(t, clientCertInPool, handlerFunc)
}

func TestMessageSenderDeliveryEnsurance(t *testing.T) {
	SetupFaulty(t, 2)
	ms, err := NewMessageSenderHTTPWithDelay(config, pool, 1*time.Second, 100)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}

	ch := make(chan bool)
	ms.SendMessageWithDeliveryEnsurance(context.Background(), msg, func(queryID, receiver string, err error) {
		close(ch)
	})

	time.Sleep(1 * time.Second)
	retries := 20
	for retries > 0 {
		ms.Lock()
		delayedMessages := ms.DelayedMessages
		ms.Unlock()
		if delayedMessages == 0 {
			break
		}
		time.Sleep(500 * time.Millisecond)
		retries--
	}
	ms.Lock()
	assert.Equal(t, 0, ms.DelayedMessages)
	ms.Unlock()

	select {
	case <-ch:
		assert.FailNow(t, "Error cb called, but not expected")
	default:
		break
	}
}

func TestMessageSenderDeliveryEnsuranceError(t *testing.T) {
	SetupFaulty(t, 2)
	ms, err := NewMessageSenderHTTPWithDelay(config, pool, 1*time.Second, 1)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyBd",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	ch := make(chan bool)
	ms.SendMessageWithDeliveryEnsurance(context.Background(), msg, func(queryID, receiver string, err error) {
		close(ch)
	})
	select {
	case <-time.After(5 * time.Second):
		assert.FailNow(t, "Timeout")
	case <-ch:
		break
	}
}

func TestMessageSenderDeliveryEnsuranceMaxRetries(t *testing.T) {
	SetupFaulty(t, 1)
	ms, err := NewMessageSenderHTTPWithDelay(config, pool, 1*time.Second, 2)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	ch := make(chan bool)
	ms.SendMessageWithDeliveryEnsurance(context.Background(), msg, func(queryID, receiver string, err error) {
		close(ch)
	})

	time.Sleep(1 * time.Second)
	retries := 20
	for retries > 0 {
		ms.Lock()
		delayedMessages := ms.DelayedMessages
		ms.Unlock()
		if delayedMessages == 0 {
			break
		}
		time.Sleep(250 * time.Millisecond)
		retries--
	}
	ms.Lock()
	assert.Equal(t, 0, ms.DelayedMessages)
	ms.Unlock()

	select {
	case <-ch:
		break
	case <-time.After(2 * time.Second):
		assert.FailNow(t, "Error cb not called")
	}
}

func TestMessageSenderDeliveryEnsuranceNoCb(t *testing.T) {
	SetupFaulty(t, 2)
	ms, err := NewMessageSenderHTTPWithDelay(config, pool, 1*time.Second, 100)
	assert.NoError(t, err)

	msg := &protocol.Message{
		Sender:   "PartyA",
		Receiver: "PartyB",
		QueryID:  "1234",
		StepID:   "0",
		Body:     []byte("Hello!"),
	}
	ms.SendMessageWithDeliveryEnsurance(context.Background(), msg, nil)

	time.Sleep(1 * time.Second)
	retries := 20
	for retries > 0 {
		ms.Lock()
		delayedMessages := ms.DelayedMessages
		ms.Unlock()
		if delayedMessages == 0 {
			break
		}
		time.Sleep(250 * time.Millisecond)
		retries--
	}
	ms.Lock()
	assert.Equal(t, 0, ms.DelayedMessages)
	ms.Unlock()
}
