// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

// ChannelSwitch is a testing thing for routing messages sent over a channel
type ChannelSwitch struct {
	incoming chan *protocol.Message
	outgoing map[string]chan *protocol.Message
}

// AddMessageReceiver adds a channel to route MPC messages to
func (cs *ChannelSwitch) AddMessageReceiver(node string, c chan *protocol.Message) {
	cs.outgoing[node] = c
}

// CreateMessageReceiver creates an channel-based message-receiver and adds it to the switch
func (cs *ChannelSwitch) CreateMessageReceiver(node string, protocolName string, mpcService *Service) *ChannelMessageReceiver {
	c := make(chan *protocol.Message)
	cs.outgoing[node] = c
	return NewChannelMessageReceiver(c, protocolName, mpcService)
}

// Start starts the channelswitch and waits for incoming messages (and thus blocks)
func (cs *ChannelSwitch) Start() {
	for m := range cs.incoming {
		cs.outgoing[m.Receiver] <- m
	}
}

// NewChannelSwitch returns a channel switch with the provided incoming channel
func NewChannelSwitch(incoming chan *protocol.Message) *ChannelSwitch {
	return &ChannelSwitch{incoming: incoming, outgoing: make(map[string]chan *protocol.Message)}
}
