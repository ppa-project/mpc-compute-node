// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"io/ioutil"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewProtocolResult(t *testing.T) {
	pr, err := NewProtocolResult("123", false, "testreason", []string{"A"})
	assert.Nil(t, err)
	assert.NotNil(t, pr)
	assert.Equal(t, "123", pr.QueryID)
	assert.Equal(t, false, pr.IsSuccess)
	assert.Equal(t, "testreason", pr.AbortMessage)
	assert.Equal(t, []string{"A"}, pr.DestinationNodes)

	pr, err = NewProtocolResult("456", true, 17, []string{"B"})
	assert.Nil(t, err)
	assert.NotNil(t, pr)
	assert.Equal(t, "456", pr.QueryID)
	assert.Equal(t, true, pr.IsSuccess)
	assert.Equal(t, 17, pr.Result)
	assert.Equal(t, []string{"B"}, pr.DestinationNodes)
	assert.Equal(t, 64, len(pr.ResultHash))
}

func TestSaveLoadResult(t *testing.T) {
	s := NewService()
	pr, err := NewProtocolResult("123", false, "testreason", []string{"A"})
	assert.Nil(t, err)
	assert.NotNil(t, pr)

	// Record a result
	s.RecordResult("abc", pr)

	// Make sure it exists in both stores
	assert.NotNil(t, s.completedProtocols["abc"]["123"])
	assert.Equal(t, *pr, *s.completedProtocols["abc"]["123"])

	f, err := ioutil.ReadFile(path.Join(ResultDir, "abc", "123"))
	assert.Nil(t, err)
	assert.NotEqual(t, 0, len(f))

	// Make sure we can get it from both loaders
	pr2, ok := s.getResultMemory("abc", "123")
	assert.Equal(t, true, ok)
	assert.Equal(t, *pr, *pr2)
	pr3, err := s.getResultDisk("abc", "123")
	assert.Nil(t, err)
	assert.Equal(t, *pr, *pr3)

	// Make sure we can get it from the public loader
	pr4, err := s.GetResult("abc", "123")
	assert.Nil(t, err)
	assert.Equal(t, *pr, *pr4)

	// Make sure we can even get it from a new Service
	s2 := NewService()
	pr5, err := s2.GetResult("abc", "123")
	assert.Nil(t, err)
	assert.Equal(t, *pr, *pr5)
}
