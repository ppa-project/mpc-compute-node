// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/hex"
	"fmt"
	"testing"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/smartcontract"
	"ci.tno.nl/gitlab/ppa-project/pkg/scbindings"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

// Test if query conversions from SC to DB work
func TestConvertQuery(t *testing.T) {
	queryID := "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"

	scQuery := smartcontract.PPAQueryQuery{
		Columns: []scbindings.PPAQueryColumn{{
			Func:      "SUM",
			Owner:     "P",
			Attribute: "Q",
		}},
		Constraints: []scbindings.PPAQueryConstraint{{
			Owner:          "A",
			Attribute:      "a",
			Operator:       "==",
			ReferenceValue: "1",
		}, {
			Owner:          "B",
			Attribute:      "b",
			Operator:       ">",
			ReferenceValue: "2",
		}},
	}
	dbQuery := types.TaggedQuery{
		ID: queryID,
		Query: &types.Query{
			Aggregates: []types.Aggregate{{
				Function:  "SUM",
				Owner:     "P",
				Attribute: "Q",
			}},
			Filters: []types.Filter{{
				Owner:          "A",
				Attribute:      "a",
				Operator:       "==",
				ReferenceValue: "1",
			}, {
				Owner:          "B",
				Attribute:      "b",
				Operator:       ">",
				ReferenceValue: "2",
			}},
		},
	}

	var hash [32]byte
	b, _ := hex.DecodeString(queryID)
	copy(hash[:], b)
	assert.Equal(t, dbQuery, scQuery.TypesTaggedQuery(&hash))
}

func TestIncomingQuerySilly(t *testing.T) {
	s1 := NewService()
	s2 := NewService()
	s3 := NewService()

	cbMap := make(map[string]func(protocol string, m *protocol.Message) error)
	cbMap["PartyC"] = s1.RouteMessage
	cbMap["PartyA"] = s2.RouteMessage
	cbMap["PartyB"] = s3.RouteMessage

	ms, _ := transport.NewMessageSenderMock(cbMap)

	init := func(thisNodeName string) ProtocolInstantiator {
		return func() (protocol.StateMachine, error) {
			neighbor := ""
			switch thisNodeName {
			case "PartyC":
				neighbor = "PartyA"
			case "PartyA":
				neighbor = "PartyB"
			case "PartyB":
				neighbor = "PartyC"
			default:
				return nil, fmt.Errorf("invalid node name %s", thisNodeName)
			}
			return silly.NewProtocol(thisNodeName, neighbor, ms), nil
		}
	}

	s1.SetProtocolInstantiator("silly", init("PartyC"))
	s2.SetProtocolInstantiator("silly", init("PartyA"))
	s3.SetProtocolInstantiator("silly", init("PartyB"))

	queryID := "1"
	startPayload := &silly.StartPayload{ID: "1", Starter: "PartyB"}
	assert.NoError(t, s1.StartProtocol("silly", queryID, startPayload, nil))
	assert.NoError(t, s2.StartProtocol("silly", queryID, startPayload, nil))
	assert.NoError(t, s3.StartProtocol("silly", queryID, startPayload, nil))
	time.Sleep(time.Second)

	r, err := s1.GetResult("silly", "1")
	if assert.NoError(t, err, "Silly failed to give result to s1") {
		assert.Equal(t, 3, r.Result, "Silly gave wrong result to s1")
	}
	r, err = s2.GetResult("silly", "1")
	if assert.NoError(t, err, "Silly failed to give result to s2") {
		assert.Equal(t, 3, r.Result, "Silly gave wrong result to s2")
	}
	r, err = s3.GetResult("silly", "1")
	if assert.NoError(t, err, "Silly failed to give result to s3") {
		assert.Equal(t, 3, r.Result, "Silly gave wrong result to s3")
	}
}

func TestIncomingQueryBasicstats(t *testing.T) {
}
