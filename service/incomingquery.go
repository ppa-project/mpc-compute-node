// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"
	"reflect"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/rs/zerolog/log"
)

// IncomingQuery implements a listener method, which will be called when an incoming query event occurs
func (svc *Service) IncomingQuery(protocolName, queryid string, query interface{}, options map[string]interface{}) {
	log := log.With().Str("queryid", queryid).Logger()
	switch protocolName {
	case "silly":
		var stringQuery string

		switch obj := query.(type) {
		case string:
			stringQuery = obj
		case []byte:
			stringQuery = string(obj)
		default:
			log.Error().
				Stringer("queryType", reflect.TypeOf(query)).
				Msg("Invalid query type received")
			return
		}

		err := svc.StartProtocol(protocolName, queryid, &silly.StartPayload{ID: queryid, Starter: stringQuery}, options)
		if err != nil {
			log.Err(err).Msg("Start protocol failed")
		}

	case "basicstats":
		var structuredQuery types.Query

		switch obj := query.(type) {
		case types.Query:
			structuredQuery = obj
		case []byte:
			err := json.Unmarshal(obj, &structuredQuery)
			if err != nil {
				log.Err(err).
					Bool("aborted", true).
					Str("queryBytes", string(obj)).
					Msg("Can not interpret query bytes as a valid query")
				return
			}
		default:
			log.Error().
				Bool("aborted", true).
				Stringer("queryType", reflect.TypeOf(query)).
				Msg("Invalid query type received")
			return
		}

		err := svc.StartProtocol(protocolName, queryid, &types.TaggedQuery{ID: queryid, Query: &structuredQuery}, options)
		if err != nil {
			log.Err(err).Msg("Start protocol failed")
		}

	default:
		log.Error().
			Bool("aborted", true).
			Str("protocol", protocolName).
			Msgf("Failed to start unknown protocol")
	}
}
