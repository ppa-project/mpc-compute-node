// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

type MockStateMachine struct {
	review bool
}

func (sm *MockStateMachine) ProcessMessage(_ *protocol.Message) error {
	return nil
}

func (sm *MockStateMachine) Start(_ protocol.Input, _ protocol.Options, callback protocol.CompletedCallback) error {
	callback(protocol.MadeByUs, protocol.ManualReview, &MockApprovalRequest{})
	return nil
}

func (sm *MockStateMachine) GetInputType() interface{} {
	return 0
}

func (sm *MockStateMachine) SubmitReview(approved bool) error {
	sm.review = approved
	return nil
}

type MockApprovalRequest struct{}

func (ra *MockApprovalRequest) Summary() string {
	return "Mock Review Assignment"
}
func (ra *MockApprovalRequest) QueryID() string {
	return "mock_query_id"
}
func (ra *MockApprovalRequest) Protocol() string {
	return "mock_protocol"
}

func TestReviewStorage(t *testing.T) {
	assert := assert.New(t)

	// Set up a Service and generate a protocol that asks for a review
	sv := NewService()
	assert.NotNil(sv, "Could not create new service")
	state := &MockStateMachine{}
	sv.SetProtocolInstantiator("mock_protocol", func() (protocol.StateMachine, error) { return state, nil })
	sv.StartProtocolWithCallback("mock_protocol", "mock_query_id", 1, map[string]interface{}{},
		sv.GetCompletedCallback("mock_protocol", "mock_query_id", []string{}))

	// See if we can see the review
	assert.Equal(sv.ListApprovalRequests("mock_protocol"), []ApprovalRequestSummary{{
		QueryID: "mock_query_id",
		Summary: "Mock Review Assignment",
	}}, "List of review assignments incorrect")
	assert.Equal(
		sv.GetApprovalRequest("mock_protocol", "mock_query_id"),
		protocol.ApprovalRequest(&MockApprovalRequest{}),
		"Review assignment could not be recovered")

	// Submit a review and see that that worked
	assert.NoError(sv.SubmitReview("mock_protocol", "mock_query_id", true), "Could not submit review")
	assert.Equal(true, state.review, "Review was not saved")
}
