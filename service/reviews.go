// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

// ApprovalRequestSummary summary of an approval request
type ApprovalRequestSummary struct {
	QueryID string `json:"queryID"`
	Summary string `json:"summary"`
}

// GetApprovalRequest returns the requested ApprovalRequest
func (svc *Service) GetApprovalRequest(protocolName, queryid string) protocol.ApprovalRequest {
	svc.armu.Lock()
	defer svc.armu.Unlock()
	return svc.approvalRequests[protocolName][queryid]
}

// ListApprovalRequests returns a list of ApprovalRequestSummaries
func (svc *Service) ListApprovalRequests(protocolName string) []ApprovalRequestSummary {
	svc.armu.Lock()
	defer svc.armu.Unlock()
	list := make([]ApprovalRequestSummary, 0, len(svc.approvalRequests[protocolName]))
	for k := range svc.approvalRequests[protocolName] {
		list = append(list, ApprovalRequestSummary{
			QueryID: svc.approvalRequests[protocolName][k].QueryID(),
			Summary: svc.approvalRequests[protocolName][k].Summary(),
		})
	}
	return list
}

// SubmitReview submits a verdict of a manual review
func (svc *Service) SubmitReview(protocolName, queryid string, approved bool) error {
	svc.mu.Lock()
	protocolStates, ok := svc.activeProtocols[protocolName]
	if !ok {
		svc.mu.Unlock()
		return ErrUnknownProtocol
	}
	stateMachine, ok := protocolStates[queryid]
	svc.mu.Unlock()
	if !ok {
		return ErrUnknownQuery
	}
	return stateMachine.SubmitReview(approved)
}
