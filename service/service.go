// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service implements the node's MPC service, which keeps track of running protocols
// and is responsible for starting new ones.
package service

import (
	"errors"
	"fmt"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"github.com/rs/zerolog/log"
)

// ProtocolInstantiator is function that instantiates a protocol state machine and returns it
type ProtocolInstantiator func() (protocol.StateMachine, error)

// Service handles MPC messages and routes them to MPC state machine instances (if valid)
type Service struct {
	mu sync.Mutex // guards activeProtocols
	// activeProtocols holds for each protocol name (i.e., a string) a map from query identifiers
	// to active MPC protocol state machines
	activeProtocols map[string]map[string]protocol.StateMachine
	// completedProtocols holds the ProtocolResults for completed protocols, indexed by
	// protocol name ('basicstats') and query ID.
	completedProtocols map[string]map[string]*ProtocolResult

	inmu sync.Mutex // guards instantiators
	// instantiators is a map of functions that instantiate a protocol
	instantiators map[string]ProtocolInstantiator

	armu sync.Mutex // guards approvalRequests
	// ApprovalRequests holds the query results that should be manually reviewed, indexed
	// by protocol and query ID
	approvalRequests map[string]map[string]protocol.ApprovalRequest
	// Precomputer is a Paillier precomputer which we inform of newly generated keys
	Precomputer *pailliercache.Precomputer
	// NodeName is the name of this node
	NodeName string
	// OrganizationList is a function that returns the list of organizations, which is needed for  newprotocol dispatch
	OrganizationList func() ([]string, error)
	// MessageSender is a messagesender which is needed for new protocol dispatch
	MessageSender protocol.MessageSender
	// dpaillierPulse is used for the DPaillier setup; sending to it initiates a keygen attempt
	dpaillierPulse chan struct{}
}

var (
	// ErrUnknownQuery is returned when the query the message was for is not known by the MPC service
	ErrUnknownQuery = errors.New("unknown query ID")
	// ErrUnknownProtocol is returned when the request for the protocol type is unknown
	ErrUnknownProtocol = errors.New("unknown protocol name")
	// ErrUnstartableProtocol is returned when a protocol can't be started via http (e.g. basicstats)
	ErrUnstartableProtocol = errors.New("this protocol type can not be started remotely")
)

// ErrInternalProtocolError is wrapped around protocol errors to aid in pattern matching
type ErrInternalProtocolError struct {
	error
}

// RouteMessage handles MPC messages to the right protocol state machine instantiation
// It returns an error if the there is no valid instantiation for the message or if there
// instantiation is unable to deal with the message.
// Can be called cocurrently
func (svc *Service) RouteMessage(protocolName string, m *protocol.Message) error {
	p, err := svc.getActiveStatemachine(protocolName, m.QueryID)
	if err != nil {
		return err
	}

	err = p.ProcessMessage(m)
	if err != nil {
		return ErrInternalProtocolError{err}
	}
	return nil
}

func (svc *Service) getActiveStatemachine(protocolName, queryID string) (protocol.StateMachine, error) {
	svc.mu.Lock()
	defer svc.mu.Unlock()
	// We now can safely fiddle with activeProtocols

	protocolInstantiations, ok := svc.activeProtocols[protocolName]
	if !ok {
		return nil, ErrUnknownProtocol
	}

	p, ok := protocolInstantiations[queryID]
	if !ok {
		return nil, ErrUnknownQuery
	}
	return p, nil
}

// SetProtocolInstantiator sets the function to be called to instantiate a protocol state machine
// for a specific protocol, identified by protocolName
func (svc *Service) SetProtocolInstantiator(protocolName string, instantiator ProtocolInstantiator) {
	svc.inmu.Lock()
	svc.instantiators[protocolName] = instantiator
	svc.inmu.Unlock()

	svc.mu.Lock()
	svc.activeProtocols[protocolName] = make(map[string]protocol.StateMachine)
	svc.mu.Unlock()
}

// IsProtocolInstantiated checks whether a protocol state machine has been instantiated
func (svc *Service) IsProtocolInstantiated(protocolName string) bool {
	svc.inmu.Lock()
	defer svc.inmu.Unlock()
	return svc.instantiators[protocolName] != nil
}

// StartProtocol starts an instantiation of the requested protocol
func (svc *Service) StartProtocol(protocolName string, queryid string, input interface{}, options map[string]interface{}) error {
	destinationNodes, _ := options["destinationNodes"].([]string)
	cb := svc.GetCompletedCallback(protocolName, queryid, destinationNodes)
	return svc.StartProtocolWithCallback(protocolName, queryid, input, options, cb)
}

// StartProtocolWithCallback starts an instantiation of the requested protocol
func (svc *Service) StartProtocolWithCallback(protocolName string, queryid string, input interface{}, options map[string]interface{}, cb protocol.CompletedCallback) error {
	svc.inmu.Lock()
	instantiator, ok := svc.instantiators[protocolName]
	svc.inmu.Unlock()
	if !ok {
		return ErrUnknownProtocol
	}

	existed, protocolInstance, err := svc.makeStatemachineIfNotExists(protocolName, queryid, instantiator)
	if err != nil || existed { // if already existed, do nothing.
		return err
	}

	if options == nil {
		options = make(map[string]interface{})
	}

	err = protocolInstance.Start(input, options, cb)
	if err != nil && cb != nil {
		// Start does not call the completion callback by itself if it fails to start.
		cb(protocol.MadeByUs, protocol.Failure, err.Error())
	}
	return err
}

// If the given queryid statemachine already exists, does nothing, and returns true, nil, nil.
// If it doesn't exist, makes one, and returns false, (the new statemachine), nil.
// On error, returns false, nil, err.
func (svc *Service) makeStatemachineIfNotExists(protocolName, queryid string, instantiator ProtocolInstantiator) (bool, protocol.StateMachine, error) {
	svc.mu.Lock()
	defer svc.mu.Unlock()
	// Now add it
	a, ok := svc.activeProtocols[protocolName]
	if !ok {
		return false, nil, fmt.Errorf("service: Internal state corrupted: protocol '%s' does not exist in activeProtocols map", protocolName)
	}
	_, ok = a[queryid]
	if ok { // State machine for queryID already exists
		log.Debug().Str("queryID", queryid).Msg("service: State machine for queryID already exists")
		return true, nil, nil
	}

	protocolInstance, err := instantiator()
	if err != nil {
		log.Err(err).
			Str("queryID", queryid).
			Str("protocolname", protocolName).
			Msgf("Can not instantiate new protocol state machine")
		return false, nil, err
	}
	a[queryid] = protocolInstance
	return false, protocolInstance, nil
}

// NewService creates a new MPC service and returns it
func NewService() *Service {
	svc := new(Service)
	svc.activeProtocols = make(map[string]map[string]protocol.StateMachine)
	svc.completedProtocols = make(map[string]map[string]*ProtocolResult)
	svc.instantiators = make(map[string]ProtocolInstantiator)
	svc.approvalRequests = make(map[string]map[string]protocol.ApprovalRequest)
	svc.dpaillierPulse = make(chan struct{}, 1)
	return svc
}
