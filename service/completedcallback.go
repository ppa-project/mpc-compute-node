// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"

	"github.com/rs/zerolog/log"
)

// GetCompletedCallback is a convenience function that gives a function suitable to
// be used as a protocol.CompletedCallback for new basicstats protocols.
func (svc *Service) GetCompletedCallback(mpctype, queryid string, destinationNodes []string) protocol.CompletedCallback {
	return func(resultOrigin protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
		switch resultKind {
		case protocol.ManualReview:
			svc.callbackManualReview(mpctype, queryid, payload)

		case protocol.SubProtocol:
			svc.callbackSubprotocolRequest(mpctype, queryid, payload)

		default:
			pr, err := NewProtocolResult(queryid, resultKind == protocol.Success, payload, destinationNodes)
			if err != nil {
				log.Err(err).Str("queryid", queryid).Msg("Can't make a ProtocolResult!")
				return
			}

			svc.RecordResult(mpctype, pr)
			// Log the result, with tags depending on the source
			if resultOrigin == protocol.MadeByUs {
				if resultKind == protocol.Success {
					log.Info().Str("queryid", queryid).Str("resulthash", pr.ResultHash).Msg("Query completed")
				} else {
					log.Warn().Str("queryid", queryid).Bool("aborted", true).Str("reason", pr.AbortMessage).Msg("Query completed")
				}
			} else {
				log.Info().Str("queryid", queryid).Msgf("Received a query complete notification (successful: %d)", resultKind)
			}

			// Remove the state machine from the service's active protocols
			svc.mu.Lock()
			delete(svc.activeProtocols[mpctype], queryid)
			svc.mu.Unlock()
		}
	}
}

func (svc *Service) callbackManualReview(mpctype, queryid string, payload interface{}) {
	svc.armu.Lock()
	defer svc.armu.Unlock()
	_, ok := svc.approvalRequests[mpctype]
	if !ok {
		svc.approvalRequests[mpctype] = make(map[string]protocol.ApprovalRequest)
	}
	svc.approvalRequests[mpctype][queryid], ok = payload.(protocol.ApprovalRequest)
	if !ok {
		log.Error().
			Msgf("Completed callback called for manual review, with payload of type %T that does not implement ApprovalRequest", payload)
	}
}

func (svc *Service) callbackSubprotocolRequest(mpctype, queryid string, payload interface{}) {
	spr, ok := payload.(protocol.SubProtocolRequest)
	if !ok {
		log.Error().
			Msgf("Completed callback called for subprotocol start, with payload of type %T that is not a SubProtocolRequest", payload)
		spr.CompletedCallback(protocol.MadeByUs, protocol.Failure, "Service was unable to start sub-protocol")
		return
	}

	err := svc.dispatchSubprotocolRequest(mpctype, queryid, spr)
	if err != nil {
		spr.CompletedCallback(protocol.MadeByUs, protocol.Failure, "Service was unable to start sub-protocol")
	}
}

func (svc *Service) dispatchSubprotocolRequest(parentMpctype, parentQueryid string, spr protocol.SubProtocolRequest) error {
	// Start the protocol in other nodes: get list of orgs, prepare input, send input to all
	orgs, err := svc.OrganizationList()
	if err != nil || len(orgs) < 3 {
		log.Err(err).Int("num-organizations", len(orgs)).
			Msg("service: couldn't get list of orgs from the coordinator")
		return err
	}
	inputBytes, err := json.Marshal(spr.Input)
	if err != nil {
		log.Err(err).Msg("service: error preparing protcol start data input for other nodes")
		return err
	}
	protocolStartBytes, err := json.Marshal(ProtocolStartData{
		QueryID: spr.SubQueryID,
		Input:   inputBytes,
		Options: spr.Options,
	})
	if err != nil {
		log.Err(err).Msg("service: error preparing protcol start data for other nodes")
		return err
	}
	for _, org := range sliceRemove(orgs, svc.NodeName) {
		err = svc.MessageSender.RequestStartProtocol(org, spr.SubProtocolName, protocolStartBytes)
		if err != nil {
			log.Err(err).Str("remote-node", org).
				Msg("service: started our own subprotocol, but another node couldn't start it")
			return err
		}
	}

	// Start the protocol in own node
	err = svc.StartProtocolWithCallback(spr.SubProtocolName, spr.SubQueryID, spr.Input, spr.Options, spr.CompletedCallback)
	if err != nil {
		log.Err(err).
			Msgf("Could not start protocol %s:%s requested by %s:%s", spr.SubProtocolName, spr.SubQueryID, parentMpctype, parentQueryid)
		// Immediately notify the parent protocol that the sub-protocol has failed
		return err
	}

	return nil
}
