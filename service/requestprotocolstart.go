// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
)

// ProtocolStartData contains the information in an external request to start a new protocol
type ProtocolStartData struct {
	// Query ID appropriate for the protocol
	QueryID string
	// Input data appropriate for the protocol, to be parsed later
	Input json.RawMessage
	// Options for the protocol, primitive types only
	Options map[string]interface{}
}

// StartProtocolFromSerializedInputs starts a protocol using input serialized to bytes, unmarshalling it into
// a ProtocolStartData struct and then interpreting the Input field depending on which protocol is used.
// Only works with protocols that can be started over https by another node, i.e. dpaikeygen and dpaidecrypt.
func (svc *Service) StartProtocolFromSerializedInputs(protocolName string, startBytes []byte) error {
	if !svc.IsProtocolInstantiated(protocolName) {
		return ErrUnknownProtocol
	}

	var startData ProtocolStartData
	err := json.Unmarshal(startBytes, &startData)
	if err != nil {
		return err
	}

	// TODO: This should probably be refactored so it interoperates with the similar code in StartProtocolWithCallback
	svc.inmu.Lock()
	instantiator := svc.instantiators[protocolName]
	svc.inmu.Unlock()
	temporaryState, err := instantiator()
	if err != nil {
		return err
	}

	input := temporaryState.GetInputType()
	err = json.Unmarshal(startData.Input, &input)
	if err != nil {
		return err
	}

	var callback protocol.CompletedCallback
	switch protocolName {
	case "dpaikeygen":
		callback = svc.remoteKeygenCompletedCallback(startData.QueryID)
	case "dpaidecrypt":
		callback = nil
	default:
		return ErrUnstartableProtocol
	}

	return svc.StartProtocolWithCallback(protocolName, startData.QueryID, input, startData.Options, callback)
}
