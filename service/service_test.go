// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"os"
	"testing"
	"time"

	"github.com/spf13/viper"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/basicstats"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/stretchr/testify/assert"
)

const FILENAME = "_testkey"

func TestMain(m *testing.M) {
	test.Setup()
	viper.SetDefault("paillier.keyfile", FILENAME)

	code := m.Run()

	os.Remove(FILENAME) // Just to be sure
	os.RemoveAll(ResultDir)

	os.Exit(code)
}

type TestKeystore struct {
	M map[string]paillier.Encrypter
}

func (tk *TestKeystore) AddPaillierPublicKey(org string, pk *paillier.PublicKey) {
	tk.M[org] = pk
}

func (tk *TestKeystore) Encrypter(org string) paillier.Encrypter {
	return tk.M[org]
}

func TestSingleService(t *testing.T) {
	assert := assert.New(t)
	s := NewService()
	assert.NotNil(s, "Should not be nil")

	c := make(chan *protocol.Message)
	ms := protocol.NewChannelMessageSender(c)
	mr := NewChannelMessageReceiver(c, "silly", s)
	s.SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("A", "A", ms), nil
	})
	t.Log("Starting receiver")
	go mr.Start()
	sp := &silly.StartPayload{ID: "1", Starter: "A"}
	err := s.StartProtocol("silly", "1", sp, nil)
	assert.Nil(err, "Should return nil")

	err = s.StartProtocol("notsilly", "1", sp, nil)
	assert.NotNil(err, "Should return an error")
	assert.Equal(ErrUnknownProtocol, err, "Should return unknown protocol error")

	var error string
	err = s.StartProtocolWithCallback("silly", "2", "invalid payload", nil, func(madeByUs protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
		if s, ok := payload.(string); madeByUs == protocol.MadeByUs && resultKind == protocol.Failure && ok {
			error = s
		}
	})
	assert.NotNil(err, "Should return an error")
	assert.Equal("input is not of type *StartPayload", err.Error(), "Should return incorrect payload error")
	assert.Equal("input is not of type *StartPayload", error, "Should call callback with error")

	// Wait a bit to let the channels do their work
	time.Sleep(1 * time.Second)
	// close(c)
}

func TestMultipleServicesSilly(t *testing.T) {
	// assert := assert.New(t)
	services := map[string]*Service{
		"A": NewService(),
		"B": NewService(),
		"C": NewService(),
	}

	for _, s := range services {
		s.OrganizationList = func() ([]string, error) { return []string{"A", "B", "C"}, nil }
	}

	// The channel all services will send stuff to
	c := make(chan *protocol.Message)

	// Setup the instiantiators
	services["A"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("A", "B", protocol.NewChannelMessageSender(c)), nil
	})
	services["B"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("B", "C", protocol.NewChannelMessageSender(c)), nil
	})
	services["C"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("C", "A", protocol.NewChannelMessageSender(c)), nil
	})

	// Also setup the channel receivers
	channelSwitch := NewChannelSwitch(c)
	receivers := make(map[string]*ChannelMessageReceiver)
	for node, service := range services {
		receivers[node] = channelSwitch.CreateMessageReceiver(node, "silly", service)
	}

	// Start the receivers
	for _, receiver := range receivers {
		go receiver.Start()
	}

	// Start the switch
	go channelSwitch.Start()

	for _, service := range services {
		// Let all nodes know we've started
		service.StartProtocol("silly", "1", "A", nil)
	}
	// And they should be off by now!
	// Let's wait for the messages to clear
	time.Sleep(1 * time.Second)
}

func TestMultipleServicesBasicstats(t *testing.T) {
	assert := assert.New(t)
	services := map[string]*Service{
		"A": NewService(),
		"B": NewService(),
		"C": NewService(),
	}

	// The channel all services will send stuff to
	c := make(chan *protocol.Message)

	for _, s := range services {
		s.OrganizationList = func() ([]string, error) { return []string{"A", "B", "C"}, nil }
		s.MessageSender = protocol.NewChannelMessageSender(c)
	}

	numItems := 10
	protocolName := "basicstats"
	tk := &TestKeystore{M: make(map[string]paillier.Encrypter)}
	// Setup the instiantiators
	services["A"].SetProtocolInstantiator(protocolName, func() (protocol.StateMachine, error) {
		db := database.NewTestDatabase("A", numItems)
		db.FillAttributePattern1("a1", 1)
		return basicstats.NewStateMachine(db, "A", func() ([]string, error) { return []string{"B", "C"}, nil }, protocol.NewChannelMessageSender(c), tk), nil
	})
	services["B"].SetProtocolInstantiator(protocolName, func() (protocol.StateMachine, error) {
		db := database.NewTestDatabase("B", numItems)
		db.FillAttributePattern2("b1", 15)
		return basicstats.NewStateMachine(db, "B", func() ([]string, error) { return []string{"A", "C"}, nil }, protocol.NewChannelMessageSender(c), tk), nil
	})
	services["C"].SetProtocolInstantiator(protocolName, func() (protocol.StateMachine, error) {
		db := database.NewTestDatabase("C", numItems)
		db.FillAttributePattern2("c1", 1)
		return basicstats.NewStateMachine(db, "C", func() ([]string, error) { return []string{"A", "B"}, nil }, protocol.NewChannelMessageSender(c), tk), nil
	})

	// Also setup the channel receivers
	channelSwitch := NewChannelSwitch(c)
	receivers := make(map[string]*ChannelMessageReceiver)
	for node, service := range services {
		receivers[node] = channelSwitch.CreateMessageReceiver(node, protocolName, service)
	}

	// Start the receivers
	for _, receiver := range receivers {
		go receiver.Start()
	}

	// Start the switch
	go channelSwitch.Start()
	query := &types.TaggedQuery{
		ID: "1",
		Query: &types.Query{
			Aggregates: []types.Aggregate{{Function: "SUM", Owner: "B", Attribute: "b1"}},
			Filters: []types.Filter{
				{Owner: "A", Attribute: "a1", Operator: "==", ReferenceValue: "1"},
				{Owner: "C", Attribute: "c1", Operator: ">", ReferenceValue: "0"},
			},
		},
	}
	for _, service := range services {
		// Let all nodes know we've started

		err := service.StartProtocol(protocolName, "1", query, nil)
		assert.Nil(err, "Unexpected error")
	}
	// And they should be off by now!
	// Let's wait for the messages to clear
	time.Sleep(5 * time.Second)
}
