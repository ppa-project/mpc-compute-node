// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"github.com/rs/zerolog/log"
)

// ChannelMessageSender is a simple implementation of the MessageSender interface that uses a channel
type ChannelMessageSender struct {
	messageChannel chan *protocol.Message
}

// Send sends the MPC message
func (cms *ChannelMessageSender) Send(m *protocol.Message) error {
	cms.messageChannel <- m
	return nil
}

// NewChannelMessageSender creates a ChannelMessageSender using the provided channel
func NewChannelMessageSender(messageChannel chan *protocol.Message) *ChannelMessageSender {
	return &ChannelMessageSender{messageChannel: messageChannel}
}

// ChannelMessageReceiver is a simple implementation of the MessageReceiver interface that uses a channel
type ChannelMessageReceiver struct {
	messageChannel chan *protocol.Message
	protocolName   string
	mpcService     *Service // The service that will handle the messages
}

// NewChannelMessageReceiver creates a ChannelMessageReceiver using the provided channel. Any incoming messages will be
// handled by the provided MPC Service
func NewChannelMessageReceiver(messageChannel chan *protocol.Message, protocolName string, mpcService *Service) *ChannelMessageReceiver {
	return &ChannelMessageReceiver{messageChannel: messageChannel, protocolName: protocolName, mpcService: mpcService}
}

// Start will start the message receiver and wait for messages (i.e., it will block)
func (cmr *ChannelMessageReceiver) Start() error {
	for {
		m := <-cmr.messageChannel
		// Send to the MPC service
		go func() {
			for attempts := 3; attempts > 0; attempts-- {
				err := cmr.mpcService.RouteMessage(cmr.protocolName, m)
				if err == nil {
					break
				}
				log.Printf("ChannelMessageReceiver: error routing message %#v - %v", m, err)
				time.Sleep(time.Second / 10)
			}
		}()
	}
}
