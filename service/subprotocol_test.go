// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"
	"errors"
	"reflect"
	"sync"
	"testing"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

// This file tests a protocol -> subprotocol -> resume interaction with service.
// To this end, we have a protocol, which at some point asks for a Silly protocol.
func TestSubProtocol(t *testing.T) {
	assert := assert.New(t)
	services := map[string]*Service{
		"A": NewService(),
		"B": NewService(),
		"C": NewService(),
	}

	// The channel all services will send stuff to
	subSillyC := make(chan *protocol.Message)
	sillyC := make(chan *protocol.Message)

	// Setup the instiantiators for the special subprotocol caller
	services["A"].SetProtocolInstantiator("subsilly", func() (protocol.StateMachine, error) {
		return NewSubProtocolTestState("A", "B", protocol.NewChannelMessageSender(subSillyC)), nil
	})
	services["B"].SetProtocolInstantiator("subsilly", func() (protocol.StateMachine, error) {
		return NewSubProtocolTestState("B", "C", protocol.NewChannelMessageSender(subSillyC)), nil
	})
	services["C"].SetProtocolInstantiator("subsilly", func() (protocol.StateMachine, error) {
		return NewSubProtocolTestState("C", "A", protocol.NewChannelMessageSender(subSillyC)), nil
	})

	// Setup the instiantiators for silly
	services["A"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("A", "B", protocol.NewChannelMessageSender(sillyC)), nil
	})
	services["B"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("B", "C", protocol.NewChannelMessageSender(sillyC)), nil
	})
	services["C"].SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		return silly.NewProtocol("C", "A", protocol.NewChannelMessageSender(sillyC)), nil
	})

	// Also setup the channel receivers
	subSillySwitch := NewChannelSwitch(subSillyC)
	sillySwitch := NewChannelSwitch(sillyC)
	subSillyRecv := make(map[string]*ChannelMessageReceiver)
	sillyRecv := make(map[string]*ChannelMessageReceiver)
	for node, service := range services {
		subSillyRecv[node] = subSillySwitch.CreateMessageReceiver(node, "subsilly", service)
		sillyRecv[node] = sillySwitch.CreateMessageReceiver(node, "silly", service)
	}

	// Start the receivers
	for _, receiver := range subSillyRecv {
		go receiver.Start()
	}
	for _, receiver := range sillyRecv {
		go receiver.Start()
	}

	// Start the switch
	go subSillySwitch.Start()
	go sillySwitch.Start()

	result := make(chan int, 1)

	for _, svc := range services {
		svc := svc // https://rytisbiel.com/2021/03/06/darker-corners-of-go/#rangevarreuse
		// Let all nodes know we've started
		sp := &StartPayload{ID: "1", Starter: "A"}
		svc.StartProtocolWithCallback("subsilly", "1", sp, nil, func(origin protocol.ResultOrigin, kind protocol.ResultKind, payload interface{}) {
			if origin == protocol.MadeByUs && kind == protocol.Success {
				result <- payload.(int)
			} else if kind == protocol.SubProtocol {
				log.Info().Msg("We need to go deeper")
				spr, ok := payload.(protocol.SubProtocolRequest)
				if !ok {
					log.Error().
						Msgf("Completed callback called for subprotocol start, with payload of type %T that is not a SubProtocolRequest", payload)
				}
				// Start the protocol
				err := svc.StartProtocolWithCallback(spr.SubProtocolName, spr.SubQueryID, spr.Input, spr.Options, spr.CompletedCallback)
				if err != nil {
					log.Err(err).
						Msgf("Could not start protocol %s:%s requested by %s:%s", spr.SubProtocolName, spr.SubQueryID, "subsilly", "1")
					// Immediately notify the parent protocol that the sub-protocol has failed
					spr.CompletedCallback(protocol.MadeByUs, protocol.Failure, "Service was unable to start sub-protocol")
				}
			}
		})
	}

	// And they should be off by now!
	assert.Equal(6, <-result)
}

// SubProtocolTestState is a silly protocol variation which calls a subprotocol (Silly)
type SubProtocolTestState struct {
	mux         sync.Mutex
	count       int
	started     bool
	contributed bool
	queryID     string
	ms          protocol.MessageSender
	me          string
	neighbor    string // The neighbor we're going to send the message to
	c           chan payload
	done        sync.WaitGroup
	callback    protocol.CompletedCallback
}

// Start starts the protocol. Input, a string, should be the name of the node
// that starts the protocol.
func (p *SubProtocolTestState) Start(input protocol.Input, options protocol.Options, callback protocol.CompletedCallback) error {
	startPayload, ok := input.(*StartPayload)
	if !ok {
		log.Error().Stringer("inputType", reflect.TypeOf(input)).Msg("subsilly: Input is not of type StartPayload")
		return errors.New("subsilly: input is not of type StartPayload")
	}

	p.queryID = startPayload.ID
	p.callback = callback
	p.done.Add(1)

	p.mux.Lock()
	p.c = make(chan payload)
	p.mux.Unlock()

	// Start a goroutine for this instance of the protocol
	go func() {
		// Wait for payloads to come in on the channel
		for sm := range p.c {
			sm.Update(p)
		}
	}()

	// Place the start message on the channel
	p.c <- startPayload
	return nil
}

// StartPayload is the payload that starts the protocol
type StartPayload struct {
	ID      string
	Starter string
}

// Update this payload contains the start payload and starts to protocol
func (r *StartPayload) Update(p *SubProtocolTestState) {
	if p.me == r.Starter {
		p.started = true
		mBody, err := json.Marshal(roundPayload{Count: p.count + 1})
		if err != nil {
			log.Fatal().Err(err).Msg("Marshal error")
		}
		_ = p.ms.SendMessage(protocol.NewMessage("subsilly", p.me, p.neighbor, p.queryID, "0", mBody))
	}
}

// payload is a message that can be processed and checked
type payload interface {
	Update(p *SubProtocolTestState)
}

// roundPayload is the message sent for each step of this protocol
type roundPayload struct {
	Count int
}

// Update processes this payload and updates the protocol state
func (r *roundPayload) Update(p *SubProtocolTestState) {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.count = r.Count
	if p.started && p.contributed {
		return
	}
	if p.started {
		// We started, let's stop here, we do not want this to
		// go on forever, right?
		log.Debug().Msgf("[%v] We are full circle (first time) and there are %d of us!", p.me, p.count)
		p.contributed = true
		// Call for a subprotocol
		p.callback(protocol.MadeByUs, protocol.SubProtocol, protocol.SubProtocolRequest{
			SubProtocolName: "silly",
			SubQueryID:      p.queryID + ".1",
			Input:           &silly.StartPayload{ID: p.queryID + ".1", Starter: p.me},
			Options:         nil,
			CompletedCallback: func(_ protocol.ResultOrigin, _ protocol.ResultKind, subResult interface{}) {
				p.callback(protocol.MadeByUs, protocol.Success, subResult.(int)+p.count)
			},
		})
		p.done.Done()
	} else {
		newBody, err := json.Marshal(roundPayload{Count: p.count + 1})
		if err != nil {
			log.Err(err).Msg("Marshal error")
		}
		_ = p.ms.SendMessage(protocol.NewMessage("subsilly", p.me, p.neighbor, p.queryID, "0", newBody))
		p.callback(protocol.MadeElsewhere, protocol.SubProtocol, protocol.SubProtocolRequest{
			SubProtocolName:   "silly",
			SubQueryID:        p.queryID + ".1",
			Input:             &silly.StartPayload{ID: p.queryID + ".1", Starter: "unknown"},
			Options:           nil,
			CompletedCallback: nil, // Don't care, let the Starter handle it
		})
		p.contributed = true
	}
}

// ProcessMessage processes the provided messages in its state machine
func (p *SubProtocolTestState) ProcessMessage(m *protocol.Message) error {
	var mBody roundPayload
	if err := json.Unmarshal(m.Body, &mBody); err != nil {
		return err
	}

	p.mux.Lock()
	defer p.mux.Unlock()
	p.c <- &mBody
	return nil
}

// GetCount returns the count value
func (p *SubProtocolTestState) GetCount() int {
	p.done.Wait()
	// Technically, we can still get data-races here!
	p.mux.Lock()
	defer p.mux.Unlock()
	r := p.count
	return r
}

// GetInputType returns the input type of the protocol
func (p *SubProtocolTestState) GetInputType() interface{} {
	return "string"
}

// SubmitReview submits a review (not implemented)
func (p *SubProtocolTestState) SubmitReview(approved bool) error {
	return errors.New("Unimplemented")
}

// NewProtocol creates a new protocol. me is the name of this node; neighbor is
// the name of the node that we should send our message to when we're done.
func NewSubProtocolTestState(me string, neighbor string, ms protocol.MessageSender) *SubProtocolTestState {
	return &SubProtocolTestState{count: 0, me: me, neighbor: neighbor, ms: ms}
}
