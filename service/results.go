// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/sha3"
)

// ResultDir path to the results directory
const ResultDir = "queryresults"

// ProtocolResult records the result of a computation, stored by the Service
// for later retrieval via the web interface.
type ProtocolResult struct {
	// The query ID of which this is the result.
	QueryID string `json:"queryID"`
	// IsSuccess is false if the computation was aborted for any reason.
	IsSuccess bool `json:"success"`
	// If IsSuccess is false, the abort message contains the reason why.
	AbortMessage string `json:"abortMessage"`
	// The result is the result of a successful computation.
	Result protocol.Result `json:"result"`
	// A hash of the result, to be recorded on the blockchain (visible to
	// all parties). Encoded as hex
	ResultHash string `json:"resultHash"`
	// The names of the nodes who may see the result.
	DestinationNodes []string `json:"destinationNodes"`
}

// NewProtocolResult constructs a new Protocol result.
// If isSuccess is true, the constructor computes the hash of the result, and
// stores the payload in pr.Result. If not, it stores the payload in pr.AbortMessage.
func NewProtocolResult(qid string, isSuccess bool, payload interface{}, destinationNodes []string) (*ProtocolResult, error) {
	pr := new(ProtocolResult)
	pr.QueryID = qid
	pr.IsSuccess = isSuccess
	pr.DestinationNodes = destinationNodes

	var ok bool
	if isSuccess {
		pr.Result, ok = payload.(protocol.Result)
	} else {
		pr.AbortMessage, ok = payload.(string)
	}
	if !ok {
		log.Error().
			Str("queryid", qid).
			Bool("isSuccess", isSuccess).
			Msgf("Can not parse payload as the correct type (string for failures,"+
				" interface{} for success) for payload %#v of type %T", payload, payload)
		return nil, errors.New("can not parse payload as the correct type")
	}

	// Generate the result hash
	if isSuccess {
		resultBytes, err := json.Marshal(pr.Result)
		if err != nil {
			log.Err(err).
				Str("queryid", qid).
				Msgf("Can not marshal result to JSON")
			return nil, err
		}
		hash := sha3.New256()
		_, _ = hash.Write([]byte(qid))
		_, _ = hash.Write(resultBytes)
		pr.ResultHash = hex.EncodeToString(hash.Sum(nil))
	}
	return pr, nil
}

// RecordResult saves the given protocol result (in memory and on disk).
func (svc *Service) RecordResult(mpctype string, pr *ProtocolResult) {
	svc.recordResultMemory(mpctype, pr)
	svc.recordResultDisk(mpctype, pr)
}

// GetResult loads a protocol result previously saved using RecordResult.
func (svc *Service) GetResult(mpctype, queryid string) (*ProtocolResult, error) {
	if pr, ok := svc.getResultMemory(mpctype, queryid); ok {
		return pr, nil
	}
	return svc.getResultDisk(mpctype, queryid)
}

// Store the result in the service's internal buffer
func (svc *Service) recordResultMemory(mpctype string, pr *ProtocolResult) {
	svc.mu.Lock()
	defer svc.mu.Unlock()
	if svc.completedProtocols[mpctype] == nil {
		svc.completedProtocols[mpctype] = make(map[string]*ProtocolResult)
	}
	svc.completedProtocols[mpctype][pr.QueryID] = pr
}

// Store the result on disk
func (svc *Service) recordResultDisk(mpctype string, pr *ProtocolResult) {
	// Ensure the directory exists
	resultDir := path.Join(ResultDir, mpctype)
	err := os.MkdirAll(resultDir, 0744)
	if err != nil {
		log.Err(err).
			Str("path", resultDir).
			Msg("Can not create result directory; results not stored to disk!")
		return
	}

	resultBytes, err := json.Marshal(pr)
	if err != nil {
		log.Err(err).
			Msg("Can not convert result to json; results not stored to disk!")
		return
	}

	resultFile := path.Join(resultDir, pr.QueryID)
	err = ioutil.WriteFile(resultFile, resultBytes, 0600)
	if err != nil {
		log.Err(err).
			Str("filename", resultFile).
			Msg("Can not write result to file; results not stored to disk!")
	}
}

// Get a result from the service's internal buffer. Bool return value indicates success
func (svc *Service) getResultMemory(mpctype, queryid string) (*ProtocolResult, bool) {
	svc.mu.Lock()
	defer svc.mu.Unlock()
	if svc.completedProtocols[mpctype] == nil {
		return nil, false
	}
	pr, ok := svc.completedProtocols[mpctype][queryid]
	return pr, ok
}

// Load a result from disk
func (svc *Service) getResultDisk(mpctype, queryid string) (*ProtocolResult, error) {
	// Ensure the directory exists
	resultFile := path.Join(ResultDir, mpctype, queryid)

	resultBytes, err := ioutil.ReadFile(filepath.Clean(resultFile))
	if err != nil {
		l := log.Error()
		if os.IsNotExist(err) {
			l = log.Debug()
		}
		l.AnErr("i/o-error", err).
			Str("filename", resultFile).
			Msg("Can't read result file from disk")

		return nil, err
	}

	pr := new(ProtocolResult)
	err = json.Unmarshal(resultBytes, pr)
	if err != nil {
		log.Err(err).
			Str("filename", resultFile).
			Msg("Can't interpret result file JSON")
		return nil, err
	}

	return pr, nil
}
