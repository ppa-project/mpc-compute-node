// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"context"
	"encoding/json"
	"fmt"
	"runtime"
	"sync"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// DefaultKeygenParams contains the default Distributed Paillier keygen parameters
var DefaultKeygenParams = dpaillier.KeyGenerationParameters{
	NumberOfParticipants:             3,
	PaillierBitSize:                  2048,
	SecretSharingDegree:              1,
	SecretSharingStatisticalSecurity: 20,
	BiprimalityCheckTimes:            100,
	NumProcessors:                    runtime.NumCPU(),
}

type keygenStartResult int

const (
	keygenStartError keygenStartResult = iota
	keysUpToDate
	keygenStarted
)

// SetupDPaillier checks the currently present DPaillier key against the coordinator's key
// version, and initiates a key generation protocol if appropriate
func (svc *Service) SetupDPaillier(coor coordinator.Coordinator) {
	// This loop serves two purposes: to restart quickly with exponential backoff if there
	// is an early keygen error, and to check after keygen completion whether it worked
	// (and if not, to start another keygen attempt).

	pause := 5 * time.Second

	for range svc.dpaillierPulse {
		res, err := svc.innerSetupDPaillier(coor, svc.dpaillierPulse)
		switch res {
		case keysUpToDate:
			tag, keyShare, _ := pailliercache.OwnSecretKeyShare()
			if err == nil {
				svc.Precomputer.AddPaillierPublicKey(tag, &keyShare.PublicKey)
				return
			}
		case keygenStarted:
			// The keygen callback will send to `pulse` to wake us up
		case keygenStartError:
			log.Info().
				AnErr("keygen-error", err).
				Msgf("service: can't initialize DPaillier key, retrying in %v", pause)
			if pause < time.Hour {
				pause *= 2
			}
			go func() { time.Sleep(pause); svc.dpaillierPulse <- struct{}{} }()
		}
	}
}

func (svc *Service) innerSetupDPaillier(coor coordinator.Coordinator, pulse chan struct{}) (keygenStartResult, error) {
	ourKeyTag, _, err := pailliercache.OwnSecretKeyShare()
	haveOwnKey := err == nil

	ctx, ctxCancel := context.WithTimeout(context.Background(), time.Minute)
	defer ctxCancel()

	theirKeyTag, _, err := coor.DPaillierRead(ctx)
	if err != nil {
		log.Debug().
			AnErr("coordinator-err", err).
			Msg("service: couldn't read smartcontract DPaillier state")
		return keygenStartError, err
	}

	if haveOwnKey && ourKeyTag != "" && theirKeyTag == ourKeyTag {
		return keysUpToDate, nil
	}

	// We will need to generate a new key.
	// Try to take the smart contract mutex. If successful, start keygen, if not, try again later
	err = coor.DPaillierLock(ctx)
	if err != nil {
		// We unlock the mutex here to account for two cases:
		// - locking succeeded, but the result was not available to us somehow
		// - we have died holding the lock earlier, and the error is "can't lock twice"
		err2 := coor.DPaillierUnlock(ctx, ourKeyTag)
		log.Debug().
			AnErr("lock-err", err).
			AnErr("unlock-err", err2).
			Msg("service: couldn't acquire the smart contract keygen mutex")
		return keygenStartError, err
	}

	// We have the mutex, and the sole right to start keygen! Huzzah!
	// Make a copy of the default parameters, validate it (generating a random prime for this run only)
	// and make a copy again for sending to the others
	ownKeygenParams := DefaultKeygenParams
	ownKeygenParams.PaillierBitSize = viper.GetInt("dpaillier.keysize")
	err = ownKeygenParams.Validate()
	if err != nil {
		// This should not happen except in case of programmer error
		log.Err(err).
			Msg("service: couldn't validate the DPaillier starting parameters")
		return keygenStartError, err
	}
	theirKeygenParams := ownKeygenParams

	queryid := fmt.Sprintf("%08x", time.Now().Unix())
	options := map[string]interface{}{"params": &ownKeygenParams}
	// Everyone gets ParticipantIndex == 0, and updates it in statemachine
	// because dpaikeygen.State.thisNode already indexes the nodes, and having
	// two conflicting indices is extremely confusing and causes bugs.
	DefaultKeygenParams.ParticipantIndex = 0
	// Start our own protocol
	err = svc.StartProtocolWithCallback("dpaikeygen", queryid, queryid, options, svc.ownKeygenCompletedCallback(queryid, coor, pulse))
	if err != nil {
		log.Debug().
			AnErr("service-err", err).
			Msg("service: couldn't start the keygen protocol")
		return keygenStartError, err
	}
	log.Debug().Msg("service: started own keygen protocol")

	// Request that the other nodes start their protocol
	orgs, err := coor.GetOrganizations()
	if err != nil || len(orgs) < 3 {
		log.Debug().
			AnErr("coordinator-err", err).
			Int("num-organizations", len(orgs)).
			Msg("service: couldn't get list of orgs from the coordinator")
		return keygenStartError, fmt.Errorf("service: can not retrieve list of organizations: %v", err)
	}

	for _, org := range sliceRemove(orgs, svc.NodeName) {
		options := map[string]interface{}{"params": &theirKeygenParams}
		protocolStartBytes, err := json.Marshal(ProtocolStartData{
			QueryID: queryid,
			Input:   []byte("\"" + queryid + "\""),
			Options: options,
		})
		if err != nil {
			log.Debug().
				AnErr("json-err", err).
				Msg("service: error preparing protcol start data for other nodes")
			return keygenStartError, fmt.Errorf("service: can not retrieve list of organizations: %v", err)
		}
		err = svc.MessageSender.RequestStartProtocol(org, "dpaikeygen", protocolStartBytes)
		if err != nil {
			log.Warn().
				AnErr("http-response-err", err).
				Str("remote-node", org).
				Msg("service: started our own keygen protocol, but another node couldn't start it")
			return keygenStartError, err
		}
	}
	log.Debug().Msg("service: started other nodes' keygen protocols")

	return keygenStarted, nil
}

func (svc *Service) ownKeygenCompletedCallback(queryid string, coor coordinator.Coordinator, pulse chan struct{}) protocol.CompletedCallback {
	return func(origin protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
		// MadeElsewhere payloads contain their result for the public key. We could check if it matches ours,
		// but for now this is too complicated, and if the math is correct(tm) it always matches.
		if origin != protocol.MadeByUs {
			return
		}
		// If this is a success, save the new key.
		if resultKind == protocol.Success {
			privateKey := payload.(*dpaillier.PrivateKeyShare)
			err := pailliercache.StoreSecretKeyShare(queryid, privateKey)
			if err != nil {
				log.Err(err).Msg("service-callback: keygen completed successfully, but couldn't store the private key")
			}
			svc.Precomputer.AddPaillierPublicKey(queryid, &privateKey.PublicKey)
		}
		// Release the mutex both in case of success and failure
		ctx, ctxCancel := context.WithTimeout(context.Background(), time.Minute)
		defer ctxCancel()

		err := coor.DPaillierUnlock(ctx, queryid)
		if err != nil {
			log.Err(err).Msg("service-callback: keygen completed successfully, but couldn't unlock the smart contract mutex and post new version")
		}
		// Wake up the loop in SetupDPaillier that's waiting for this keygen to finish, so it can release resources or try again
		pulse <- struct{}{}
	}
}

func (svc *Service) remoteKeygenCompletedCallback(queryid string) protocol.CompletedCallback {
	return func(origin protocol.ResultOrigin, resultKind protocol.ResultKind, payload interface{}) {
		// MadeElsewhere payloads contain other nodes' result for the public key. We could check if it matches ours,
		// but for now this is too complicated, and if the math is correct(tm) it always matches.
		if origin == protocol.MadeByUs && resultKind == protocol.Success {
			privateKey := payload.(*dpaillier.PrivateKeyShare)
			err := pailliercache.StoreSecretKeyShare(queryid, privateKey)
			if err != nil {
				log.Err(err).Msg("service-callback: keygen completed successfully, but couldn't store the private key")
			}
			svc.Precomputer.AddPaillierPublicKey(queryid, &privateKey.PublicKey)
		}
	}
}

// StartDpaillierKeygenForce starts the Dpailler keygen protocol by force
func (svc *Service) StartDpaillierKeygenForce() {
	svc.dpaillierPulse <- struct{}{}
}

// initialPulse will be set to true when we attempt to start keygen, so it doesn't happen twice
var initialPulse = false
var initialPulseMu sync.Mutex // guards initialPulse

// UpdateHealthFunc returns a function that implements the listener interface of the coordinator
// Its purpose is, if the dpaillier keygen has not been started yet, to check whether the two other nodes are present and healthy, and if so, to attempt to start the keygen process.
// Each call returns an update listener with separate memory, so UpdateHealthFunc should only be called once for each network (usually, this means only once).
func (svc *Service) UpdateHealthFunc() func(nodeName string, healthy bool, err error) error {
	healthStatus := make(map[string]bool)
	return func(nodeName string, healthy bool, err error) error {
		initialPulseMu.Lock()
		defer initialPulseMu.Unlock()

		healthStatus[nodeName] = healthy
		if !initialPulse {
			for _, v := range healthStatus {
				if !v {
					return nil
				}
			}
			if len(healthStatus) == 2 {
				initialPulse = true
				svc.dpaillierPulse <- struct{}{}
			}
		}
		return nil
	}
}

func sliceRemove(flock []string, wolf string) []string {
	if flock == nil {
		return nil
	}
	l := len(flock)
	for i := 0; i < l; i++ {
		if flock[i] == wolf {
			flock[i] = flock[l-1]
			l--
		}
	}
	return flock[0:l]
}
