// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package database

import (
	"bufio"
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/sha3"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// CSVDatabase implements Database and provides access to a CSV file (in memory)
type CSVDatabase struct {
	thisNode     string
	attributes   types.DatabaseAttributes
	valuesInt    map[string]map[string]int64
	valuesString map[string]map[string]string
}

// NewCSVDatabaseFromFile returns a CSVDatabase by reading the provided CSV file
func NewCSVDatabaseFromFile(thisNode, filename, metadatafilename string) (*CSVDatabase, error) {
	csvFile, err := os.Open(filepath.Clean(filename))
	if err != nil {
		return nil, err
	}
	reader := csv.NewReader(bufio.NewReader(csvFile))
	allRecords, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}
	if len(allRecords) < 2 {
		return nil, errors.New("CSV file contains no records")
	}
	if allRecords[0][0] != "id" {
		return nil, errors.New("first column name of CSV file should be \"id\"")
	}

	db := &CSVDatabase{
		thisNode:     thisNode,
		valuesInt:    make(map[string]map[string]int64),
		valuesString: make(map[string]map[string]string),
	}

	// Read database attributes
	db.attributes, err = NewDatabaseAttributesFromFile(metadatafilename)
	if err != nil {
		return nil, err
	}

	// Make a lookup table to see which attribute should go in valuesInt
	attributeLookup := make(map[string]int)
	for i := range db.attributes {
		attributeLookup[db.attributes[i].Name] = i
	}

	// Make sure all database attributes are present in the metadata
	// (we omit column 0, which is `id`)
	for col := 1; col != len(allRecords[0]); col++ {
		if _, ok := attributeLookup[allRecords[0][col]]; !ok {
			return nil, fmt.Errorf("database: attribute %s from the database is not present in the metadata file", allRecords[0][col])
		}
	}
	// and vice versa
	if len(db.attributes)+1 != len(allRecords[0]) {
		return nil, fmt.Errorf("database: there are attributes in the metadata file that do not exist in the database")
	}

	// Fill the database
	for row := 1; row != len(allRecords); row++ {
		db.valuesInt[allRecords[row][0]] = make(map[string]int64)
		db.valuesString[allRecords[row][0]] = make(map[string]string)

		for col := 1; col != len(allRecords[0]); col++ {
			if metadata, ok := attributeLookup[allRecords[0][col]]; ok && db.attributes[metadata].Kind == "int" {
				db.valuesInt[allRecords[row][0]][allRecords[0][col]], err = strconv.ParseInt(allRecords[row][col], 10, 64)
				if err != nil {
					return nil, fmt.Errorf("can not convert database value '%s' at row %d, col %d to integer", allRecords[row][col], row, col)
				}
			} else {
				db.valuesString[allRecords[row][0]][allRecords[0][col]] = allRecords[row][col]
			}
		}
	}

	return db, nil
}

// NewDatabaseAttributesFromFile return DatabaseAttributes by reading the provided CSV file
func NewDatabaseAttributesFromFile(filename string) (types.DatabaseAttributes, error) {
	filecontents, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return nil, err
	}

	var attributesMap map[string]interface{}
	err = json.Unmarshal(filecontents, &attributesMap)
	if err != nil {
		return nil, err
	}

	attributes := make([]types.DatabaseAttribute, 0, len(attributesMap))
	for name, kind := range attributesMap {
		switch k := kind.(type) {
		case string:
			if k != "int" && k != "string" {
				return nil, fmt.Errorf("database metadata: type for attribute %s was specified as %s, but must be 'int', 'string' or ['enum', 'values']", name, k)
			}
			attributes = append(attributes, types.DatabaseAttribute{Name: name, Kind: k})
		case []interface{}:
			enumValues := make([]string, len(k))
			for i := range k {
				var ok bool
				enumValues[i], ok = k[i].(string)
				if !ok {
					return nil, fmt.Errorf("database metadata: enum value %d (%v) for attribute %s must be a string, but was instead %T", i, k[i], name, k[i])
				}
			}
			attributes = append(attributes, types.DatabaseAttribute{Name: name, Kind: "enum", EnumValues: enumValues})
		default:
			return nil, fmt.Errorf("database metadata: type for attribute %s was of type %T, but must be 'int', 'string' or ['enum', 'values']", name, k)
		}
	}

	return attributes, nil
}

// GetAttributes returns the attributes present in this database
func (db *CSVDatabase) GetAttributes() types.DatabaseAttributes {
	return db.attributes
}

// Len returns the number of records in the database
func (db *CSVDatabase) Len() int {
	return len(db.valuesInt)
}

// GetIDs returns the IDs present in this database
func (db *CSVDatabase) GetIDs() []string {
	ids := make([]string, 0, len(db.valuesInt))
	for id := range db.valuesInt {
		ids = append(ids, id)
	}
	return ids
}

// ContainsID returns whether the given ID is present in this database
func (db *CSVDatabase) ContainsID(id string) bool {
	_, ok := db.valuesInt[id]
	return ok
}

// GetIntValue returns the int value for the given id and attribute, or an error if
// either of those does not exist
func (db *CSVDatabase) GetIntValue(id string, attribute string) (int64, error) {
	record, ok := db.valuesInt[id]
	if !ok {
		return 0, fmt.Errorf("unknown ID '%s'", id)
	}
	value, ok := record[attribute]
	if !ok {
		return 0, fmt.Errorf("unknown integer attribute '%s'", attribute)
	}
	return value, nil
}

// GetStringValue returns the string value for the given id and attribute, or an error if
// either of those does not exist
func (db *CSVDatabase) GetStringValue(id string, attribute string) (string, error) {
	record, ok := db.valuesString[id]
	if !ok {
		return "", fmt.Errorf("unknown ID '%s'", id)
	}
	value, ok := record[attribute]
	if !ok {
		return "", fmt.Errorf("unknown string attribute '%s'", attribute)
	}
	return value, nil
}

// Matches returns whether the record for the given ID matches the given QueryConditions.
//
// Any QueryConditions corresponding to another node than the current one are skipped.
// For the remaining ones, EvalInt or EvalString is applied. Matches immediately returns
// if a QueryCondition does not match.
//
// If the id does not exist in the database, Matches returns false and an error.
// If no QueryCondition in the list prevents a match, Matches returns true.
func (db *CSVDatabase) Matches(id string, conditions []types.Filter) (bool, error) {
	if !db.ContainsID(id) {
		return false, fmt.Errorf("unknown ID '%s'", id)
	}

	for _, c := range conditions {
		// Skip attributes we don't have
		if c.Owner != db.thisNode {
			continue
		}

		// Evaluate the condition, returning on false or errors
		if v, err := db.GetIntValue(id, c.Attribute); err == nil {
			r, err := c.EvalInt(v)
			if err != nil || !r {
				return false, err
			}
		} else if v, err := db.GetStringValue(id, c.Attribute); err == nil {
			r, err := c.EvalString(v)
			if err != nil || !r {
				return false, err
			}
		} else {
			return false, fmt.Errorf("unknown attribute '%s' in query condition", c.Attribute)
		}
	}
	return true, nil
}

// Hash returns a sha3 hash of the current database
func (db *CSVDatabase) Hash() string {
	// JSON marshal happens to sort its keys, and is therefore deterministic.
	// Also, it encodes the keys as well, which saves us from adding the ids
	// and attributes members manually.
	hash := sha3.New256()

	bytes, err := json.Marshal(db.valuesInt)
	if err != nil {
		log.Err(err).Msg("Can't hash database; error marshaling JSON")
		return ""
	}
	_, _ = hash.Write(bytes)
	bytes, err = json.Marshal(db.valuesString)
	if err != nil {
		log.Err(err).Msg("Can't hash database; error marshaling JSON")
		return ""
	}
	_, _ = hash.Write(bytes)

	return hex.EncodeToString(hash.Sum(nil))
}

func contains(haystack []string, needle string) bool {
	for i := range haystack {
		if haystack[i] == needle {
			return true
		}
	}
	return false
}
