// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package database

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/test"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

func TestMain(m *testing.M) {
	test.Setup()

	code := m.Run()
	os.Exit(code)
}

func TestTestDatabase(t *testing.T) {
	assert := assert.New(t)
	tdb := NewTestDatabase("A", 10)
	assert.NotNil(tdb, "Should not be nil")
	t.Logf("IDs: %v\n", tdb.GetIDs())
	t.Logf("Attributes: %v\n", tdb.GetAttributes())
	tdb.FillAttributePattern1("a1", 1)
	tdb.FillAttributePattern2("a2", 1)
	tdb.FillAttributePattern3("a3", 1)
	tdb.FillAttributePattern2s("a4", "a", "z")
	ids := tdb.GetIDs()
	for _, id := range ids {
		v, _ := tdb.GetIntValue(id, "a1")
		t.Logf("%d\n", v)
	}

	c := []types.Filter{*types.NewFilterFromString("A:a1 == 1")}
	t.Logf("qc: %v", c)
	numMatches := 0
	for _, id := range ids {
		match, err := tdb.Matches(id, c)
		assert.Nil(err, "No error expected")
		if match {
			numMatches++
		}
	}
	assert.Equal(len(ids)/2, numMatches, "Number of matches (int) not as expected")

	c = []types.Filter{*types.NewFilterFromString("A:a4 > g")}
	t.Logf("qc: %v", c)
	numMatches = 0
	for _, id := range ids {
		match, err := tdb.Matches(id, c)
		assert.Nil(err, "No error expected")
		if match {
			numMatches++
		}
	}
	assert.Equal(len(ids)/2, numMatches, "Number of matches (str) not as expected")

	assert.Equal(
		"6fcd6e514bf616bcd98bdd50aff586671e42d4a1ace8fc8c1140e27082833c75",
		tdb.Hash(),
		"Database hash value not as expected",
	)
}
