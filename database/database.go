// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package database is the interface to the MPC node's database.
//
// It comes with two kinds of Database: a CSV-based database, which reads a CSV file into
// memory and serves its contents, and a testing database, which can be filled with data
// using simple patterns and is mainly intended for use in tests. It also provides the
// Query type that protocol state machines can use to retrieve data from a Database.
package database

import (
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// Database is an interface for the simple database we need
type Database interface {
	// Len returns the number of records (persons) in the database
	Len() int

	// GetIDs returns a slice of all IDs present in the database.
	GetIDs() []string

	// Matches checks whether a specific row, identified by id, matches all applicable
	// QueryConditions in conditions.
	Matches(id string, conditions []types.Filter) (bool, error)

	// GetAttributes returns all attribute names in the database.
	GetAttributes() types.DatabaseAttributes

	// GetIntValue returns the integer value at row id and column attribute.
	GetIntValue(id string, attribute string) (int64, error)

	// GetStringValue returns the string value at row id and column attribute.
	GetStringValue(id string, attribute string) (string, error)

	// Hash returns a sha3-hash of the entire database.
	Hash() string
}

// NodeName identifies a data owner
type NodeName string
