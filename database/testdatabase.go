// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package database

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/sha3"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

// TestDatabase implements Database and provides a test database for testing MPC protocols
type TestDatabase struct {
	node       string
	ids        []string
	attributes types.DatabaseAttributes
	intData    map[string]map[string]int64
	strData    map[string]map[string]string
}

// Len returns the number of records in the database
func (db *TestDatabase) Len() int {
	return len(db.ids)
}

// GetIDs returns the identifiers of the records
func (db *TestDatabase) GetIDs() []string {
	return db.ids
}

// GetAttributes returns all attributes in the database
func (db *TestDatabase) GetAttributes() types.DatabaseAttributes {
	return db.attributes
}

// Matches checks whether a record matches the provided conditions
func (db *TestDatabase) Matches(id string, conditions []types.Filter) (bool, error) {
	if !contains(db.ids, id) {
		return false, fmt.Errorf("unknown ID '%s'", id)
	}

	for _, c := range conditions {
		// Skip attributes we don't have
		if c.Owner != db.node {
			continue
		}

		// Evaluate the condition, returning on false or errors
		if v, ok := db.intData[id][c.Attribute]; ok {
			r, err := c.EvalInt(v)
			if err != nil || !r {
				return false, err
			}
		} else if v, ok := db.strData[id][c.Attribute]; ok {
			r, err := c.EvalString(v)
			if err != nil || !r {
				return false, err
			}
		} else {
			return false, fmt.Errorf("unknown attribute '%s' in query condition", c.Attribute)
		}
	}
	return true, nil
}

// GetIntValue returns the int64 value of an attribute from a record
func (db *TestDatabase) GetIntValue(id string, attribute string) (int64, error) {
	v, ok := db.intData[id][attribute]
	if !ok {
		return 0, errors.New("unknown attribute")
	}
	return v, nil
}

// GetStringValue return the string value of an attribute from a record
func (db *TestDatabase) GetStringValue(id string, attribute string) (string, error) {
	v, ok := db.strData[id][attribute]
	if !ok {
		return "", errors.New("unknown attribute")
	}
	return v, nil
}

func int2ID(i int) string {
	return "id" + strconv.Itoa(i)
}

// FillAttributePattern1 adds the given attribute to the testing database.
// The value of the attribute will be value for the first 50% of rows, and
// zero for the others.
func (db *TestDatabase) FillAttributePattern1(attribute string, value int64) {
	db.attributes = append(db.attributes, types.DatabaseAttribute{
		Name: attribute,
		Kind: "int",
	})
	for i := 0; i < len(db.ids); i++ {
		if i < len(db.ids)/2 {
			db.intData[int2ID(i)][attribute] = value
		} else {
			db.intData[int2ID(i)][attribute] = int64(0)
		}
	}
}

// FillAttributePattern2 adds the given attribute to the testing database.
// The value of the attribute will be value for the even rows, and zero for the
// odd rows.
func (db *TestDatabase) FillAttributePattern2(attribute string, value int64) {
	db.attributes = append(db.attributes, types.DatabaseAttribute{
		Name: attribute,
		Kind: "int",
	})
	for i := 0; i < len(db.ids); i++ {
		if i%2 == 0 {
			db.intData[int2ID(i)][attribute] = value
		} else {
			db.intData[int2ID(i)][attribute] = int64(0)
		}
	}
}

// FillAttributePattern3 adds the given attribute to the testing database.
// The value of the attribute will be value for the first, fourth, seventh etc.
// row, and zero for the others.
func (db *TestDatabase) FillAttributePattern3(attribute string, value int64) {
	db.attributes = append(db.attributes, types.DatabaseAttribute{
		Name: attribute,
		Kind: "int",
	})
	for i := 0; i < len(db.ids); i++ {
		if i%3 == 0 {
			db.intData[int2ID(i)][attribute] = value
		} else {
			db.intData[int2ID(i)][attribute] = int64(0)
		}
	}
}

// FillAttributePattern2s adds the given attribute to the testing database.
// The value of the attribute will be value1 for the even rows, and value2 for the
// odd rows.
func (db *TestDatabase) FillAttributePattern2s(attribute string, value1 string, value2 string) {
	db.attributes = append(db.attributes, types.DatabaseAttribute{
		Name: attribute,
		Kind: "string",
	})
	for i := 0; i < len(db.ids); i++ {
		if i%2 == 0 {
			db.strData[int2ID(i)][attribute] = value1
		} else {
			db.strData[int2ID(i)][attribute] = value2
		}
	}
}

// FillAttributePattern2e adds the given attribute to the testing database as an enum.
// The value of the attribute will be value1 for the even rows, and value2 for the
// odd rows.
func (db *TestDatabase) FillAttributePattern2e(attribute string, value1 string, value2 string) {
	db.attributes = append(db.attributes, types.DatabaseAttribute{
		Name:       attribute,
		Kind:       "enum",
		EnumValues: []string{value1, value2},
	})
	for i := 0; i < len(db.ids); i++ {
		if i%2 == 0 {
			db.strData[int2ID(i)][attribute] = value1
		} else {
			db.strData[int2ID(i)][attribute] = value2
		}
	}
}

// Hash returns a hex-encoded sha-3 hash string
func (db *TestDatabase) Hash() string {
	// JSON marshal happens to sort its keys, and is therefore deterministic.
	// Also, it encodes the keys as well, which saves us from adding the ids
	// and attributes members manually.
	hash := sha3.New256()

	bytes, err := json.Marshal(db.intData)
	if err != nil {
		log.Err(err).Msg("Can't hash database; error marshaling JSON")
		return ""
	}
	_, _ = hash.Write(bytes)
	bytes, err = json.Marshal(db.strData)
	if err != nil {
		log.Err(err).Msg("Can't hash database; error marshaling JSON")
		return ""
	}
	_, _ = hash.Write(bytes)

	return hex.EncodeToString(hash.Sum(nil))
}

// NewTestDatabase returns a new testing database belonging to the node with name
// node, containing numItems rows.
func NewTestDatabase(node string, numItems int) *TestDatabase {
	db := new(TestDatabase)
	db.node = node
	db.ids = make([]string, numItems)
	for i := 0; i < len(db.ids); i++ {
		db.ids[i] = int2ID(i)
	}
	db.attributes = make([]types.DatabaseAttribute, 0)
	db.intData = make(map[string]map[string]int64)
	for i := 0; i < len(db.ids); i++ {
		db.intData[int2ID(i)] = make(map[string]int64)
	}
	db.strData = make(map[string]map[string]string)
	for i := 0; i < len(db.ids); i++ {
		db.strData[int2ID(i)] = make(map[string]string)
	}

	return db
}
