// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package database

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

func TestCSVDatabase(t *testing.T) {
	assert := assert.New(t)

	db, err := NewCSVDatabaseFromFile("A", "test.csv", "meta.json")
	assert.NotNil(db, "Database nil")
	assert.Nil(err, "Reading csv database returned error: %v", err)

	// GetAttributes
	assert.ElementsMatch(
		types.DatabaseAttributes{
			{Name: "age", Kind: "int"},
			{Name: "sex", Kind: "enum", EnumValues: []string{"M", "F", "X"}},
			{Name: "weight", Kind: "int"},
			{Name: "birthPlace", Kind: "string"},
		},
		db.GetAttributes(),
		"Attributes not correct",
	)

	// GetIDs
	assert.Equal(10, len(db.GetIDs()), "Not all IDs returned")

	// ContainsID
	assert.True(db.ContainsID("a7"))
	assert.False(db.ContainsID("a17"))

	// GetIntValue
	v, err := db.GetIntValue("a2", "age")
	assert.Nil(err)
	assert.Equal(int64(30), v)

	_, err = db.GetIntValue("a2", "sheep")
	assert.NotNil(err)
	_, err = db.GetIntValue("a12", "age")
	assert.NotNil(err)

	// GetStringValue
	s, err := db.GetStringValue("a2", "sex")
	assert.Nil(err)
	assert.Equal("F", s)

	_, err = db.GetStringValue("a2", "shoes")
	assert.NotNil(err)
	_, err = db.GetStringValue("a12", "sex")
	assert.NotNil(err)

	// Hash
	assert.Equal("3c57d0939968890beb95c61a391cf3f21786ff128bbf6651140f2c232a4687c1", db.Hash())
}

func TestCSVDatabaseMatches(t *testing.T) {
	db, err := NewCSVDatabaseFromFile("A", "test.csv", "meta.json")
	assert.Nil(t, err)

	t.Run("String == and different attribute owner", testMatchInternal1(db, []types.Filter{
		*types.NewFilterFromString("A:sex == F"),
		*types.NewFilterFromString("B:cars > 7"),
	}))
	t.Run("String !=", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:sex != M")}))
	t.Run("String > ", testMatchInternal2(db, []types.Filter{*types.NewFilterFromString("A:sex > G")}))
	t.Run("String >=", testMatchInternal2(db, []types.Filter{*types.NewFilterFromString("A:sex >= M")}))
	t.Run("String < ", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:sex < J")}))
	t.Run("String <=", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:sex <= F")}))

	t.Run("Int ==", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:weight == 70")}))
	t.Run("Int !=", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:weight != 80")}))
	t.Run("Int > ", testMatchInternal2(db, []types.Filter{*types.NewFilterFromString("A:weight > 75")}))
	t.Run("Int >=", testMatchInternal2(db, []types.Filter{*types.NewFilterFromString("A:weight >= 80")}))
	t.Run("Int < ", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:weight < 75")}))
	t.Run("Int <=", testMatchInternal1(db, []types.Filter{*types.NewFilterFromString("A:weight <= 70")}))
}

// A condition that is true for a2 but false for a1
func testMatchInternal1(db *CSVDatabase, qc []types.Filter) func(t *testing.T) {
	return func(t *testing.T) {
		rt, err := db.Matches("a2", qc)
		assert.Nil(t, err)
		assert.True(t, rt)
		rf, err := db.Matches("a1", qc)
		assert.Nil(t, err)
		assert.False(t, rf)
	}
}

// A condition that is true for a1 but false for a2
func testMatchInternal2(db *CSVDatabase, qc []types.Filter) func(t *testing.T) {
	return func(t *testing.T) {
		rt, err := db.Matches("a1", qc)
		assert.Nil(t, err)
		assert.True(t, rt)
		rf, err := db.Matches("a2", qc)
		assert.Nil(t, err)
		assert.False(t, rf)
	}
}

func TestCSVDatabaseIncorrectMetadata(t *testing.T) {
	assert := assert.New(t)

	// Metadata contains columns not in the database
	assert.Nil(ioutil.WriteFile("meta1.json", []byte(`{"age":"int","sex":["M","F","X"],"weight":"int","birthPlace":"string","fingers":"int"}`), 0640))

	db, err := NewCSVDatabaseFromFile("A", "test.csv", "meta1.json")
	assert.Nil(db, "Database not nil")
	assert.Equal("database: there are attributes in the metadata file that do not exist in the database", err.Error())

	os.Remove("meta1.json")

	// Database contains columns not in the metadata
	assert.Nil(ioutil.WriteFile("meta1.json", []byte(`{"age":"int","sex":["M","F","X"]}`), 0640))

	db, err = NewCSVDatabaseFromFile("A", "test.csv", "meta1.json")
	assert.Nil(db, "Database not nil")
	assert.Equal("database: attribute weight from the database is not present in the metadata file", err.Error())

	os.Remove("meta1.json")

	// Database contains nonsense type
	assert.Nil(ioutil.WriteFile("meta1.json", []byte(`{"age":"int","sex":["M","F","X"],"weight":"stone"}`), 0640))

	db, err = NewCSVDatabaseFromFile("A", "test.csv", "meta1.json")
	assert.Nil(db, "Database not nil")
	assert.Equal("database metadata: type for attribute weight was specified as stone, but must be 'int', 'string' or ['enum', 'values']", err.Error())

	os.Remove("meta1.json")

	// Database contains non-string type
	assert.Nil(ioutil.WriteFile("meta1.json", []byte(`{"age":"int","sex":["M","F","X"],"weight":7}`), 0640))

	db, err = NewCSVDatabaseFromFile("A", "test.csv", "meta1.json")
	assert.Nil(db, "Database not nil")
	assert.Equal("database metadata: type for attribute weight was of type float64, but must be 'int', 'string' or ['enum', 'values']", err.Error())

	os.Remove("meta1.json")

	// Database contains nonsense enum value
	assert.Nil(ioutil.WriteFile("meta1.json", []byte(`{"age":"int","sex":["M",6,"X"],"weight":"int"}`), 0640))

	db, err = NewCSVDatabaseFromFile("A", "test.csv", "meta1.json")
	assert.Nil(db, "Database not nil")
	assert.Equal("database metadata: enum value 1 (6) for attribute sex must be a string, but was instead float64", err.Error())

	os.Remove("meta1.json")
}
