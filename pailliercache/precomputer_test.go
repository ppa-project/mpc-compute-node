// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package pailliercache

import (
	"encoding/json"
	"math/big"
	"testing"

	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"

	"github.com/stretchr/testify/assert"
)

func TestNewPrecomputer(t *testing.T) {
	pc := NewPrecomputer()
	assert.NotNil(t, pc.buffers)
}
func TestAddPaillierPublicKey(t *testing.T) {
	pk, _ := paillier.GenerateKey(1024)

	pc := NewPrecomputer()
	pc.AddPaillierPublicKey("TestOrg", &pk.PublicKey)

	assert.NotNil(t, pc.buffers["TestOrg"], "Did not add PK at all")
	_, ok := pc.buffers["TestOrg"].(*paillier.PrecomputeBuffer)
	assert.True(t, ok, "Did not create a precompute buffer for PK")
}

func TestAddPaillierPublicKeyUnbuffered(t *testing.T) {
	pk, _ := paillier.GenerateKey(1024)

	pc := NewPrecomputer()
	pc.AddPaillierPublicKeyUnbuffered("TestOrg", &pk.PublicKey)

	assert.NotNil(t, pc.buffers["TestOrg"], "Did not add PK at all")
	_, ok := pc.buffers["TestOrg"].(*paillier.PublicKey)
	assert.True(t, ok, "Did not add plain PK")
}

// Returns the encrypter (buffer or public key) associated to the organization
func TestEncrypter(t *testing.T) {
	pk, _ := paillier.GenerateKey(1024)

	pc := NewPrecomputer()
	pc.AddPaillierPublicKey("TestOrg", &pk.PublicKey)

	en := pc.Encrypter("TestOrg")
	assert.NotNil(t, en, "Encrypter did not return an encrypter")

	assert.Equal(t, 0, pk.Decrypt(en.Encrypt(big.NewInt(7))).Cmp(big.NewInt(7)),
		"Precomputer from the buffer does not properly encrypt")
}

// For convenience, Precomputer implements smartcontract.NodeConfigurationUpdate,
// extracting the Paillier key and starting a precomputation buffer for it.
func TestNodeConfigurationUpdate(t *testing.T) {
	pk, _ := paillier.GenerateKey(1024)

	pkbytes, err := json.Marshal(pk.PublicKey)
	assert.Nil(t, err, "Couldn't marshal Paillier key?!")

	pc := NewPrecomputer()
	pc.NodeConfigurationUpdate("TestOrg", "a", "b", "c", string(pkbytes))

	en := pc.Encrypter("TestOrg")
	assert.NotNil(t, en, "Node configuration update did not add the given key")

	assert.Equal(t, 0, pk.Decrypt(en.Encrypt(big.NewInt(7))).Cmp(big.NewInt(7)),
		"Key added via node config does not properly encrypt")
}
