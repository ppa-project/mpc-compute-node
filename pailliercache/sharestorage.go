// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package pailliercache

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"path/filepath"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/pkg/dpaillier"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type keyshareStorage struct {
	// Identifier is an identifier given when the key was stored
	Identifier string
	// Keyshare is the key share
	Keyshare *dpaillier.PrivateKeyShare
}

// The mutex prevents concurrent reads and writes to the file
// and is only really needed in tests, where every node uses the same file
var secretKeyStorageMutex sync.Mutex

// OwnSecretKeyShare loads a Distributed Paillier private key share from disk.
// A string identifier is also returned, which is equal to the one given when the
// key was saved. If no key exists, an error is returned.
func OwnSecretKeyShare() (string, *dpaillier.PrivateKeyShare, error) {
	keyfile := viper.GetString("dpaillier.keyfile")
	if len(keyfile) == 0 {
		log.Debug().Msg("Can't load stored Distributed Paillier key: no paillier key file given in config")
		return "", nil, errors.New("no dpaillier key filename given (dpaillier.keyfile)")
	}

	secretKeyStorageMutex.Lock()
	keybytes, err := ioutil.ReadFile(filepath.Clean(keyfile))
	secretKeyStorageMutex.Unlock()

	if err != nil {
		log.Debug().Err(err).Str("filename", keyfile).Msg("Can't load stored Distributed Paillier key: Paillier key file can't be read")
		return "", nil, err
	}
	sk := new(keyshareStorage)
	err = json.Unmarshal(keybytes, sk)
	if err != nil {
		log.Debug().Err(err).Str("filename", keyfile).Msg("Can't load stored Distributed Paillier key: Paillier key file can't be parsed")
		return "", nil, err
	}
	return sk.Identifier, sk.Keyshare, nil
}

// StoreSecretKeyShare saves a Distributed Paillier private key share to disk.
// The identifier is also saved and is returned when the key is later loaded.
func StoreSecretKeyShare(identifier string, keyshare *dpaillier.PrivateKeyShare) error {
	keyfile := viper.GetString("dpaillier.keyfile")
	if len(keyfile) == 0 {
		log.Debug().Msg("Can't save Distributed Paillier key to disk: no paillier key file given in config")
		return errors.New("no dpaillier key filename given (dpaillier.keyfile)")
	}
	keybytes, err := json.Marshal(&keyshareStorage{Identifier: identifier, Keyshare: keyshare})
	if err != nil {
		log.Debug().Err(err).Msg("Can't save Distributed Paillier key to disk: error serializing key to json")
		return err
	}

	secretKeyStorageMutex.Lock()
	err = ioutil.WriteFile(keyfile, keybytes, 0600)
	secretKeyStorageMutex.Unlock()

	if err != nil {
		log.Debug().Err(err).Str("filename", keyfile).Msg("Can't save Distributed Paillier key to disk: error opening file")
		return err
	}
	return nil
}

// SerializedPublicKeyFromCache returns a json-serialized version of the Paillier
// public key.
func SerializedPublicKeyFromCache() ([]byte, error) {
	_, sk, err := OwnSecretKeyShare()
	if err != nil {
		return nil, err
	}
	return json.Marshal(sk.PublicKey)
}
