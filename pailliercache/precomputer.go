// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package pailliercache provides infrastructure to manage Paillier public keys.
// It starts precomputation goroutines for every new key, and makes them accessible
// by the name of the associated organization.
package pailliercache

import (
	"encoding/json"
	"runtime"
	"sync"

	"github.com/spf13/viper"

	"github.com/rs/zerolog/log"

	"ci.tno.nl/gitlab/ppa-project/pkg/paillier"
)

// These are the defaults used by AddPaillierPublicKey when calling AddPaillierPublicKeyExt.
const (
	BufferSize        = 32000
	WaitForCompletion = false
)

func init() {
	log.Info().Int("num-cpu", runtime.NumCPU()).Msg("Initializing Paillier precomputation buffers with num-cpu runners")
}

// Precomputer is the object that takes care of precomputations for the Paillier
// cryptosystem. It accepts any number of Paillier public keys, and will
// start precomputation for each of them.
type Precomputer struct {
	sync.Mutex
	buffers map[string]paillier.Encrypter
}

// NewPrecomputer constructs a new Precomputer
func NewPrecomputer() *Precomputer {
	return &Precomputer{buffers: make(map[string]paillier.Encrypter)}
}

// AddPaillierPublicKey starts precomputation for the given public key. The resulting
// encrypter is available using the key org via Precomputer.Encrypter.
// This method always succeeds, using the plain public key in case the buffer can not
// be instantiated.
//
// This method calls AddPaillierPublicKeyExt under the hood, using the constants given
// at the top of this file. The number of processors used for precomputation is set
// to the value of runtime.NumCPU().
func (pc *Precomputer) AddPaillierPublicKey(org string, pk *paillier.PublicKey) {
	pc.AddPaillierPublicKeyExt(org, pk, BufferSize, runtime.NumCPU(), WaitForCompletion)
}

// AddPaillierPublicKeyExt is like AddPaillierPublicKey, but allows its users to specify
// how large a buffer to compute, how many processors to do it with, and whether to
// wait for computation to complete before returning.
func (pc *Precomputer) AddPaillierPublicKeyExt(org string, pk *paillier.PublicKey, bufferSize int, numProc int, waitForCompletion bool) {
	pc.Lock()
	defer pc.Unlock()
	// Check if the same key is already added
	if encrypter, ok := pc.buffers[org]; ok {
		if buffer, ok := encrypter.(*paillier.PrecomputeBuffer); ok {
			if buffer.PublicKey.N.Cmp(pk.N) == 0 {
				log.Info().Str("organization", org).Msg("Re-using existing precomputation buffer")
				return
			}
		}
		if buffer, ok := encrypter.(*paillier.PrecomputeBufferMock); ok {
			if buffer.PublicKey.N.Cmp(pk.N) == 0 {
				log.Warn().Str("organization", org).Msg("Re-using existing mock precomputation buffer, DO NOT USE IN PRODUCTION")
				return
			}
		}
	}

	var err error
	// Make a new precomputation buffer
	if viper.GetBool("dev") {
		pc.buffers[org], err = paillier.NewPrecomputeBufferMock(*pk)
	} else {
		pc.buffers[org], err = paillier.NewPrecomputeBuffer(*pk, bufferSize, numProc, waitForCompletion)
	}
	if err != nil {
		log.Err(err).
			Str("organization", org).
			Msg("Unable to instantiate new precomputation buffer")
		// fall back to using the 'regular' public key
		pc.buffers[org] = pk
	}
}

// AddPaillierPublicKeyUnbuffered adds the given key without creating a precomputation
// buffer.
func (pc *Precomputer) AddPaillierPublicKeyUnbuffered(org string, pk *paillier.PublicKey) {
	pc.Lock()
	defer pc.Unlock()
	pc.buffers[org] = pk
}

// Encrypter returns the encrypter (buffer or public key) associated to the organization
func (pc *Precomputer) Encrypter(organization string) paillier.Encrypter {
	pc.Lock()
	defer pc.Unlock()
	return pc.buffers[organization]
}

// NodeConfigurationUpdate implements coordinator.NodeConfigListener and can be registered as listener.
// For convenience, Precomputer implements smartcontract.NodeConfigurationUpdate,
// extracting the Paillier key and starting a precomputation buffer for it.
func (pc *Precomputer) NodeConfigurationUpdate(org, _, _, _, paillierkey string) {
	pk := new(paillier.PublicKey)
	if err := json.Unmarshal([]byte(paillierkey), pk); err != nil {
		log.Err(err).
			Str("organization", org).
			Str("rawdata", paillierkey).
			Msgf("Can not interpret Paillier key received from smartcontract "+
				"from node %s", org)
		return
	}
	pc.AddPaillierPublicKey(org, pk)
}
