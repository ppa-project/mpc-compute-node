// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package logger defines a logger in terms of a zerolog.log.
// It sets up the proper outputs and configures the timestamp display.
package logger

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// LogConfig is a custom logger wrapping zerolog
type LogConfig struct {
	output []io.Writer
}

// InitLog initializes zerolog and returns a new LogConfig
func InitLog() LogConfig {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.DurationFieldInteger = true

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if viper.GetBool("debug") {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if viper.GetBool("verbose") {
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	}

	f, err := os.OpenFile(viper.GetString("logfile"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		log.Fatal().Err(err).Msgf("Error opening file for log")
	}

	consoleWriter := CallerConsoleWriter{consoleWriter: &zerolog.ConsoleWriter{Out: os.Stdout}}
	config := LogConfig{
		output: []io.Writer{consoleWriter, f},
	}

	config.apply()
	return config
}

// AddOutput adds an additional writer to the LogConfig
func (lc *LogConfig) AddOutput(additionalWriter io.Writer) {
	lc.output = append(lc.output, additionalWriter)
	lc.apply()
}

func (lc *LogConfig) apply() {
	log.Logger = zerolog.New(zerolog.MultiLevelWriter(lc.output...)).With().Timestamp().Caller().Logger()
}

// CallerConsoleWriter is a console writer that extracts any "caller" keys (which usually contain
// a long path name e.g. /home/user/go/github.com/blah/foo/bar.go:123) and reformats the message as
// bar.go:123: <old value of (zerolog.MessageFieldName) field>
type CallerConsoleWriter struct {
	consoleWriter *zerolog.ConsoleWriter
}

// Write takes the caller log field and removes it, integrating it into the message field.
// It then writes the result (or in case of any error, the original) to ccw.consoleWriter.
func (ccw CallerConsoleWriter) Write(p []byte) (int, error) {
	var entry map[string]interface{}
	err := json.Unmarshal(p, &entry)
	if err != nil {
		return ccw.consoleWriter.Write(p)
	}

	// Try to extract all relevant parts
	caller, ok := entry["caller"]
	if !ok {
		return ccw.consoleWriter.Write(p)
	}

	callerStr, ok := caller.(string)
	if !ok {
		return ccw.consoleWriter.Write(p)
	}

	message, ok := entry[zerolog.MessageFieldName]
	if !ok {
		return ccw.consoleWriter.Write(p)
	}

	messageStr, ok := message.(string)
	if !ok {
		return ccw.consoleWriter.Write(p)
	}

	shortCaller := path.Base(callerStr)
	messageStr = fmt.Sprintf("%s: %s", shortCaller, messageStr)

	delete(entry, "caller")
	entry[zerolog.MessageFieldName] = messageStr

	newP, err := json.Marshal(entry)
	if err != nil {
		return ccw.consoleWriter.Write(p)
	}
	n, err := ccw.consoleWriter.Write(newP)

	if err != nil {
		return n, err
	}
	return len(p), nil
}
