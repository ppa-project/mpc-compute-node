// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package logger

import (
	"fmt"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func TestLogger(t *testing.T) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.DurationFieldInteger = true

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if viper.GetBool("debug") {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if viper.GetBool("verbose") {
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	}

	tw := &testingWriter{}
	consoleWriter := CallerConsoleWriter{consoleWriter: &zerolog.ConsoleWriter{Out: tw}}
	config := LogConfig{output: []io.Writer{consoleWriter}}
	config.apply()

	log.Warn().
		Str("field", "value").
		Msg("Hello Log") // This is the line number in the log message below

	time.Sleep(time.Second)

	// We can't check the entire message, since it contains a timestamp (of non-constant length)
	// but after that is a space.
	expected := "\x1b[90m7:35PM\x1b[0m \x1b[31mWRN\x1b[0m logger_test.go:49: Hello Log \x1b[36mfield=\x1b[0mvalue\n"
	assert.Equal(t,
		expected[strings.IndexByte(expected, ' '):],
		tw.written[strings.IndexByte(tw.written, ' '):])
}

type testingWriter struct {
	written string
}

func (t *testingWriter) Write(p []byte) (int, error) {
	t.written = string(p)
	fmt.Println(t.written)
	return len(p), nil
}
