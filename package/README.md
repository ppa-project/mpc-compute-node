# Running a ethereum-connect MPC node

## Intialize geth

```
geth --datadir eth-network/data/client_node --config eth-network/mpc-client_config.toml init eth-network/genesis.json
```

## Start geth

```
geth --datadir eth-network/data/client_node --config eth-network/mpc-client_config.toml --verbosity 2 console
```

## Start one of the nodes
```
./start_node.sh <nodename>
```

Where `<nodename>` is `PartyA`, `PartyC`, or `PartyB`.

So, for example:
```
./start_node.sh PartyA 
```

Notes:
- It might be necessary to change some configuration options in the `mpc_node.toml` file.
- It might also be necessary to regenerate the certificates (CN will no longer be localhost).

## Stop an MPC node

Stopping a node is a bit weird at the moment: press q, press ctrl-c :)
