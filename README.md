Limitations in (end-)use: the content of this repository may solely be used for applications that comply with international export control laws.

# MPC Compute Node

This repository contains the code of the MPC compute node.

Note that the repository uses submodules. To clone it correctly, ensure your ssh agent is running, and run:
```bash
$ git clone git@ci.tno.nl:ppa-project/mpc-compute-node.git
$ cd mpc-compute-node
$ git submodule init
$ git submodule update
```

## Build and run compute node
```sh
$ export GOPRIVATE="ci.tno.nl"
$ go build
$ ./mpc-compute-node start --config /config/config.toml
```

## Run compute node (Docker)
```sh
$ docker pull ci.tno.nl:4567/ppa-project/mpc-compute-node
$ docker run -v $(pwd)/config.toml:/config/config.toml ci.tno.nl:4567/ppa-project/mpc-compute-node start --config /config/config.toml
```

## Configuration
All shown values are default values. Environment variables have a higher precedence than variables defined in a config file.

### TOML config
```toml
version = "v1"
logfile = "mpc_node-<nodename>.log"

nodename = <required>
host = <required>
port = 9443
bindhost = "0.0.0.0"

coordinator = "smartcontract"

[dashboard]
enabled = 0
host = "127.0.0.1"
port = 8000
bindhost = "0.0.0.0"

[database]
filename = /data/<nodename>.csv
intcolumns = []

[tls]
certfile = "tls/<nodename>.crt"
keyfile = "tls/<nodename>.key"

[ethereum]
privatekey = <required when coordinator=smartcontract>
url = "ws://0.0.0.0:0"
smartcontractaddress = <required when coordinator=smartcontract>

[etcd]
host = "localhost"
port = "2379"
cacert = 
clientcert = 
clientkey = 

[paillier]
keysize = 2048
keyfile = "paillierkey.pub"
```

### Enviroment variables
```yaml
MPC_VERSION: v1
MPC_LOGFILE: mpc_node-<nodename>.log

MPC_NODENAME: <required>
MPC_HOST: <required>
MPC_PORT: 443
MPC_BINDHOST: 0.0.0.0

MPC_COORDINATOR: smartcontract

MPC_DASHBOARD_ENABLED: 0
MPC_DAHSBOARD_HOST: 127.0.0.1
MPC_DASHBOARD_PORT: 8000
MPC_DASHBOARD_BINDHOST: 0.0.0.0

MPC_DATABASE_FILENAME: /data/<nodename>.csv
MPC_DATABASE_INTCOLUMNS: []

MPC_TLS_CERTFILE: tls/<nodename>.crt
MPC_TLS_KEYFILE: tls/<nodename>.key

MPC_ETHEREUM_PRIVATEKEY: <required when coordinator=smartcontract>
MPC_ETHEREUM_URL: ws://0.0.0.0:0
MPC_ETHEREUM_SMARTCONTRACTADDRESS: <required when coordinator=smartcontract>

MPC_ETCD_HOST: etcd-master
MPC_ETCD_PORT: 2379
MPC_ETCD_CACERT:
MPC_ETCD_CLIENTCERT:
MPC_ETCD_CLIENTKEY:

MPC_PALLIER_KEYFILE: pallierkey.pub
MPC_PALLIER_KEYSIZE: 2048
MPC_PALLIER_PRECOMPUTE_BUFFER: 2
```
