// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package daemon is responsible for initializing all other components and connecting them to each other.
package daemon

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/dashboard"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/database"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/health"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/logger"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/pailliercache"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/basicstats"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaidecrypt"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/dpaikeygen"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/protocol/silly"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/rest"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/service"
	"ci.tno.nl/gitlab/ppa-project/mpc-compute-node/transport"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/etcd"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/smartcontract"
	"ci.tno.nl/gitlab/ppa-project/pkg/http"
	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const (
	// HTTPGracefulShutdownTimeout timeout to gracefully shutdown a HTTP server
	HTTPGracefulShutdownTimeout = 5 * time.Second
)

// Daemon is where the other components are created, initialized and linked together to create a working MPC node
type Daemon struct {
}

// Start initializes and starts various controller, services, listeners, etc.
func Start() { //nolint:funlen
	// Initialize config
	logConfig := logger.InitLog()
	thisNodeName := viper.GetString("nodename")

	// Migration logic
	RunMigrations()

	httpConfig := GetOwnHTTPConfig()
	pool := make(map[string]http.Config)

	// Check whether Valid TLS Certificates are present.
	// If not, generate them.
	hosts := strings.Split(viper.GetString("host"), ",")
	http.SetupTLSCertificateWithAltNames(httpConfig, hosts[1:])

	// Initialize Database
	db, err := SetupDatabase(thisNodeName)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to load database")
	}

	// Initialize Paillier Precomputer
	precomputer := pailliercache.NewPrecomputer()
	_, paillierPrivateKey, err := pailliercache.OwnSecretKeyShare()
	if err == nil {
		precomputer.AddPaillierPublicKey(thisNodeName, &paillierPrivateKey.PublicKey)
	}

	// Initialize Coordinator
	var coor coordinator.Coordinator
	coord := viper.GetString("coordinator")

	switch coord {
	case "smartcontract":
		// Set up the smart contract binding
		scConfig := GetSCConfig(thisNodeName)
		scConfig.DbHash = db.Hash()
		scConfig.Attributes = db.GetAttributes()
		coor = smartcontract.NewCoordinator(scConfig)
		logConfig.AddOutput(coor.Writer())
	case "etcd":
		etcdConfig := GetEtcdConfig(thisNodeName)
		coor = etcd.NewCoordinator(etcdConfig, false)
	default:
		log.Fatal().Msgf("Unknown coordinator: %s", coord)
	}

	// Initialize MessageSender
	ms, err := transport.NewMessageSenderHTTP(httpConfig, pool)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to instantiate MessageSender")
	}

	// Initialize Service
	service := service.NewService()
	service.Precomputer = precomputer
	service.NodeName = thisNodeName
	service.OrganizationList = coor.GetOrganizations
	service.MessageSender = ms
	service.SetProtocolInstantiator("silly", func() (protocol.StateMachine, error) {
		neighbor := ""
		switch thisNodeName {
		case "PartyC":
			neighbor = "PartyA"
		case "PartyA":
			neighbor = "PartyB"
		case "PartyB":
			neighbor = "PartyC"
		default:
			return nil, fmt.Errorf("invalid node name %s", thisNodeName)
		}
		return silly.NewProtocol(thisNodeName, neighbor, ms), nil
	})
	service.SetProtocolInstantiator("basicstats", func() (protocol.StateMachine, error) {
		return basicstats.NewStateMachine(db, thisNodeName, func() ([]string, error) {
			var orgs []string
			orgs, err = coor.GetOrganizations()
			return sliceRemove(orgs, thisNodeName), err
		}, ms, precomputer), nil
	})
	service.SetProtocolInstantiator("dpaikeygen", func() (protocol.StateMachine, error) {
		orgs, coorErr := coor.GetOrganizations()
		if coorErr != nil {
			return nil, fmt.Errorf("daemon: can not retrieve list of organizations: %v", coorErr)
		}
		thisNodeI, ok := sliceFind(orgs, thisNodeName)
		if !ok {
			return nil, fmt.Errorf("daemon: our org is not one of the active organizations")
		}
		return dpaikeygen.NewStateMachine(thisNodeI, orgs, ms), nil
	})
	service.SetProtocolInstantiator("dpaidecrypt", func() (protocol.StateMachine, error) {
		orgs, coorErr := coor.GetOrganizations()
		if coorErr != nil {
			return nil, fmt.Errorf("daemon: can not retrieve list of organizations: %v", coorErr)
		}
		thisNodeI, ok := sliceFind(orgs, thisNodeName)
		if !ok {
			return nil, fmt.Errorf("daemon: our org is not one of the active organizations")
		}
		_, keyshare, cacheErr := pailliercache.OwnSecretKeyShare()
		if cacheErr != nil {
			return nil, cacheErr
		}
		return dpaidecrypt.NewStateMachine(thisNodeI, orgs, keyshare, ms), nil
	})

	// Initialize HealthMonitor
	var monCloseChan chan bool
	healthMonitor := health.NewMonitor(ms, pool)
	monCloseChan = healthMonitor.Start(1*time.Minute, 1*time.Minute)
	healthMonitor.RegisterListener(service.UpdateHealthFunc())
	if _, ok := coor.(*etcd.Coordinator); ok {
		healthMonitor.RegisterListener(coor.UpdateHealth)
	}

	// Initialize HTTPController / REST API
	hc := server.NewHTTPControllerWithClientAuth(httpConfig, pool)
	rc := rest.NewRESTController(hc, service, healthMonitor)
	hc.RunHTTPServerWithRouter(rc.GetRouter()) // Non-blocking

	// Initialize MPC Dashboard
	if viper.GetBool("dashboard.enabled") {
		// TODO: Add TLS?
		dashboardHost := viper.GetString("dashboard.bindhost")
		dashboardPort := viper.GetInt("dashboard.port")
		dhc := server.NewHTTPController(dashboardHost, dashboardPort)
		drc := dashboard.NewRESTController(&logConfig)
		dhc.RunHTTPServerWithRouter(drc.GetRouter()) // Non-blocking
	}

	// Register listeners
	coor.RegisterQueryListener(service.IncomingQuery)
	coor.RegisterNodeConfigListener(precomputer.NodeConfigurationUpdate)
	coor.RegisterNodeConfigListener(hc.NodeConfigurationUpdate)
	coor.RegisterNodeConfigListener(ms.NodeConfigurationUpdate)
	coor.RegisterNodeConfigListener(healthMonitor.NodeConfigurationUpdate)
	coor.RegisterGuiConfigListener(hc.GuiConfigurationUpdate)

	// Start coordinator
	err = coor.ConnectAndListen() // Non-blocking call
	if err != nil {
		log.Fatal().Err(err).Str("coordinator", coord).Msg("Failed to run coordinator")
	}

	// Update node configuration
	go UpdateNodeConfig(coor, httpConfig)

	// Start DPaillier key generation if necessary
	go service.SetupDPaillier(coor)

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)    // SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, syscall.SIGTERM) // SIGTERM signal sent from Docker/Kubernetes

	// Block until we receive our signal.
	<-c

	log.Info().Msg("Received interrupt, shutting down...")
	// Health monitor
	close(monCloseChan)

	// RESTController
	restConnsClosed := make(chan struct{})
	_, restCancel := hc.Shutdown(HTTPGracefulShutdownTimeout, restConnsClosed)
	defer restCancel()

	// MPC Dashboard
	dashboardConnsClosed := make(chan struct{})
	_, dashboardCancel := hc.Shutdown(HTTPGracefulShutdownTimeout, dashboardConnsClosed)
	defer dashboardCancel()

	// Coordinator
	coor.Close()

	// Blocks until the HTTP server is shutdown.
	<-restConnsClosed
	<-dashboardConnsClosed
}

// GetOwnHTTPConfig returns a HTTPConfig based on the configuration in viper
func GetOwnHTTPConfig() http.Config {
	return http.Config{
		Name:        viper.GetString("nodename"),
		Host:        strings.Split(viper.GetString("host"), ",")[0],
		BindHost:    viper.GetString("bindhost"),
		Port:        viper.GetInt("port"),
		Role:        http.Self,
		TLSCertFile: viper.GetString("tls.certfile"),
		TLSKeyFile:  viper.GetString("tls.keyfile"),
	}
}

// GetSCConfig loads the smart contract-related configuration for the current node from the
// config file.
func GetSCConfig(thisNodeName string) coordinator.Config {
	httpConfig := GetOwnHTTPConfig()

	certfile, err := ioutil.ReadFile(httpConfig.TLSCertFile)
	if err != nil {
		log.Fatal().Err(err).Str("node", thisNodeName).Msg("Failed to read TLS Certificate")
	}
	cert := string(certfile)

	keybytes, err := pailliercache.SerializedPublicKeyFromCache()
	if err != nil {
		log.Fatal().Err(err).Str("node", thisNodeName).Msg("Failed to read Paillier PK")
	}
	paillierKey := string(keybytes)

	return coordinator.Config{
		Organization: thisNodeName,
		Ip:           httpConfig.Host,
		Port:         strconv.Itoa(httpConfig.Port),
		Cert:         cert,
		PaillierKey:  paillierKey,

		EthereumPrivateKey:      getEthereumPrivateKey(),
		EthereumURL:             viper.GetString("ethereum.url"),
		EthereumContractAddress: viper.GetString("ethereum.smartcontractaddress"),
	}
}

// GetEtcdConfig returns a etcd configuration from viper
func GetEtcdConfig(thisNodeName string) etcd.Config {
	return etcd.Config{
		Organization:   thisNodeName,
		EtcdHost:       viper.GetString("etcd.host"),
		EtcdPort:       viper.GetString("etcd.port"),
		EtcdCACert:     viper.GetString("etcd.cacert"),
		EtcdClientCert: viper.GetString("etcd.clientcert"),
		EtcdClientKey:  viper.GetString("etcd.clientkey"),
	}
}

// SetupDatabase attempts to set up a CSV-based database using the file provided in the
// configuration (database.filename and database.intColumns). If unsuccessful, it returns
// a fake database with testing values instead.
func SetupDatabase(thisNodeName string) (database.Database, error) {
	var intCols []string
	err := viper.UnmarshalKey("database.intColumns", &intCols)
	if err == nil {
		var csvDatabase database.Database
		csvDatabase, err = database.NewCSVDatabaseFromFile(thisNodeName, viper.GetString("database.filename"), viper.GetString("database.metadatafilename"))
		if err == nil {
			log.Info().Str("db-hash", csvDatabase.Hash()).Msgf("Successfully loaded database with %d rows and %d attributes", csvDatabase.Len(), len(csvDatabase.GetAttributes()))
			return csvDatabase, nil
		}
	}

	if viper.GetBool("dev") {
		log.Err(err).Msg("Failed to load database, loading fake data")
		tdb := database.NewTestDatabase(thisNodeName, 10)
		tdb.FillAttributePattern1("a1", 1)
		tdb.FillAttributePattern2("a2", 1)
		tdb.FillAttributePattern3("a3", 1)
		tdb.FillAttributePattern2s("a4", "a", "z")
		return tdb, nil
	}
	return nil, err
}

// UpdateNodeConfig triggers an update of configuration on the coordintator
func UpdateNodeConfig(coor coordinator.Coordinator, config http.Config) {
	caCert, _ := ioutil.ReadFile(config.TLSCertFile)
	paillierkey, _ := pailliercache.SerializedPublicKeyFromCache()
	if err := coor.UpdateNodeConfig(config.Host, strconv.Itoa(config.Port), string(caCert), string(paillierkey)); err != nil {
		log.Error().Err(err).Msg("Set failed")
	}
}
