// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemon

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

// Migrations moves old key from deprecated filename to the new one
func TestMigrationPaillierKeys(t *testing.T) {
	viper.Set("dpaillier.keyfile", "testkey")
	oldDpaillierkey := filepath.Clean("dpaillierkey.pub")
	newDpaillierkey := filepath.Clean(viper.GetString("dpaillier.keyfile"))

	_, err := os.Create(oldDpaillierkey)
	assert.NoError(t, err, "Could not create 'old key' for testing")

	RunMigrations()

	_, err = os.Stat(oldDpaillierkey)
	assert.True(t, os.IsNotExist(err), "Migrations did not remove 'old key', but it should have")
	_, err = os.Stat(newDpaillierkey)
	assert.NoError(t, err, "Migrations did not make 'new key', but it should have")

	err = os.Remove(newDpaillierkey)
	assert.NoError(t, err, "Could not remove 'new key' after testing")
}

// Migrations does nothing if new key filename already exists
func TestMigrationPaillierNewKeyExists(t *testing.T) {
	viper.Set("dpaillier.keyfile", "testkey")
	oldDpaillierkey := filepath.Clean("dpaillierkey.pub")
	newDpaillierkey := filepath.Clean(viper.GetString("dpaillier.keyfile"))

	_, err := os.Create(oldDpaillierkey)
	assert.NoError(t, err, "Could not create 'old key' for testing")
	_, err = os.Create(newDpaillierkey)
	assert.NoError(t, err, "Could not create 'new key' for testing")

	RunMigrations()

	_, err = os.Stat(oldDpaillierkey)
	assert.NoError(t, err, "Migrations removed 'old key', but it shouldn't have")
	_, err = os.Stat(newDpaillierkey)
	assert.NoError(t, err, "Migrations removed 'new key', but it shouldn't have")

	err = os.Remove(oldDpaillierkey)
	assert.NoError(t, err, "Could not remove 'old key' after testing")
	err = os.Remove(newDpaillierkey)
	assert.NoError(t, err, "Could not remove 'new key' after testing")
}
