// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemon

import (
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// RunMigrations executes various migration steps
func RunMigrations() {
	oldDpaillierkey := filepath.Clean("dpaillierkey.pub")
	newDpaillierkey := filepath.Clean(viper.GetString("dpaillier.keyfile"))
	if _, err := os.Stat(oldDpaillierkey); err == nil {
		// old dpaillier key exists
		if _, err := os.Stat(newDpaillierkey); os.IsNotExist(err) {
			// new dpaillier key does not exist
			err = os.Rename(oldDpaillierkey, newDpaillierkey)
			if err != nil {
				log.Debug().Err(err).Str("old-key", oldDpaillierkey).Str("new-key", newDpaillierkey).Msg("Failed to migrate old dpaillierkey")
			} else {
				log.Info().Str("old-key", oldDpaillierkey).Str("new-key", newDpaillierkey).Msg("Successfully migrated old dpaillierkey")
			}
		}
	}
}
